const constants = require("../constants");
const axios = require("axios");
const Boom = require("boom");
const ms = require("ms");

const expireInMinutes = ms(constants.otpExpiresIn, { long: true });
const authkey = process.env.MSG_AUTH_KEY;
// let url = `http://api.msg91.com/api/`;
let url = `https://www.txtguru.in/imobile/api.php?username=zepkartinfo&password=hsbc12hsbc&source=ZepKrt`;

module.exports = {
  sender: async (mobile, otp, message) => {
    try {
      if (!message) {
        message = `Your verification code is ${otp}. It will expire in ${expireInMinutes}.`;
      }
      let endpoint = url + `&dmobile=${mobile}&message=${message}`;
      axios
        .get(endpoint)
        .then(function(response) {
          return Promise.resolve();
        })
        .catch(function(err) {
          return Promise.reject();
        });
    } catch (err) {
      console.log(err);
      return Promise.reject(err);
    }
  }
};

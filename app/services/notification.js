const admin = require("firebase-admin");
const constants = require("../constants");

module.exports = (title, body, token) => {
  let payload = {
    notification: {
      title: title,
      body: body
    }
  };
  let options = {
    priority: constants.notification.priority,
    timeToLive: constants.notification.ttl
  };
  admin
    .messaging()
    .sendToDevice(token, payload, options)
    .then(function(response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
};

const Boom = require("boom");
const SendResponse = require("../apiHandler");
const UserModel = require("../models/user.model");
const jwt = require("jsonwebtoken");
const constants = require("../../constants");
const util = require("../../util");
var config = require("../../config");
const mailService = require("../services/mailer");
const userProfileDetailModel = require("../models/profileDetails.model");
const themeModel = require("../models/themeManagement.model");

const self = (module.exports = {
  login: async (req, res) => {
    try {
      let { user, password } = req.body;

      let query = {
        userType: {
          $ne: "admin"
        }
      };
      query.email = user.toString().toLowerCase();
      query.status = "active";

      user = await UserModel.findOne(query)
        .select("+password")
        .lean();
      if (!user) {
        return SendResponse(
          res,
          null,
          false,
          "User not found or blocked by admin."
        );
      } else {
        let model = new UserModel();
        let compare = await model.compare(password, user.password);
        if (compare) {
          delete user.password;
          let token = jwt.sign(
            {
              data: {
                _id: user._id,
                createdAt: new Date(),
                userType: user.userType
              }
            },
            constants.jwtSecret,
            {
              expiresIn: constants.tokenExpiresIn
            }
          );
          user.token = token;
          user.rememberMe = req.body.rememberMe;
          UserModel.findByIdAndUpdate(
            {
              _id: user._id
            },
            {
              webToken: req.body.webToken,
              token: token,
              rememberMe: req.body.rememberMe
            },
            {
              new: true
            },
            (err, data) => {}
          );
          let profileData = await userProfileDetailModel.findOne({
            userId: user._id
          });
          if (!profileData) {
            user.profileDataFlag = false;
            user.profileImage = ""; //       ......checking user profile is set or not
          } else {
            user.profileDataFlag = true;
            user.profileImage = profileData.profileImage;
            if (profileData.themeId) {
              user.themeId = profileData.themeId;
            }
          }
          return SendResponse(res, user, true, "Logged in successfully.");
        } else {
          return SendResponse(res, null, false, "Password is incorrect.");
        }
      }
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  socialLogin: async (req, res) => {
    try {
      let { email, firstName, facebookId, twitterId, instagramId } = req.body;
      let query = {
        userType: {
          $ne: "admin"
        }
      };
      query.email = email.toString().toLowerCase();
      let user = await UserModel.findOne(query)
        .select("+password")
        .lean();
      if (user) {
        let profileData = await userProfileDetailModel.findOne({
          userId: user._id
        });

        if (!profileData)
          //       ......checking user profile is set or not
          user.profileDataFlag = false;
        else user.profileDataFlag = true;
      }

      if (!user) {
        let user = new UserModel(req.body);
        user = await user.save();

        let token = jwt.sign(
          {
            data: {
              _id: user._id,
              createdAt: new Date(),
              userType: user.userType
            }
          },
          constants.jwtSecret,
          {
            expiresIn: constants.tokenExpiresIn
          }
        );
        user.token = token;
        user.set("userTypeFlag", false, {
          strict: false
        });
        user.set("profileDataFlag", false, {
          strict: false
        });
        UserModel.findByIdAndUpdate(
          {
            _id: user._id
          },
          {
            token: token
          },
          {
            new: true
          },
          (err, data) => {}
        );

        return SendResponse(res, user, true, "Logged in successfully.");
      } else {
        if (user.userType) {
          user.userTypeFlag = true;
          let token = jwt.sign(
            {
              data: {
                _id: user._id,
                createdAt: new Date(),
                userType: user.userType
              }
            },
            constants.jwtSecret,
            {
              expiresIn: constants.tokenExpiresIn
            }
          );
          user.token = token;
          delete user.password;
          UserModel.findByIdAndUpdate(
            {
              _id: user._id
            },
            {
              token: token
            },
            {
              new: true
            },
            (err, data) => {}
          );
          if (user.facebookId == facebookId) {
            return SendResponse(res, user, true, "Logged in successfully.");
          } else {
            UserModel.findByIdAndUpdate(
              {
                _id: user._id
              },
              {
                facebookId: facebookId
              },
              {
                new: true
              },
              (err, data) => {}
            );
            return SendResponse(res, user, true, "Logged in successfully.");
          }
        } else {
          user.userTypeFlag = false;
          return SendResponse(res, user, true, "Logged in successfully.");
        }
      }
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  jwtAuthenticate: async (req, res, next) => {
    var token = req != undefined ? req.headers["authenticate"] : false;
    if (!token) return SendResponse(res, null, false, "No token provided.");
    else {
      try {
        var decoded = await jwt.verify(token, constants.jwtSecret);
        req.decoded = decoded;
        UserModel.findOne(
          {
            _id: decoded.data._id,
            status: "active"
          },
          (err, result) => {
            if (!result) {
              req.decoded = undefined;
              return SendResponse(
                res,
                null,
                false,
                "Failed to authenticate token."
              );
            } else if (result) {
              next();
            }
          }
        );
      } catch (err) {
        req.decoded = undefined;
        return SendResponse(res, null, false, "Failed to authenticate token.");
      }
    }
  },

  forgetPassword: async function(req, res) {
    try {
      let { email } = req.body;
      let query = {};
      query.email = email;
      query.status = "active";
      user = await UserModel.findOne(query).select(
        "firstName email emailVerified"
      );
      if (!user) {
        return SendResponse(res, null, false, "User not found.");
      }
      let emailOtp = util.generateOTP();
      let emailLinkTime = new Date().getTime();
      emailLinkTime = emailLinkTime + 86400000;
      let emailToken = user._id + "~" + emailLinkTime + "~" + emailOtp;
      var link1 = `Hi ${user.firstName},<br> Please click the link to reset your password- http://localhost:3000/reset?emailToken=${emailToken}`;
      let sendMail = await mailService.send(
        req.body.email,
        "Password reset link",
        link1
      );
      UserModel.findByIdAndUpdate(
        {
          _id: user._id
        },
        {
          emailToken: emailOtp,
          emailLinkTime: emailLinkTime
        },
        {
          new: true
        },
        (err, data) => {}
      );
      return SendResponse(res, null, true, "Link sent successfully.");
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  // http://localhost:3002/api/auth/passwordReset?emailToken=${emailToken}&userId=${user._id}`

  tokenVerify: async function(req, res) {
    try {
      if (!req.params.token) {
        return SendResponse(res, null, false, "Please provide valid token.");
      }
      let timestamp = new Date().getTime();
      var fields = req.params.token.split("~");
      var userId = fields[0];
      var emailToken = fields[2];
      let userData = await UserModel.findOne({
        _id: userId,
        status: "active"
      });
      if (userData) {
        if (
          emailToken == userData.emailToken &&
          timestamp <= userData.emailLinkTime
        ) {
          let updateData = await UserModel.findOneAndUpdate(
            {
              _id: userId
            },
            {
              $set: {
                emailToken: "NULL",
                emailLinkTime: "NULL"
              }
            },
            {
              new: true
            }
          );
          return SendResponse(res, null, true, "Token Matched.");
        } else {
          return SendResponse(
            res,
            null,
            false,
            "Token Mismatch or Token Expired."
          );
        }
      } else
        return SendResponse(
          res,
          null,
          false,
          "Token Mismatch or Token Expired."
        );
    } catch (err) {
      return SendResponse(res, Boom.badImplementation());
    }
  },

  passwordReset: async function(req, res) {
    try {
      let { password } = req.body;
      var fields = req.body.emailToken.split("~");
      var userId = fields[0];
      let userData = await UserModel.findOne({
        _id: userId,
        status: "active"
      });
      if (userData) {
        let model = new UserModel();
        userData.password = await model.hash(password);
        userData.save();
        return SendResponse(res, null, true, "Password changed successfully.");
      } else
        return SendResponse(res, null, false, "User not found or blocked.");
    } catch (err) {
      return SendResponse(res, Boom.badImplementation());
    }
  },

  changePassword: async (req, res) => {
    try {
      let userId = req.decoded.data._id;
      let { oldPassword, newPassword } = req.body;
      let user = await UserModel.findOne({
        _id: userId,
        status: "active"
      }).select("+password");
      if (!user) {
        return SendResponse(res, null, false, "User not found.");
      }
      let model = new UserModel();
      let check = await model.compare(oldPassword, user.password);
      if (!check) {
        return SendResponse(res, null, false, "Password is incorrect.");
      } else {
        user.password = await model.hash(newPassword);
        user.save();
        return SendResponse(res, null, true, "Password changed.");
      }
    } catch (err) {
      return SendResponse(res, Boom.badImplementation());
    }
  }
});

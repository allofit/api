const nodeMailer = require("nodemailer");
const process = require("process");
const Sendresponse = require("../apiHandler");

module.exports.send = async (receiver, mailSubject, body, attachments) => {
  try {
    const transporter = nodeMailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.AUTHEMAIL, // generated ethereal user
        pass: process.env.AUTHPASSWORD // generated ethereal password
      }
    });
    const mailOptions = {
      from: { name: "all-of-it", address: "developer.clixlogix@gmail.com" },
      to: receiver,
      subject: mailSubject,
      attachments: attachments ? attachments : "",
      html: body
    };
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        return Promise.reject(err);
      }
      return Promise.resolve();
    });
  } catch (err) {
    console.log(err);
    return Promise.reject();
  }
};

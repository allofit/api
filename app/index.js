const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
var morgan = require("morgan");
//This tells express to log via morgan
//and morgan to log in the "combined" pre-defined format

require("dotenv").config();
require("../core/db");
//Connect Database

const apiRouter = require("./routes/api");
//call router file

const app = express();

app.use(
  morgan(function(tokens, req, res) {
    return [
      tokens.method(req, res),
      tokens.url(req, res),
      tokens.status(req, res),
      tokens.res(req, res, "content-length"),
      "-",
      tokens["response-time"](req, res),
      "ms",
      JSON.stringify(req.body)
    ].join(" ");
  })
);
//morgan for print request url and request payload\

app.use(cors());
//enable cors
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
// app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// we define our static folder
app.use(express.static("public"));

app.use("/api", apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
});

module.exports = app;

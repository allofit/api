module.exports = (res, data, success, message = "Success") => {
  if (data && data.isBoom && data.isBoom == true) {
    const response = {
      statusCode: 200,
      message: data.output.payload.message,
      success: false
    };
    res.status(200).json(response);
  } else {
    const response = {
      statusCode: 200,
      message,
      success,
      data
    };
    res.status(200).json(response);
  }
};

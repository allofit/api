const Boom = require("boom");
const messageModel = require("../models/message.model");
const SendResponse = require("../apiHandler");
const notificationController = require("./notification.controller");

module.exports.sendMessage = async (req, res) => {
  try {
    let { senderId, receiverId, message } = req.body;

    let messageDetails = await messageModel
      .findOneAndUpdate(
        { participants: { $all: [senderId, receiverId] } },
        { $push: { conversation: req.body } },
        { new: true }
      )
      .populate([
        {
          path: "conversation.receiverId",
          select: "profileId userType firstName lastName webToken",
          populate: { path: "profileId", select: "profileImage" }
        },
        {
          path: "conversation.senderId",
          select: "profileId userType firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        }
      ]);
    if (!messageDetails) {
      req.body.participants = [senderId, receiverId];
      req.body.conversation = [
        {
          senderId: senderId,
          receiverId: receiverId,
          message: message
        }
      ];
      messageDetails = new messageModel(req.body);
      messageDetails = await messageDetails.save();

      messageDetails = await messageModel
        .findOne({ participants: { $all: [senderId, receiverId] } })
        .populate([
          {
            path: "conversation.receiverId",
            select: "profileId userType firstName lastName webToken",
            populate: { path: "profileId", select: "profileImage" }
          },
          {
            path: "conversation.senderId",
            select: "profileId userType firstName lastName",
            populate: { path: "profileId", select: "profileImage" }
          }
        ]);

      return SendResponse(res, messageDetails, true, "Message Sent.");
      let notiDetails = await notificationController.sendNotificationForMessage(
        messageDetails,
        req.decoded.data._id,
        "New message"
      );
    } else {
      return SendResponse(res, messageDetails, true, "Message Sent.");
      let notiDetails = await notificationController.sendNotificationForMessage(
        messageDetails,
        req.decoded.data._id,
        "New message"
      );
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteMessage = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { messageId } = req.query;

    let deletedMessage = await messageModel.findOneAndUpdate(
      { "message._id": messageId },
      { "conversation.$.deletedUserId": userId },
      { new: true }
    );

    return SendResponse(res, deletedMessage, true, "Message deleted");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.viewMessage = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { messageId } = req.query;

    let messageData = await messageModel.findOne({ _id: messageId }).populate([
      {
        path: "conversation.receiverId",
        select: "profileId userType firstName lastName",
        populate: { path: "profileId", select: "profileImage" }
      },
      {
        path: "conversation.senderId",
        select: "profileId userType firstName lastName",
        populate: { path: "profileId", select: "profileImage" }
      }
    ]);

    return SendResponse(res, messageData, true, "Message shown");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.messageList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let messageList = await messageModel
      .find({ participants: { $in: [userId] } })
      .populate([
        {
          path: "conversation.receiverId",
          select: "profileId userType firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        },
        {
          path: "conversation.senderId",
          select: "profileId userType firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        }
      ]);
    return SendResponse(
      res,
      messageList,
      true,
      "Message List shown successfully"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteChat = async (req, res) => {
  try {
    let { chatId } = req.query;
    let deletedChatData = await messageModel.findByIdAndDelete({ _id: chatId });
    if (deletedChatData) {
      return SendResponse(res, null, true, "Chat delete successfully");
    } else {
      return SendResponse(res, null, false, "no chat found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

const Boom = require("boom");
const UserModel = require("../models/user.model");
const AssociationModel = require("../models/associationManagement.model");
const SendResponse = require("../apiHandler");
require("dotenv").config();
process.env.NODE_ENV = "local";
const notificationController = require("./notification.controller");

module.exports.getUnjoinedList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;

    let user = await UserModel.findOne({
      _id: userId,
      status: "active"
    }).populate("associationId");
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    var user_id = [];
    if (user.associationId) {
      user_id = [
        ...user.associationId.joinedRequest,
        ...user.associationId.sentRequest,
        ...user.associationId.receivedRequest
      ];
    }
    if (user.userType == "artist") {
      let studioList = await UserModel.find(
        { _id: { $nin: user_id }, userType: "studio" },
        "firstName lastName profileId"
      ).populate({ path: "profileId", select: "profileImage" });
      return SendResponse(res, studioList, true, "List shown.");
    } else {
      let artistList = await UserModel.find({
        _id: { $nin: user_id },
        userType: "artist"
      }).populate({ path: "profileId", select: "profileImage" });
      return SendResponse(res, artistList, true, "List shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.sendJoinRequest = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let senderData = await UserModel.findOne({
      _id: userId,
      status: "active"
    }).populate({ path: "associationId" });
    let receiverData = await UserModel.findOne({
      _id: req.body.joinId,
      status: "active"
    });
    //......Checking for user exist or not
    if (!senderData || !receiverData) {
      return SendResponse(res, null, false, "User not found.");
    } else {
      if (
        (senderData.userType == "artist" &&
          receiverData.userType == "studio") ||
        (senderData.userType == "studio" && receiverData.userType == "artist")
      ) {
        if (senderData.associationId) {
          let requestedUser = await AssociationModel.findOneAndUpdate(
            { userId: req.body.joinId },
            { $addToSet: { receivedRequest: userId } }
          );
          if (!requestedUser) {
            let obj = {
              userId: req.body.joinId,
              receivedRequest: [userId],
              userProfileId: receiverData.profileId
            };
            //......create new association model for the request receiver
            let associationData = new AssociationModel(obj);
            associationData = await associationData.save();

            let userData = await UserModel.findByIdAndUpdate(
              { _id: req.body.joinId },
              { $set: { associationId: associationData._id } }
            );
          }
          let joinedAssociation = await AssociationModel.findOneAndUpdate(
            {
              userId: userId
            },
            { $addToSet: { sentRequest: req.body.joinId } }
          );
        } else {
          let requestedUser = await AssociationModel.findOneAndUpdate(
            { userId: req.body.joinId },
            { $addToSet: { receivedRequest: userId } }
          );

          if (!requestedUser) {
            let obj = {
              userId: req.body.joinId,
              receivedRequest: [userId],
              userProfileId: receiverData.profileId
            };
            //......create new association model for the request receiver
            let associationData = new AssociationModel(obj);
            associationData = await associationData.save();
            let userData = await UserModel.findByIdAndUpdate(
              { _id: req.body.joinId },
              { $set: { associationId: associationData._id } }
            );
          }
          let obj = {
            userId: userId,
            sentRequest: [req.body.joinId],
            userProfileId: senderData.profileId
          };
          //......create new association model for the request sender
          let associationData = new AssociationModel(obj);
          associationData = await associationData.save();
          let data1 = await UserModel.findByIdAndUpdate(
            { _id: userId },
            { $set: { associationId: associationData._id } }
          );
        }
        return SendResponse(
          res,
          senderData,
          true,
          "Joined request sent successfully."
        );
        let notiDetails = await notificationController.sendNotificationForJoinRequest(
          senderData,
          receiverData,
          "Joined request"
        );
      } else {
        return SendResponse(res, null, false, "You cannot send request.");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getPendingRequestList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;

    let user = await UserModel.findOne({ _id: userId, status: "active" });
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }

    let requestList = await AssociationModel.findOne(
      { userId: userId },
      { receivedRequest: 1, _id: 0 }
    ).populate({
      path: "receivedRequest",
      select: "firstName lastName profileId",
      populate: { path: "profileId", select: "profileImage" }
    });
    if (!requestList || requestList.receivedRequest.length == 0) {
      return SendResponse(res, null, false, "No pending request.");
    } else {
      return SendResponse(res, requestList, true, "List Shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.joinRequestAction = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let user = await UserModel.findOne({
      _id: userId,
      status: "active"
    }).populate("associationId");
    let { action, joinId } = req.body;
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    let isExist = user.associationId.receivedRequest.includes(joinId);
    if (isExist) {
      if (action == "accept") {
        let result = await AssociationModel.findOneAndUpdate(
          { userId: userId },
          {
            $pull: { receivedRequest: joinId },
            $addToSet: { joinedRequest: joinId }
          }
        );
        let senderResult = await AssociationModel.findOneAndUpdate(
          { userId: joinId },
          { $pull: { sentRequest: userId }, $push: { joinedRequest: userId } }
        );
        return SendResponse(res, null, true, "Request accepted successfully.");
      } else {
        let result = await AssociationModel.findOneAndUpdate(
          { userId: userId },
          { $pull: { receivedRequest: joinId } }
        );
        let senderResult = await AssociationModel.findOneAndUpdate(
          { userId: joinId },
          { $pull: { sentRequest: userId } }
        );
        return SendResponse(res, null, true, "Request accepted successfully.");
      }
    } else {
      return SendResponse(res, null, false, "Can't perform this action.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getJoinedList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;

    let user = await UserModel.findOne({ _id: userId, status: "active" });
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    } else {
      let requestList = await AssociationModel.findOne(
        { userId: userId },
        { joinedRequest: 1, _id: 0 }
      ).populate({
        path: "joinedRequest",
        select: "firstName lastName profileId",
        populate: { path: "profileId", select: "profileImage" }
      });

      if (!requestList || requestList.joinedRequest.length == 0) {
        return SendResponse(res, null, true, "No data found.");
      } else {
        return SendResponse(res, requestList, true, "List Shown.");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.removeJoinedUser = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let removerId = req.params.removerId;

    let user = await UserModel.findOne({ _id: userId, status: "active" });
    if (!user) {
      return SendResponse(res, null, true, "User not found.");
    } else {
      let userAssociation = await AssociationModel.findOneAndUpdate(
        { userId: userId },
        { $pull: { joinedRequest: removerId } }
      );
      let removerAssociation = await AssociationModel.findOneAndUpdate(
        { userId: removerId },
        { $pull: { joinedRequest: userId } }
      );
      return SendResponse(res, null, true, "Removed successfully.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

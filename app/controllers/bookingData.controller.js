const BookingModel = require("../models/bookingData.model");
const BookingOrder = require("../models/bookingOrder.model");
const Boom = require("boom");
const CalendarModel = require("../models/calendar.model");
const UserModel = require("../models/user.model");
const SendResponse = require("../apiHandler");
const mongoose = require("mongoose");
const AssociationModel = require("../models/associationManagement.model");
const notificationController = require("./notification.controller");
const paymentController = require("./payment.controller");
const rescheduleBookingModel = require("../models/rescheduleBooking.model");

const ObjectId = mongoose.Types.ObjectId;

const fs = require("fs");
const path = require("path");
const AWS = require("aws-sdk");
//configuring the AWS environment
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY
});
var s3 = new AWS.S3();

module.exports.artistStudioList = async (req, res) => {
  try {
    let userId = req.query.userId;

    let requestList = await AssociationModel.findOne(
      { userId: userId },
      { joinedRequest: 1, _id: 0 }
    ).populate({
      path: "joinedRequest",
      select: "firstName lastName profileId",
      populate: { path: "profileId", select: "profileImage" }
    });

    if (!requestList || requestList.joinedRequest.length == 0) {
      return SendResponse(res, null, true, "No data found.");
    } else {
      return SendResponse(res, requestList, true, "List Shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.addBooking = async (req, res) => {
  try {
    let clientId = req.decoded.data._id;
    req.body.data = JSON.parse(req.body.data);

    let { studioId } = req.body.data;

    let promiseArr = [];

    if (req.files.length >= 1) {
      for (let i = 0; i <= req.files.length - 1; i++) {
        var filePath = req.files[i].path;
        var params = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Body: fs.createReadStream(filePath),
          Key: "referenceImages/" + Date.now() + "_" + path.basename(filePath),
          ACL: "public-read"
        };
        promiseArr.push(uploadImageToS3(params));
      }
    }
    let finalImageArr = await Promise.all(promiseArr);
    req.body.data.referenceImages = finalImageArr;
    req.body.data.clientId = clientId;
    let bookingOrder = new BookingOrder(req.body.data);

    bookingOrder = await bookingOrder.save();
    let clientData = await UserModel.findOne({
      _id: clientId,
      status: "active",
      userType: "client"
    }).populate("profileId");
    let bookingData = {};
    if (clientData) {
      if (studioId) {
        req.body.data.studioId = studioId;
      }
      req.body.data.bookingOrderId = bookingOrder._id;

      bookingData = new BookingModel(req.body.data);
      bookingData = await bookingData.save();
      bookingOrder = await BookingOrder.findOneAndUpdate(
        { _id: bookingOrder._id },
        { $set: { bookingDataId: bookingData._id } },
        { new: true }
      );

      return SendResponse(res, bookingData, true, "Booking Successfully");
      let notiDetails = await notificationController.sendNotificationForBooking(
        bookingData,
        clientId,
        "New Booking"
      );
    } else {
      return SendResponse(res, null, false, "Client not found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.clientBookingList = async (req, res) => {
  try {
    let clientId = req.decoded.data._id;
    let { limit, page, searchKey, searchBody, type, date } = req.query;

    let aggregate = [];

    aggregate.push({
      $lookup: {
        from: "bookingdatas",
        localField: "bookingDataId",
        foreignField: "_id",
        as: "bookingData"
      }
    });

    let matchBookingData = {};

    if (type == "upcoming") {
      matchBookingData = {
        $match: {
          "bookingData.clientId": ObjectId(clientId),
          "bookingData.date": { $gt: date }
        }
      };
    } else if (type == "current") {
      matchBookingData = {
        $match: {
          "bookingData.clientId": ObjectId(clientId),
          "bookingData.date": { $eq: date }
        }
      };
    } else if (type == "completed") {
      matchBookingData = {
        $match: {
          "bookingData.clientId": ObjectId(clientId),
          "bookingData.date": { $lt: date }
        }
      };
    } else {
      matchBookingData = {
        $match: {
          "bookingData.clientId": ObjectId(clientId)
        }
      };
    }

    if (searchKey == "date") {
      matchBookingData.$match.$and = [{ "bookingData.date": searchBody }];
    }

    //1
    aggregate.push(matchBookingData);

    //3
    aggregate.push({
      $lookup: {
        from: "users",
        localField: "artistId",
        foreignField: "_id",
        as: "artistData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "profiledetails",
        localField: "artistData.profileId",
        foreignField: "_id",
        as: "profileData"
      }
    });

    matchArtistData = {};
    if (searchBody) {
      if (searchKey == "name") {
        var nameQuery = searchBody,
          queryArr = nameQuery.split(" "),
          firstName,
          lastName;

        if (queryArr.length == 1) {
          firstName = queryArr[0];
          matchArtistData = {
            "artistData.firstName": { $regex: firstName, $options: "i" }
          };
        } else if (queryArr.length == 2) {
          firstName = queryArr[0];
          lastName = queryArr[1];
          matchArtistData.$or = [
            {
              $and: [
                {
                  "artistData.firstName": {
                    $regex: firstName,
                    $options: "i"
                  }
                },
                {
                  "artistData.lastName": { $regex: lastName, $options: "i" }
                }
              ]
            },
            {
              $and: [
                {
                  "artistData.firstName": { $regex: lastName, $options: "i" }
                },
                {
                  "artistData.lastName": { $regex: firstName, $options: "i" }
                }
              ]
            }
          ];
        }

        aggregate.push({ $match: matchArtistData });
      }
      if (searchKey == "contactNumber") {
        let contactNumber = searchBody;
        matchArtistData = {
          "profileData.contactNumber": { $regex: contactNumber, $options: "i" }
        };
        aggregate.push({ $match: matchArtistData });
      }
    }

    aggregate.push({
      $project: {
        paymentStatus: 1,
        bookingStatus: {
          $arrayElemAt: ["$bookingData.bookingStatus", 0]
        },
        paymentCollectedByArtist: {
          $arrayElemAt: ["$bookingData.paymentCollectedByArtist", 0]
        },
        isBookingCompleted: {
          $arrayElemAt: ["$bookingData.isBookingCompleted", 0]
        },
        clientPaidDepositAmount: {
          $arrayElemAt: ["$bookingData.clientPaidDepositAmount", 0]
        },
        clientPaidFullAmount: {
          $arrayElemAt: ["$bookingData.clientPaidFullAmount", 0]
        },
        isBookingConfirmByArtist: {
          $arrayElemAt: ["$bookingData.isBookingConfirmByArtist", 0]
        },
        isBookingConfirmByClient: {
          $arrayElemAt: ["$bookingData.isBookingConfirmByClient", 0]
        },
        date: { $arrayElemAt: ["$bookingData.date", 0] },
        bookingId: { $arrayElemAt: ["$bookingData._id", 0] },
        firstName: { $arrayElemAt: ["$artistData.firstName", 0] },
        lastName: { $arrayElemAt: ["$artistData.lastName", 0] },
        contactNumber: { $arrayElemAt: ["$profileData.contactNumber", 0] },
        address: { $arrayElemAt: ["$profileData.address", 0] },
        totalPrice: { $arrayElemAt: ["$bookingData.amount", 0] },
        depositAmountByArtist: {
          $arrayElemAt: ["$bookingData.depositAmountByArtist", 0]
        },
        fullAmountByArtist: {
          $arrayElemAt: ["$bookingData.fullAmountByArtist", 0]
        },
        artistId: {
          $arrayElemAt: ["$bookingData.artistId", 0]
        }
      }
    });

    aggregate.push({
      $facet: {
        data: [
          { $sort: { createdAt: -1 } },
          { $skip: parseInt(page) },
          { $limit: parseInt(limit) }
        ],
        pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
      }
    });

    let bookingSlotList = await BookingOrder.aggregate(aggregate);

    let data = bookingSlotList[0].data;
    let finalResult = {};
    if (data.length > 0) {
      finalResult = {
        data: data,
        count: bookingSlotList[0].pageInfo[0].count
      };
    } else {
      finalResult = {
        data: [],
        count: 0
      };
    }

    return SendResponse(res, finalResult, true, "List shown");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.artistBookingList = async (req, res) => {
  try {
    {
      let artistId = req.decoded.data._id;
      let { limit, page, searchKey, searchBody, type, date } = req.query;

      let aggregate = [];

      aggregate.push({
        $lookup: {
          from: "bookingdatas",
          localField: "bookingDataId",
          foreignField: "_id",
          as: "bookingData"
        }
      });

      let matchBookingData = {};

      if (type == "upcoming") {
        matchBookingData = {
          $match: {
            "bookingData.artistId": ObjectId(artistId),
            "bookingData.date": { $gt: date }
          }
        };
      } else if (type == "current") {
        matchBookingData = {
          $match: {
            "bookingData.artistId": ObjectId(artistId),
            "bookingData.date": { $eq: date }
          }
        };
      } else if (type == "completed") {
        matchBookingData = {
          $match: {
            "bookingData.artistId": ObjectId(artistId),
            "bookingData.date": { $lt: date }
          }
        };
      } else {
        matchBookingData = {
          $match: {
            "bookingData.artistId": ObjectId(artistId)
          }
        };
      }

      if (searchKey == "date") {
        matchBookingData.$match.$and = [{ "bookingData.date": searchBody }];
      }

      //1
      aggregate.push(matchBookingData);

      aggregate.push({
        $lookup: {
          from: "users",
          localField: "clientId",
          foreignField: "_id",
          as: "clientData"
        }
      });

      aggregate.push({
        $lookup: {
          from: "profiledetails",
          localField: "clientData.profileId",
          foreignField: "_id",
          as: "profileData"
        }
      });
      //3
      matchClientData = {};
      if (searchBody) {
        if (searchKey == "name") {
          var nameQuery = searchBody,
            queryArr = nameQuery.split(" "),
            firstName,
            lastName;

          if (queryArr.length == 1) {
            firstName = queryArr[0];
            matchClientData = {
              "clientData.firstName": { $regex: firstName, $options: "i" }
            };
          } else if (queryArr.length == 2) {
            firstName = queryArr[0];
            lastName = queryArr[1];
            matchClientData.$or = [
              {
                $and: [
                  {
                    "clientData.firstName": {
                      $regex: firstName,
                      $options: "i"
                    }
                  },
                  {
                    "clientData.lastName": {
                      $regex: lastName,
                      $options: "i"
                    }
                  }
                ]
              },
              {
                $and: [
                  {
                    "clientData.firstName": {
                      $regex: lastName,
                      $options: "i"
                    }
                  },
                  {
                    "clientData.lastName": {
                      $regex: firstName,
                      $options: "i"
                    }
                  }
                ]
              }
            ];
          }

          aggregate.push({ $match: matchClientData });
        }
        if (searchKey == "contactNumber") {
          let contactNumber = searchBody;
          matchClientData = {
            "profileData.contactNumber": {
              $regex: contactNumber,
              $options: "i"
            }
          };
          aggregate.push({ $match: matchClientData });
        }
      }
      aggregate.push({
        $project: {
          paymentStatus: 1,
          bookingStatus: {
            $arrayElemAt: ["$bookingData.bookingStatus", 0]
          },
          isBookingConfirmByArtist: {
            $arrayElemAt: ["$bookingData.isBookingConfirmByArtist", 0]
          },
          isBookingConfirmByClient: {
            $arrayElemAt: ["$bookingData.isBookingConfirmByClient", 0]
          },
          date: { $arrayElemAt: ["$bookingData.date", 0] },
          bookingId: { $arrayElemAt: ["$bookingData._id", 0] },
          firstName: { $arrayElemAt: ["$clientData.firstName", 0] },
          lastName: { $arrayElemAt: ["$clientData.lastName", 0] },
          contactNumber: { $arrayElemAt: ["$profileData.contactNumber", 0] },
          address: { $arrayElemAt: ["$profileData.address", 0] },
          totalPrice: { $arrayElemAt: ["$bookingData.amount", 0] },
          depositAmountByArtist: {
            $arrayElemAt: ["$bookingData.depositAmountByArtist", 0]
          },
          fullAmountByArtist: {
            $arrayElemAt: ["$bookingData.fullAmountByArtist", 0]
          }
        }
      });

      aggregate.push({
        $facet: {
          data: [
            { $sort: { createdAt: -1 } },
            { $skip: parseInt(page) },
            { $limit: parseInt(limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });

      let bookingSlotList = await BookingOrder.aggregate(aggregate);

      let data = bookingSlotList[0].data;
      let finalResult = {};
      if (data.length > 0) {
        finalResult = {
          data: data,
          count: bookingSlotList[0].pageInfo[0].count
        };
      } else {
        finalResult = {
          data: [],
          count: 0
        };
      }

      return SendResponse(res, finalResult, true, "List shown");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.studioBookingList = async (req, res) => {
  try {
    {
      let studioId = req.decoded.data._id;
      let { limit, page, searchKey, searchBody, type, date } = req.query;

      let aggregate = [];

      aggregate.push({
        $lookup: {
          from: "bookingdatas",
          localField: "bookingDataId",
          foreignField: "_id",
          as: "bookingData"
        }
      });

      let matchBookingData = {};

      if (type == "upcoming") {
        matchBookingData = {
          $match: {
            "bookingData.studioId": ObjectId(studioId),
            "bookingData.date": { $gt: date }
          }
        };
      } else if (type == "current") {
        matchBookingData = {
          $match: {
            "bookingData.studioId": ObjectId(studioId),
            "bookingData.date": { $eq: date }
          }
        };
      } else if (type == "completed") {
        matchBookingData = {
          $match: {
            "bookingData.studioId": ObjectId(studioId),
            "bookingData.date": { $lt: date }
          }
        };
      } else {
        matchBookingData = {
          $match: {
            "bookingData.studioId": ObjectId(studioId)
          }
        };
      }

      if (searchKey == "date") {
        matchBookingData.$match.$and = [{ "bookingData.date": searchBody }];
      }

      aggregate.push(matchBookingData);

      //3
      aggregate.push({
        $lookup: {
          from: "users",
          localField: "artistId",
          foreignField: "_id",
          as: "artistData"
        }
      });

      aggregate.push({
        $lookup: {
          from: "users",
          localField: "clientId",
          foreignField: "_id",
          as: "clientData"
        }
      });

      aggregate.push({
        $lookup: {
          from: "profiledetails",
          localField: "artistData.profileId",
          foreignField: "_id",
          as: "artistProfileData"
        }
      });

      aggregate.push({
        $lookup: {
          from: "profiledetails",
          localField: "clientData.profileId",
          foreignField: "_id",
          as: "clientProfileData"
        }
      });

      //3
      matchArtistData = {};
      if (searchBody) {
        if (searchKey == "name") {
          var nameQuery = searchBody,
            queryArr = nameQuery.split(" "),
            firstName,
            lastName;

          if (queryArr.length == 1) {
            firstName = queryArr[0];
            matchArtistData = {
              $or: [
                {
                  "artistData.firstName": {
                    $regex: firstName,
                    $options: "i"
                  }
                },
                {
                  "clientData.firstName": {
                    $regex: firstName,
                    $options: "i"
                  }
                }
              ]
            };
          } else if (queryArr.length == 2) {
            firstName = queryArr[0];
            lastName = queryArr[1];
            matchArtistData.$or = [
              {
                $and: [
                  {
                    $or: [
                      {
                        "artistData.firstName": {
                          $regex: firstName,
                          $options: "i"
                        },
                        "artistData.lastName": {
                          $regex: lastName,
                          $options: "i"
                        }
                      },
                      {
                        "clientData.firstName": {
                          $regex: firstName,
                          $options: "i"
                        },
                        "clientData.lastName": {
                          $regex: lastName,
                          $options: "i"
                        }
                      }
                    ]
                  }
                ]
              },
              {
                $and: [
                  {
                    $or: [
                      {
                        "artistData.firstName": {
                          $regex: lastName,
                          $options: "i"
                        }
                      },
                      {
                        "clientData.firstName": {
                          $regex: lastName,
                          $options: "i"
                        }
                      }
                    ]
                  },
                  {
                    $or: [
                      {
                        "artistData.lastName": {
                          $regex: firstName,
                          $options: "i"
                        }
                      },
                      {
                        "clientData.lastName": {
                          $regex: firstName,
                          $options: "i"
                        }
                      }
                    ]
                  }
                ]
              }
            ];
          }

          aggregate.push({ $match: matchArtistData });
        }
        if (searchKey == "contactNumber") {
          let contactNumber = searchBody;
          matchArtistData = {
            $or: [
              {
                "artistProfileData.contactNumber": {
                  $regex: contactNumber,
                  $options: "i"
                }
              },
              {
                "clientProfileData.contactNumber": {
                  $regex: contactNumber,
                  $options: "i"
                }
              }
            ]
          };
          aggregate.push({ $match: matchArtistData });
        }
      }

      aggregate.push({
        $project: {
          isBookingConfirmByArtist: {
            $arrayElemAt: ["$bookingData.isBookingConfirmByArtist", 0]
          },
          isBookingConfirmByClient: {
            $arrayElemAt: ["$bookingData.isBookingConfirmByClient", 0]
          },
          date: { $arrayElemAt: ["$bookingData.date", 0] },
          bookingId: { $arrayElemAt: ["$bookingData._id", 0] },
          artistFirstName: { $arrayElemAt: ["$artistData.firstName", 0] },
          artistLastName: { $arrayElemAt: ["$artistData.lastName", 0] },
          clientFirstName: {
            $arrayElemAt: ["$clientData.firstName", 0]
          },
          clientLastName: { $arrayElemAt: ["$clientData.lastName", 0] },
          artistContactNumber: {
            $arrayElemAt: ["$artistProfileData.contactNumber", 0]
          },
          clientContactNumber: {
            $arrayElemAt: ["$clientProfileData.contactNumber", 0]
          },
          artistAddress: {
            $arrayElemAt: ["$artistProfileData.address", 0]
          },
          clientAddress: {
            $arrayElemAt: ["$clientProfileData.address", 0]
          },
          totalPrice: { $arrayElemAt: ["$bookingData.amount", 0] },

          depositAmountByArtist: {
            $arrayElemAt: ["$bookingData.depositAmountByArtist", 0]
          },
          fullAmountByArtist: {
            $arrayElemAt: ["$bookingData.fullAmountByArtist", 0]
          }
        }
      });

      aggregate.push({
        $facet: {
          data: [
            { $sort: { createdAt: -1 } },
            { $skip: parseInt(page) },
            { $limit: parseInt(limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });

      let bookingSlotList = await BookingOrder.aggregate(aggregate);

      let data = bookingSlotList[0].data;
      let finalResult = {};
      if (data.length > 0) {
        finalResult = {
          data: data,
          count: bookingSlotList[0].pageInfo[0].count
        };
      } else {
        finalResult = {
          data: [],
          count: 0
        };
      }

      return SendResponse(res, finalResult, true, "List shown");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.artistBookedSlot = async (req, res) => {
  try {
    let artistId = req.decoded.data._id;
    let { limit, page, searchKey, searchBody, type, date } = req.query;

    let matchBookingData = {};

    if (type == "upcoming") {
      matchBookingData = {
        $match: { artistId: ObjectId(artistId), date: { $gt: date } }
      };
    } else if (type == "current") {
      matchBookingData = {
        $match: { artistId: ObjectId(artistId), date: { $eq: date } }
      };
    } else {
      matchBookingData = {
        $match: { artistId: ObjectId(artistId), date: { $lt: date } }
      };
    }

    if (searchKey == "date") {
      matchBookingData.$match.$and = [{ date: searchBody }];
    }

    let aggregate = [];

    //1
    aggregate.push(matchBookingData);

    //2

    aggregate.push({
      $lookup: {
        from: "bookingorders",
        localField: "bookingOrderId",
        foreignField: "_id",
        as: "bookingOrderData"
      }
    });

    //3
    aggregate.push({
      $lookup: {
        from: "users",
        localField: "clientId",
        foreignField: "_id",
        as: "clientData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "profiledetails",
        localField: "clientData.profileId",
        foreignField: "_id",
        as: "profileData"
      }
    });

    matchClientData = {};
    if (searchBody) {
      if (searchKey == "name") {
        var nameQuery = searchBody,
          queryArr = nameQuery.split(" "),
          firstName,
          lastName;
        if (queryArr.length == 1) {
          firstName = queryArr[0];
          matchClientData = {
            "clientData.firstName": { $regex: firstName, $options: "i" }
          };
        } else if (queryArr.length == 2) {
          firstName = queryArr[0];
          lastName = queryArr[1];
          matchClientData.$or = [
            {
              $and: [
                {
                  "clientData.firstName": {
                    $regex: firstName,
                    $options: "i"
                  }
                },
                {
                  "clientData.lastName": { $regex: lastName, $options: "i" }
                }
              ]
            },
            {
              $and: [
                {
                  "clientData.firstName": {
                    $regex: lastName,
                    $options: "i"
                  }
                },
                {
                  "clientData.lastName": {
                    $regex: firstName,
                    $options: "i"
                  }
                }
              ]
            }
          ];
        }

        aggregate.push({ $match: matchClientData });
      }
      if (searchKey == "contactNumber") {
        let contactNumber = searchBody;
        matchClientData = {
          "profileData.contactNumber": { $regex: contactNumber, $options: "i" }
        };
        aggregate.push({ $match: matchClientData });
      }
    }

    aggregate.push({
      $project: {
        date: 1,
        startTime: 1,
        endTime: 1,
        bookingId: { $arrayElemAt: ["$bookingOrderData.bookingId", 0] },
        firstName: { $arrayElemAt: ["$clientData.firstName", 0] },
        lastName: { $arrayElemAt: ["$clientData.lastName", 0] },
        contactNumber: { $arrayElemAt: ["$profileData.contactNumber", 0] },
        address: { $arrayElemAt: ["$profileData.address", 0] }
      }
    });

    aggregate.push({
      $facet: {
        data: [
          { $sort: { createdAt: -1 } },
          { $skip: parseInt(page) },
          { $limit: parseInt(limit) }
        ],
        pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
      }
    });

    let bookingSlotList = await BookingModel.aggregate(aggregate);

    let data = bookingSlotList[0].data;
    let finalResult = {};
    if (data.length > 0) {
      finalResult = {
        data: data,
        count: bookingSlotList[0].pageInfo[0].count
      };
    } else {
      finalResult = {
        data: [],
        count: 0
      };
    }

    return SendResponse(res, finalResult, true, "List shown");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.studioBookedSlot = async (req, res) => {
  try {
    let studioId = req.decoded.data._id;
    if (req.decoded.data.userType != "studio") {
      return SendResponse(res, null, false, "Not a Artist Studio");
    }
    let { limit, page, searchKey, searchBody, type, date } = req.query;

    let matchBookingData = {};

    if (type == "upcoming") {
      matchBookingData = {
        $match: { studioId: ObjectId(studioId), date: { $gt: date } }
      };
    } else if (type == "current") {
      matchBookingData = {
        $match: { studioId: ObjectId(studioId), date: { $eq: date } }
      };
    } else {
      matchBookingData = {
        $match: { studioId: ObjectId(studioId), date: { $lt: date } }
      };
    }

    if (searchKey == "date") {
      matchBookingData.$match.$and = [{ date: searchBody }];
    }

    let aggregate = [];

    //1
    aggregate.push(matchBookingData);

    //2

    aggregate.push({
      $lookup: {
        from: "bookingorders",
        localField: "bookingOrderId",
        foreignField: "_id",
        as: "bookingOrderData"
      }
    });

    //3
    aggregate.push({
      $lookup: {
        from: "users",
        localField: "clientId",
        foreignField: "_id",
        as: "clientData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "profiledetails",
        localField: "clientData.profileId",
        foreignField: "_id",
        as: "profileData"
      }
    });

    matchClientData = {};
    if (searchBody) {
      if (searchKey == "name") {
        var nameQuery = searchBody,
          queryArr = nameQuery.split(" "),
          firstName,
          lastName;

        if (queryArr.length == 1) {
          firstName = queryArr[0];
          matchClientData = {
            "clientData.firstName": { $regex: firstName, $options: "i" }
          };
        } else if (queryArr.length == 2) {
          firstName = queryArr[0];
          lastName = queryArr[1];
          matchClientData.$or = [
            {
              $and: [
                {
                  "clientData.firstName": {
                    $regex: firstName,
                    $options: "i"
                  }
                },
                {
                  "clientData.lastName": { $regex: lastName, $options: "i" }
                }
              ]
            },
            {
              $and: [
                {
                  "clientData.firstName": {
                    $regex: lastName,
                    $options: "i"
                  }
                },
                {
                  "clientData.lastName": {
                    $regex: firstName,
                    $options: "i"
                  }
                }
              ]
            }
          ];
        }

        aggregate.push({ $match: matchClientData });
      }
      if (searchKey == "contactNumber") {
        let contactNumber = searchBody;
        matchClientData = {
          "profileData.contactNumber": { $regex: contactNumber, $options: "i" }
        };
        aggregate.push({ $match: matchClientData });
      }
    }

    aggregate.push({
      $project: {
        date: 1,
        startTime: 1,
        endTime: 1,
        bookingId: { $arrayElemAt: ["$bookingOrderData.bookingId", 0] },
        firstName: { $arrayElemAt: ["$clientData.firstName", 0] },
        lastName: { $arrayElemAt: ["$clientData.lastName", 0] },
        contactNumber: { $arrayElemAt: ["$profileData.contactNumber", 0] },
        address: { $arrayElemAt: ["$profileData.address", 0] }
      }
    });

    aggregate.push({
      $facet: {
        data: [
          { $sort: { createdAt: -1 } },
          { $skip: parseInt(page) },
          { $limit: parseInt(limit) }
        ],
        pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
      }
    });
    console.log(JSON.stringify(aggregate));
    let bookingSlotList = await BookingModel.aggregate(aggregate);

    let data = bookingSlotList[0].data;
    let finalResult = {};
    if (data.length > 0) {
      finalResult = {
        data: data,
        count: bookingSlotList[0].pageInfo[0].count
      };
    } else {
      finalResult = {
        data: [],
        count: 0
      };
    }

    return SendResponse(res, finalResult, true, "List shown");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getBookingDetails = async (req, res) => {
  try {
    let { bookingId } = req.query;
    let bookingData = await BookingModel.findOne({ _id: bookingId }).populate([
      {
        path: "artistId",
        select: "firstName lastName email",
        populate: { path: "profileId", select: { _id: 0, contactNumber: 1 } }
      },
      {
        path: "clientId",
        select: "firstName lastName email",
        populate: { path: "profileId", select: { _id: 0, contactNumber: 1 } }
      },
      {
        path: " bookingOrderId",
        select:
          "startTime endTime slots paymentStatus bookingStatus bookingId paymentMode totalAmount transactionId paymentId"
      }
    ]);
    if (bookingData) {
      if (bookingData.artistId.profileId.contactNumber)
        bookingData.set(
          "contactNumber",
          bookingData.artistId.profileId.contactNumber,
          {
            strict: false
          }
        );
      bookingData.artistId.contactNumber =
        bookingData.artistId.profileId.contactNumber;
      return SendResponse(res, bookingData, true, "Data shown");
    } else {
      return SendResponse(res, null, false, "No booking data");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.changeBookingStatus = async (req, res) => {
  try {
    let { bookingId, status } = req.body;
    let bookingData = await BookingOrder.findOneAndUpdate(
      { _id: bookingId, bookingStatus: "pending" },
      { $set: { bookingStatus: status } },
      { new: true }
    );
    let updatedBookingOrderData = await BookingOrder.findOneAndUpdate(
      { bookingDataId: bookingId, bookingStatus: "pending" },
      { $set: { bookingStatus: status, paymentStatus: status } },
      { new: true }
    );
    if (!bookingData) {
      return SendResponse(res, null, false, "No booking found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.completeBooking = async (req, res) => {
  try {
    let { bookingId } = req.body;
    let bookingData = await BookingModel.findOneAndUpdate(
      { _id: bookingId },
      { $set: { isBookingCompleted: true } },
      { new: true }
    );
    bookingData = await BookingOrder.findOneAndUpdate(
      { bookingDataId: bookingId },
      { $set: { isBookingCompleted: true } },
      { new: true }
    );
    return SendResponse(res, bookingData, true, "Data update successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.paymentCollectedByArtist = async (req, res) => {
  try {
    let { bookingId } = req.body;
    let bookingData = await BookingModel.findOneAndUpdate(
      { _id: bookingId },
      { $set: { paymentCollectedByArtist: true } },
      { new: true }
    );
    bookingData = await BookingOrder.findOneAndUpdate(
      { bookingDataId: bookingId },
      { $set: { paymentCollectedByArtist: true } },
      { new: true }
    );
    return SendResponse(res, bookingData, true, "Data update successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.cancelBookingByClient = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { bookingId, paymentId, message } = req.body;
    let bookingData = await BookingModel.findOne({ _id: bookingId });
    if (!bookingData) {
      return SendResponse(res, null, false, "Booking not found");
    }
    let refundDataObj = {
      senderId: userId,
      paymentId: paymentId
    };
    let refundPayment = await paymentController.cancelBookingRefundPayment(
      refundDataObj
    );
    if (refundPayment.responseCode == "400") {
      return SendResponse(
        res,
        refundPayment.data,
        false,
        "Refund unsuccessful"
      );
    }
    bookingData = await BookingModel.findOneAndUpdate(
      { _id: bookingId },
      { $set: { bookingStatus: "cancelled", cancellationReason: message } },
      { new: true }
    );
    if (!bookingData) {
      return SendResponse(res, null, false, "Booking not found");
    }
    let bookingOrder = await BookingOrder.findOneAndUpdate(
      { bookingDataId: bookingId, paymentStatus: "partialCompleted" },
      { $set: { bookingStatus: "cancelled", paymentStatus: "refunded" } },
      { new: true }
    );

    return SendResponse(
      res,
      bookingOrder,
      true,
      "Booking cancelled successful"
    );
    let notiDetails = await notificationController.sendNotificationForCancelBooking(
      bookingData,
      userId,
      "Cancel Booking"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.rescheduleBooking = async (req, res) => {
  try {
    let { bookingId, date, slots } = req.body;
    let bookingData = await BookingModel.findOne({
      _id: bookingId,
      bookingStatus: "completed"
    }).populate({ path: "bookingOrderId", select: "slots paymentId" });
    if (!bookingData) {
      return SendResponse(res, null, false, "No booking found");
    }
    if (bookingData.bookingOrderId.slots.length != slots.length) {
      return SendResponse(
        res,
        null,
        false,
        "Please select same number of slots as booking"
      );
    }

    let obj = {
      date: date,
      slots: slots,
      bookingId: bookingId,
      paymentId: bookingData.bookingOrderId.paymentId
    };
    let rescheduleBookingData = new rescheduleBookingModel(obj);
    rescheduleBookingData = await rescheduleBookingData.save();

    bookingData = await BookingModel.findOneAndUpdate(
      { _id: bookingId },
      {
        $set: {
          isBookingReschedule: true,
          rescheduleBookingId: rescheduleBookingData._id
        }
      },
      { new: true }
    );

    return SendResponse(
      res,
      rescheduleBookingData,
      true,
      "Your reschedule request send to the artist"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.rescheduleBookingList = async (req, res) => {
  try {
    let artistId = req.decoded.data._id;
    let bookingList = await BookingModel.find(
      {
        artistId: artistId,
        isBookingReschedule: true
      },
      "date clientId"
    ).populate([
      {
        path: "rescheduleBookingId",
        select: "slots",
        match: { isArtistRespond: false }
      },
      { path: "bookingOrderId", select: "slots" }
    ]);
    return SendResponse(res, bookingList, true, "List shown successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.rescheduleBookingDetails = async (req, res) => {
  try {
    let artistId = req.decoded.data._id;
    let { bookingId } = req.query;
    let bookingDetails = await BookingModel.find({
      _id: bookingId
    }).populate([
      {
        path: "rescheduleBookingId",
        select: "slots",
        match: { isArtistRespond: false }
      },
      { path: "bookingOrderId", select: "slots" }
    ]);
    return SendResponse(res, bookingDetails, true, "Data shown successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.clientListOfArtist = async (req, res) => {
  try {
    artistId = req.decoded.data._id;
    let clientList = await BookingModel.find(
      { artistId: artistId },
      "clientId"
    ).populate({
      path: "clientId",
      select: "firstName lastName email profileId",
      populate: { path: "profileId", select: "contactNumber profileImage" }
    });
    return SendResponse(res, clientList, true, "List shown successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.clientBookingListForArtist = async (req, res) => {
  try {
    let { clientId } = req.query;
    let artistId = req.decoded.data._id;
    let clientBookingList = await BookingModel.find({
      clientId: clientId,
      artistId: artistId
    });
    console.log(JSON.stringify(clientBookingList));
    return SendResponse(
      res,
      clientBookingList,
      true,
      "List shown successfully"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.bookingData = async (req, res) => {
  try {
    let { bookingId } = req.query;
    let bookingData = await BookingModel.findOne({ _id: bookingId });
    return SendResponse(res, bookingData, true, "Data shown successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.addBookingAmountByArtist = async (req, res) => {
  try {
    let {
      fullAmountByArtist,
      bookingId,
      depositAmountByArtist,
      totalHour,
      selectedDateByArtist,
      bookingFee,
      stripeFee
    } = req.body;
    let bookingData = await BookingModel.findOneAndUpdate(
      { _id: bookingId },
      {
        $set: {
          fullAmountByArtist: fullAmountByArtist,
          depositAmountByArtist: depositAmountByArtist,
          totalHour: totalHour,
          selectedDateByArtist: selectedDateByArtist,
          date: selectedDateByArtist,
          isBookingConfirmByArtist: true,
          bookingFee: 10,
          stripeFee: 1.75
        }
      },
      { new: true }
    );
    return SendResponse(res, bookingData, true, "Data shown successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

function uploadImageToS3(params) {
  return new Promise((resolve, reject) => {
    s3.upload(params, function(err, data) {
      if (err) {
        console.log("Error", err);
        reject(err);
      }
      if (data) {
        resolve(data.Location);
        console.log("Uploaded in:", data.Location);
      }
    });
  });
}

const Boom = require("boom");
const supportModel = require("../models/helpAndSupport.model").helpAndSupport;
const userModel = require("../models/user.model");
const SendResponse = require("../apiHandler");

const self = (module.exports = {
  generateTicket: async (req, res) => {
    try {
      let userId = req.decoded.data._id;
      let { title, message, by } = req.body;
      let obj = new supportModel({
        userId: userId,
        title,
        conversation: [
          {
            by,
            message
          }
        ]
      });
      obj = await obj.save();
      return SendResponse(res, obj, true, "Ticket created successfully.");
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  sendTicketMessage: async (req, res) => {
    try {
      let userId = req.decoded.data._id;
      let { ticketId, message, by } = req.body;
      let ticketData = await supportModel.findOne({ _id: ticketId });
      if (!ticketData) {
        return SendResponse(res, false, null, "Ticket not found.");
      }
      let reply = {
        by: by !== "admin" ? "user" : "support",
        message
      };
      ticketData.conversation.push(reply);
      ticketData = await ticketData.save();
      ticketData = await supportModel.findOne({ _id: ticketId }).populate({
        path: "userId",
        select: "firstName lastName profileId",
        populate: { path: "profileId", select: "profileImage" }
      });
      return SendResponse(
        res,
        ticketData,
        true,
        "Ticket updated with new message successfully."
      );
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  updateTicketStatus: async (req, res) => {
    try {
      let { ticketId, status } = req.body;
      if (req.decoded.data.userType !== "admin") {
        return SendResponse(
          res,
          null,
          false,
          "Only admin can change the status"
        );
      }
      let ticketData = await supportModel.findOne({ _id: ticketId });
      if (!ticketData) {
        return SendResponse(res, null, false, "Ticket not found.");
      }
      ticketData = await supportModel.findOneAndUpdate(
        { _id: ticketId },
        { status },
        { new: true }
      );
      return SendResponse(res, ticketData, true, "Ticket updated.");
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  deleteTicket: async (req, res) => {
    try {
      let { ticketId } = req.query;
      if (req.decoded.data.userType !== "admin") {
        return SendResponse(
          res,
          null,
          false,
          "Only admin can delete a ticket."
        );
      }
      let ticketData = await supportModel.findOneAndDelete({ _id: ticketId });
      if (ticketData != null) {
        return SendResponse(res, null, true, "Ticket deleted successfully.");
      } else {
        return SendResponse(res, null, false, "Ticket not found.");
      }
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  updateMessage: async (req, res) => {
    try {
      let { ticketId, message, messageId, user } = req.body;
      if (req.user.type !== "admin" && user !== req.user._id) {
        return SendResponse(res, Boom.unauthorized());
      }
      let query = {
        _id: ticketId,
        user: user,
        conversation: {
          $elemMatch: {
            _id: messageId,
            by: req.user.type !== "admin" ? "user" : "support"
          }
        }
      };
      let ticketData = await supportModel.findOneAndUpdate(
        query,
        { $set: { "conversation.$.message": message } },
        { new: true }
      );
      if (ticketData === null) {
        return SendResponse(res, null, false, "Ticket or message not found.");
      }
      return SendResponse(res, ticketData, true, "Message updated.");
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  deleteMessage: async (req, res) => {
    try {
      let { ticketId, messageId } = req.query;
      if (req.decoded.data.userType !== "admin") {
        return SendResponse(
          res,
          null,
          false,
          "Only admin can delete the message"
        );
      }
      let query = {
        _id: ticketId,
        conversation: {
          $elemMatch: {
            _id: messageId
          }
        }
      };
      let ticketData = await supportModel.findOneAndUpdate(
        query,
        { $pull: { conversation: query.conversation.$elemMatch } },
        { new: true }
      );
      if (ticketData === null) {
        return SendResponse(res, null, false, "Ticket or message not found.");
      }
      return SendResponse(res, ticketData, true, "Message Deleted.");
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  getTicketList: async (req, res) => {
    try {
      let userId = req.decoded.data._id;
      let { limit, page } = req.query;
      let options = {
        limit: parseInt(limit) || 20,
        page: parseInt(page) || 1,
        populate: {
          path: "userId",
          select: "firstName lastName profileId",
          populate: { path: "profileId", select: "profileImage" }
        },
        sort: {
          createdAt: -1
        }
      };
      let ticketList = await supportModel.paginate({ userId: userId }, options);
      return SendResponse(res, ticketList, true, "Tickets fetched.");
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  getTicketDetails: async (req, res) => {
    try {
      let userId = req.decoded.data._id;
      let { ticketId } = req.query;

      let ticketData = await supportModel.findOne({ _id: ticketId }).populate({
        path: "userId",
        select: "firstName lastName profileId",
        populate: { path: "profileId", select: "profileImage" }
      });
      if (ticketData) {
        return SendResponse(res, ticketData, true, "Tickets fetched.");
      } else {
        return SendResponse(res, null, false, "Ticket not found.");
      }
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  }
});

const Boom = require("boom");
const UserModel = require("../models/user.model");
const themeModel = require("../models/themeManagement.model");
const userProfileDetailModel = require("../models/profileDetails.model");
const SendResponse = require("../apiHandler");
const util = require("../../util");
const constants = require("../../constants");
const jwt = require("jsonwebtoken");
const mailService = require("../services/mailer");
require("dotenv").config();
process.env.NODE_ENV = "local";
var config = require("../../config");

module.exports.updateUserThemeData = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    req.body.userId = userId;
    let themeDetails = await themeModel.findOneAndUpdate(
      { userId: userId },
      req.body,
      { new: true }
    );

    if (!themeDetails) {
      themeDetails = new themeModel(req.body);
      themeDetails = await themeDetails.save();
    }
    let updatedTheme = await userProfileDetailModel.findOneAndUpdate(
      { userId: userId },
      { $set: { themeId: themeDetails._id } },
      { new: true }
    );
    return SendResponse(res, themeDetails, true, "Data Shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.themeDetails = async (req, res) => {
  try {
    let { themeId } = req.query;
    let themeDetails = await themeModel.findOne({ _id: themeId });
    return SendResponse(res, themeDetails, true, "Data Shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.userThemeDetails = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let themeDetails = await themeModel.findOne({ userId: userId });
    return SendResponse(res, themeDetails, true, "Data Shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

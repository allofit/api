const Boom = require("boom");
const CalendarModel = require("../models/calendar.model");
const bookingModel = require("../models/bookingData.model");
const SendResponse = require("../apiHandler");
require("dotenv").config();
process.env.NODE_ENV = "local";
var config = require("../../config");
const async = require("async");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
module.exports.addSlotsInCalendar = async (req, res) => {
  try {
    let artistId = req.decoded.data._id;
    let { startTime, endTime, slotId } = req.body;
    endTime = parseFloat(endTime);
    startTime = parseFloat(startTime);
    totalHour = endTime - startTime;
    req.body.totalHour = totalHour;
    req.body.artistId = artistId;
    if (slotId) {
      let mongoWhere = {
        _id: slotId
      };

      let calendarData = await CalendarModel.findOneAndUpdate(
        mongoWhere,
        req.body,
        { new: true }
      );

      let arr = [];
      arr.push(calendarData);
      return SendResponse(res, arr, true, "Slot updated Successfully.");
    } else {
      calendarData = new CalendarModel(req.body);
      calendarData = await calendarData.save();
      let arr = [];
      arr.push(calendarData);
      return SendResponse(res, arr, true, "Slot Added Successfully.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getRemainingHour = async (req, res) => {
  try {
    let { startDate, endDate, artistId } = req.query;

    let remainingHourSlots = await CalendarModel.aggregate([
      {
        $match: {
          artistId: ObjectId(artistId),
          date: { $gte: startDate },
          date: { $lte: endDate }
        }
      },
      { $group: { _id: "$date", totalHours: { $sum: "$totalHour" } } },
      {
        $lookup: {
          from: "bookingdatas",
          let: { cal_date: "$_id" },
          pipeline: [
            { $match: { $expr: { $eq: ["$date", "$$cal_date"] } } },
            {
              $group: {
                _id: "$date",
                totalBookingHours: { $sum: "$totalHour" }
              }
            },
            { $project: { booking_date: "$_id", totalBookingHours: 1 } }
          ],
          as: "bookingData"
        }
      },
      {
        $project: {
          sHour: {
            $cond: [
              {
                $gte: [
                  { $arrayElemAt: ["$bookingData.totalBookingHours", 0] },
                  0
                ]
              },
              {
                $subtract: [
                  "$totalHours",
                  { $arrayElemAt: ["$bookingData.totalBookingHours", 0] }
                ]
              },
              "$totalHours"
            ]
          }
        }
      }
    ]);
    if (remainingHourSlots.length > 0) {
      return SendResponse(
        res,
        remainingHourSlots,
        true,
        "Slot Shown Successfully."
      );
    } else {
      return SendResponse(res, remainingHourSlots, true, "No slots found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getCalendarSlots = async (req, res) => {
  try {
    let { startDate, endDate, artistId } = req.query;
    let calendarSlots = await CalendarModel.find({
      $and: [
        { artistId: artistId },
        { date: { $gte: startDate } },
        { date: { $lte: endDate } }
      ]
    });
    if (calendarSlots.length > 0) {
      return SendResponse(res, calendarSlots, true, "Slot Shown Successfully.");
    } else {
      return SendResponse(res, calendarSlots, true, "No slots found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getArtistAvailableDateList = async (req, res) => {
  try {
    let { artistId, date } = req.query;
    let calendarData = await CalendarModel.aggregate([
      { $match: { artistId: ObjectId(artistId), date: { $eq: date } } },
      { $group: { _id: "$date", totalHour: { $sum: "$totalHour" } } }
    ]);
    if (calendarData.length > 0) {
      let bookingData = await bookingModel.aggregate([
        {
          $match: {
            artistId: ObjectId(artistId),
            date: { $eq: date },
            isBookingConfirmByArtist: true,
            isBookingConfirmByClient: true
          }
        },
        { $group: { _id: "$date", totalHour: { $sum: "$totalHour" } } }
      ]);
      let obj = {};
      if (bookingData.length > 0) {
        let remainingHour =
          parseFloat(calendarData[0].totalHour) -
          parseFloat(bookingData[0].totalHour);
        obj = {
          date: calendarData[0]._id,
          remainingHour: remainingHour
        };
      } else {
        obj = {
          date: calendarData[0]._id,
          remainingHour: calendarData[0].totalHour
        };
      }
      return SendResponse(res, obj, true, "Hour Shown Successfully.");
    } else {
      return SendResponse(res, null, true, "No slot found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteSlotFromCalendar = async (req, res) => {
  try {
    let artistId = req.decoded.data._id;
    let { slotId, startDate, endDate } = req.query;
    let deletedSlot = await CalendarModel.findByIdAndDelete(slotId);
    if (deletedSlot) {
      let calendarSlots = await CalendarModel.find({
        $and: [
          { artistId: artistId },
          { date: { $gte: startDate } },
          { date: { $lte: endDate } }
        ]
      });
      return SendResponse(
        res,
        calendarSlots,
        true,
        "Slot deleted successfully"
      );
    } else {
      return SendResponse(res, null, false, "No deleted slots found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

const Boom = require("boom");
const specializationModel = require("../models/specialization.model");
const faqModel = require("../models/faq.model");
const SendResponse = require("../apiHandler");
const util = require("../../util");
const constants = require("../../constants");
const jwt = require("jsonwebtoken");
const mailService = require("../services/mailer");
require("dotenv").config();
process.env.NODE_ENV = "local";
var config = require("../../config");
const UserModel = require("../models/user.model");
const userProfileDetailModel = require("../models/profileDetails.model");
const subscriptionModel = require("../models/subscription.model");
const userSubscriptionModel = require("../models/userSubscription.model");
const supportModel = require("../models/helpAndSupport.model").helpAndSupport;
const themeModel = require("../models/themeManagement.model");
const bookingModel = require("../models/bookingData.model");
const bookingOrderModel = require("../models/bookingOrder.model");

const fs = require("fs");
const path = require("path");
const AWS = require("aws-sdk");
//configuring the AWS environment
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY
});
var s3 = new AWS.S3();

module.exports.addSpecialization = async (req, res) => {
  try {
    let specializationData = await specializationModel.findOne({
      specializationName: req.body.specializationName
    });
    if (specializationData) {
      return SendResponse(
        res,
        null,
        false,
        "Specialization name already exist."
      );
    } else {
      let imageURL = "";
      if (req.file) {
        var filePath = req.file.path;
        var params = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Body: fs.createReadStream(filePath),
          Key:
            "specializationImages/" +
            Date.now() +
            "_" +
            path.basename(filePath),
          ACL: "public-read"
        };
        imageURL = await uploadImageToS3(params);
        req.body.imageUrl = imageURL;
      }
      let specialization = new specializationModel(req.body);
      specialization = await specialization.save();
      return SendResponse(res, specialization, true, "Specialization created.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateSpecialization = async (req, res) => {
  try {
    let imageURL = "";
    if (req.file) {
      var filePath = req.file.path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key:
          "specializationImages/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      imageURL = await uploadImageToS3(params);
      req.body.imageUrl = imageURL;
    }

    let specializationData = await specializationModel.findOneAndUpdate(
      {
        _id: req.body.specializationId,
        specializationName: { $ne: req.body.specializationName }
      },
      req.body,
      { new: true }
    );
    if (specializationData) {
      return SendResponse(
        res,
        specializationData,
        true,
        "Specialization updated successfully."
      );
    } else {
      return SendResponse(
        res,
        null,
        false,
        "Specialization name already exist."
      );
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.specializationList = async (req, res) => {
  try {
    let options = {
      page: parseInt(req.query.page),
      limit: parseInt(req.query.limit),
      sort: {
        createdAt: -1
      }
    };
    let query = {};
    let { searchKey, searchBody } = req.body;
    if (searchKey == "name") {
      query.specializationName = {
        $regex: searchBody,
        $options: "i"
      };
    } else if (searchKey == "status") {
      if (searchBody == "all") {
      } else query.status = searchBody;
    }
    let specializationList = await specializationModel.paginate(query, options);
    return SendResponse(
      res,
      specializationList,
      true,
      "Specialization List shown."
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteSpecialization = async (req, res) => {
  try {
    let checkSpecializationData = await userProfileDetailModel.findOne({
      specialization: { $in: [req.query.specializationId] }
    });
    if (checkSpecializationData) {
      return SendResponse(
        res,
        null,
        false,
        "You can't delete, this specialization used by artist."
      );
    }
    let specializationData = await specializationModel.findByIdAndDelete({
      _id: req.query.specializationId
    });
    if (specializationData) {
      return SendResponse(
        res,
        specializationData,
        true,
        "Specialization deleted successfully."
      );
    } else {
      return SendResponse(res, null, false, "Specialization not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.addFaq = async (req, res) => {
  try {
    let faqData = await faqModel.findOne({ title: req.body.title });
    if (faqData) {
      return SendResponse(res, null, false, "Title already exist.");
    } else {
      let faq = new faqModel(req.body);
      faq = await faq.save();
      return SendResponse(res, faq, true, "FAQ created.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateFaq = async (req, res) => {
  try {
    let faqData = await faqModel.findByIdAndUpdate(
      { _id: req.body.faqId },
      req.body,
      { new: true }
    );
    if (faqData) {
      return SendResponse(res, faqData, true, "FAQ updated successfully.");
    } else {
      return SendResponse(res, null, false, "FAQ not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.faqList = async (req, res) => {
  try {
    let faqList = await faqModel.find({}).sort({ priorityOrder: 1 });
    return SendResponse(res, faqList, true, "FAQ List shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteFaq = async (req, res) => {
  try {
    let faqData = await faqModel.findByIdAndDelete({ _id: req.query.faqId });
    if (faqData) {
      return SendResponse(res, faqData, true, "FAQ deleted successfully.");
    } else {
      return SendResponse(res, null, false, "FAQ not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.delete = async (req, res) => {
  try {
    let { userId } = req.query;

    let user = await UserModel.findOne({ _id: userId, status: "active" });
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    user = await UserModel.findByIdAndDelete(userId);
    return SendResponse(res, user, true, "User deleted.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.login = async (req, res) => {
  try {
    let { user, password } = req.body;

    let query = {
      userType: "admin"
    };
    query.email = user.toString().toLowerCase();
    query.status = "active";

    user = await UserModel.findOne(query)
      .select("+password")
      .lean();
    if (!user) {
      return SendResponse(res, null, false, "Invalid Credentials");
    } else {
      let model = new UserModel();
      let compare = await model.compare(password, user.password);
      if (compare) {
        delete user.password;
        let token = jwt.sign(
          {
            data: {
              _id: user._id,
              createdAt: new Date(),
              userType: user.userType
            }
          },
          constants.jwtSecret,
          {
            expiresIn: constants.tokenExpiresIn
          }
        );
        user.token = token;
        user.rememberMe = req.body.rememberMe;
        UserModel.findByIdAndUpdate(
          {
            _id: user._id
          },
          {
            token: token,
            rememberMe: req.body.rememberMe
          },
          {
            new: true
          },
          (err, data) => {}
        );

        return SendResponse(res, user, true, "Logged in successfully.");
      } else {
        return SendResponse(res, null, false, "Invalid Credentials.");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.forgetPassword = async function(req, res) {
  try {
    let { email } = req.body;
    let query = {};
    query.email = email;
    user = await UserModel.findOne(query).select(
      "firstName email emailVerified"
    );
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    let emailOtp = util.generateOTP();
    let emailLinkTime = new Date().getTime();
    emailLinkTime = emailLinkTime + 86400000;
    let emailToken = user._id + "~" + emailLinkTime + "~" + emailOtp;
    var link1 = `Hi ${user.firstName},<br> Please click the link to reset your password- http://localhost:3000/reset?emailToken=${emailToken}`;
    let sendMail = await mailService.send(
      req.body.email,
      "Password reset link",
      link1
    );
    UserModel.findByIdAndUpdate(
      {
        _id: user._id
      },
      {
        emailToken: emailOtp,
        emailLinkTime: emailLinkTime
      },
      {
        new: true
      },
      (err, data) => {}
    );
    return SendResponse(res, null, true, "Link sent successfully.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.passwordReset = async function(req, res) {
  try {
    let { password } = req.body;
    var fields = req.body.emailToken.split("~");
    var userId = fields[0];
    let userData = await UserModel.findOne({
      _id: userId,
      status: "active"
    });
    if (userData) {
      let model = new UserModel();
      userData.password = await model.hash(password);
      userData.save();
      return SendResponse(res, null, true, "Password changed successfully.");
    } else return SendResponse(res, null, false, "User not found or blocked.");
  } catch (err) {
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.changePassword = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { oldPassword, newPassword } = req.body;
    let user = await UserModel.findOne({
      _id: userId,
      status: "active"
    }).select("+password");
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    let model = new UserModel();
    let check = await model.compare(oldPassword, user.password);
    if (!check) {
      return SendResponse(res, null, false, "Password is incorrect.");
    } else {
      user.password = await model.hash(newPassword);
      user.save();
      return SendResponse(res, null, true, "Password changed.");
    }
  } catch (err) {
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.userList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { userType, limit, page, searchKey, searchBody } = req.body;
    let query = {};
    if (userType) query.userType = userType;
    if (searchBody) {
      if (searchKey == "name") {
        var nameQuery = searchBody,
          queryArr = nameQuery.split(" "),
          firstName,
          lastName;

        if (queryArr.length == 1) {
          firstName = queryArr[0];
          query.$or = [
            {
              firstName: { $regex: firstName, $options: "i" }
            },
            {
              lastName: { $regex: firstName, $options: "i" }
            }
          ];
        } else if (queryArr.length == 2) {
          firstName = queryArr[0];
          lastName = queryArr[1];

          query.$or = [
            {
              $and: [
                {
                  firstName: { $regex: firstName, $options: "i" }
                },
                {
                  lastName: { $regex: lastName, $options: "i" }
                }
              ]
            },
            {
              $and: [
                {
                  firstName: { $regex: lastName, $options: "i" }
                },
                {
                  lastName: { $regex: firstName, $options: "i" }
                }
              ]
            }
          ];
        }
      }
      if (searchKey == "email") {
        query.$or = [
          {
            email: {
              $regex: searchBody,
              $options: "i"
            }
          }
        ];
      }
      if (searchKey == "status") {
        if (searchBody == "all") {
        } else query.status = searchBody;
      }
    }
    query.$and = [
      {
        status: {
          $ne: "delete"
        },
        _id: {
          $ne: userId
        }
      }
    ];

    let options = {
      page: parseInt(page) || 1,
      limit: parseInt(limit) || 10,
      populate: { path: "profileId ", select: "contactNumber profileImage" },
      sort: {
        createdAt: -1
      }
    };
    const userList = await UserModel.paginate(query, options);
    return SendResponse(res, userList, true, "List Shown");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.blockUser = async (req, res) => {
  try {
    let { userId, action } = req.body;
    let userData = await UserModel.findByIdAndUpdate(
      { _id: userId },
      { $set: { status: action } }
    );
    if (userData) {
      return SendResponse(res, null, true, "Updated successfully");
    } else {
      return SendResponse(res, null, false, "User not found");
    }
  } catch (err) {
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateUser = async (req, res) => {
  try {
    let finalObj = {};
    finalObj = JSON.parse(req.body.data);
    let { userId } = finalObj;

    let userProfileData = await userProfileDetailModel.findOne({
      userId: userId
    });

    const themeData = await themeModel.findOne({ userId: userId });
    if (themeData) finalObj.themeId = themeData._id;
    let newPromiseArr = [];

    //profileImage   .....Checking profile image exist or not
    if (req.files && req.files.profileImage) {
      var filePath = req.files.profileImage[0].path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key: "profileImage/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      newPromiseArr.push(uploadImageToS3(params));
    } else {
      newPromiseArr.push(Promise.resolve(""));
    }

    //coverImage     .....Checking cover image exist or not
    if (req.files && req.files.coverImage) {
      var filePath = req.files.coverImage[0].path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key: "coverImage/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      newPromiseArr.push(uploadImageToS3(params));
    } else {
      newPromiseArr.push(Promise.resolve(""));
    }

    // //drivingLicenceDoc    .....Checking driving licence doc exist or not
    // if (req.files && req.files.drivingLicenceDoc) {
    //   var filePath = req.files.drivingLicenceDoc[0].path;
    //   var params = {
    //     Bucket: process.env.AWS_BUCKET_NAME,
    //     Body: fs.createReadStream(filePath),
    //     Key: "drivingLicenceDoc/" + Date.now() + "_" + path.basename(filePath),
    //     ACL: "public-read"
    //   };
    //   newPromiseArr.push(uploadImageToS3(params));
    // } else {
    //   newPromiseArr.push(Promise.resolve(""));
    // }

    //tattooLicenceDoc    .....Checking tattoo licence doc exist or not
    if (req.files && req.files.tattooLicenceDoc) {
      var filePath = req.files.tattooLicenceDoc[0].path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key: "tattooLicenceDoc/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      newPromiseArr.push(uploadImageToS3(params));
    } else {
      newPromiseArr.push(Promise.resolve(""));
    }

    //gallery     .....Checking gallery image exist or not
    if (req.files && req.files.gallery) {
      if (req.files.gallery.length >= 1) {
        let promiseArr = [];
        for (let i = 0; i <= req.files.gallery.length - 1; i++) {
          var filePath = req.files.gallery[i].path;
          var params = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Body: fs.createReadStream(filePath),
            Key: "gallery/" + Date.now() + "_" + path.basename(filePath),
            ACL: "public-read"
          };
          promiseArr.push(uploadImageToS3(params));
        }
        newPromiseArr.push(Promise.all(promiseArr));
      }
    } else {
      newPromiseArr.push(Promise.resolve([]));
    }

    // .....Resolving all the promises
    let finalImageArr = await Promise.all(newPromiseArr);

    if (finalImageArr[0]) {
      //    .....checking profileImage is in request or not
      finalObj.profileImage = finalImageArr[0];
    }
    if (finalImageArr[1]) {
      //       .....checking coverImage is in request or not
      finalObj.coverImage = finalImageArr[1];
    }
    // if (finalImageArr[2]) {
    //   //       ......checking drivingLicenceDoc is in request or not
    //   finalObj.drivingLicenceDoc = finalImageArr[2];
    // }
    if (finalImageArr[2]) {
      //         ......checking tattooLicenceDoc is in request or not
      finalObj.tattooLicenceDoc = finalImageArr[2];
    }
    if (finalImageArr[3].length > 0) {
      //       ....checking gallery is in request or not
      for (let i of finalImageArr[3]) {
        finalObj.gallery.push(i);
      }
    }

    let { firstName, lastName, userName, status, password } = finalObj;
    var userData = await UserModel.findOne({
      _id: userId
    });

    if (!userData) return SendResponse(res, null, false, "User not found.");
    else {
      let checkUserName = await UserModel.findOne({
        userName: userName,
        _id: { $ne: userData._id }
      });
      if (checkUserName) {
        return SendResponse(res, null, false, "User name already exist");
      }
      let model = new UserModel();
      if (password) {
        password = await model.hash(password);
        userData = await UserModel.findOneAndUpdate(
          {
            _id: userId
          },
          {
            $set: {
              password: password
            }
          },
          {
            new: true
          }
        );
      }
      userData = await UserModel.findOneAndUpdate(
        {
          _id: userId
        },
        {
          $set: {
            firstName: firstName,
            lastName: lastName,
            userName: userName,
            status: status
          }
        },
        {
          new: true
        }
      );
      if (userProfileData) {
        let user = await userProfileDetailModel.findOneAndUpdate(
          {
            userId: userId
          },
          finalObj,
          {
            new: true
          }
        );
        user.set("firstName", userData.firstName, {
          strict: false
        });
        user.set("lastName", userData.lastName, {
          strict: false
        });
        user.set("userName", userData.userName, {
          strict: false
        });
        user.set("status", userData.status, {
          strict: false
        });
        return SendResponse(res, user, true, "User Updated.");
      } else {
        finalObj.userId = userId;
        let user = new userProfileDetailModel(finalObj);
        user = await user.save();
        let updateProfileId = await UserModel.findOneAndUpdate(
          {
            _id: userId
          },
          {
            $set: {
              profileId: user._id
            }
          },
          {
            new: true
          }
        );
        user.set("firstName", userData.firstName, {
          strict: false
        });
        user.set("lastName", userData.lastName, {
          strict: false
        });
        return SendResponse(res, user, true, "User Updated.");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.userDetails = async (req, res) => {
  try {
    let { userId } = req.query;
    let userData = await UserModel.findOne({ _id: userId }).populate({
      path: "profileId",
      populate: {
        path: "specialization",
        select: " specializationName imageUrl"
      }
    });
    if (userData) {
      return SendResponse(res, userData, true, "Shown successfully");
    } else {
      return SendResponse(res, null, false, "User not found");
    }
  } catch (err) {
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.addSubscription = async (req, res) => {
  try {
    let subscriptionData = await subscriptionModel.findOne({
      name: req.body.name,
      userType: req.body.userType
    });
    if (subscriptionData) {
      return SendResponse(res, null, false, "Subscription name already exist.");
    } else {
      let subscription = new subscriptionModel(req.body);
      subscription = await subscription.save();
      return SendResponse(res, subscription, true, "Subscription created.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.subscriptionList = async (req, res) => {
  try {
    let { limit, page } = req.query;
    let options = {
      limit: parseInt(limit),
      page: parseInt(page),
      sort: {
        createdAt: -1
      }
    };
    let query = {};
    let { searchKey, searchBody } = req.body;
    if (searchKey == "name") {
      query.name = {
        $regex: searchBody,
        $options: "i"
      };
    } else if (searchKey == "status") {
      if (searchBody == "all") {
      } else query.status = searchBody;
    } else if (searchKey == "userType") {
      query.userType = searchBody;
    }
    let subscriptionList = await subscriptionModel.paginate(query, options);
    return SendResponse(
      res,
      subscriptionList,
      true,
      "Subscription list shown successfully."
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteSubscription = async (req, res) => {
  try {
    let checkSubscriptionData = await userSubscriptionModel.findOne({
      subscriptionId: req.query.subscriptionId
    });
    if (checkSubscriptionData) {
      return SendResponse(
        res,
        null,
        false,
        "You can't delete this, subscription purchased by users."
      );
    }
    let subscriptionData = await subscriptionModel.findByIdAndDelete({
      _id: req.query.subscriptionId
    });
    if (subscriptionData) {
      return SendResponse(
        res,
        subscriptionData,
        true,
        "Subscription deleted successfully."
      );
    } else {
      return SendResponse(res, null, false, "Subscription not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.subscriptionDetails = async (req, res) => {
  try {
    let { subscriptionId } = req.query;

    let subscriptionDetails = await subscriptionModel.findOne({
      _id: subscriptionId
    });
    if (subscriptionDetails) {
      return SendResponse(
        res,
        subscriptionList,
        true,
        "Subscription list shown successfully."
      );
    } else {
      return SendResponse(res, null, false, "No subscription found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateSubscription = async (req, res) => {
  try {
    let { subscriptionId } = req.body;
    let subscriptionDetails = await subscriptionModel.findOneAndUpdate(
      { _id: subscriptionId },
      req.body,
      { new: true }
    );
    if (subscriptionDetails) {
      return SendResponse(
        res,
        subscriptionDetails,
        true,
        "Subscription updated successfully"
      );
    } else {
      return SendResponse(res, Boom.badImplementation());
    }
  } catch (err) {}
};

module.exports.ticketList = async (req, res) => {
  try {
    let userType = req.decoded.data.userType;
    let { status, limit, page } = req.query;
    if (userType != "admin") {
      return SendResponse(res, null, false, "Unauthorized access");
    }
    let query = {};
    if (status) {
      query = { status: status };
    }
    let options = {
      limit: parseInt(limit) || 10,
      page: parseInt(page) || 1,
      populate: { path: "userId", select: "firstName lastName" },
      sort: {
        createdAt: -1
      }
    };
    let ticketList = await supportModel.paginate(query, options);
    return SendResponse(res, ticketList, true, "List shown successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteTicket = async (req, res) => {
  try {
    let { ticketId } = req.query;
    if (req.decoded.data.userType !== "admin") {
      return SendResponse(res, null, false, "Only admin can delete a ticket.");
    }
    let ticketData = await supportModel.findOneAndDelete({ _id: ticketId });
    if (ticketData != null) {
      return SendResponse(res, null, true, "Ticket deleted successfully.");
    } else {
      return SendResponse(res, null, false, "Ticket not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.userCount = async (req, res) => {
  try {
    let clientCount = await UserModel.find({
      userType: "client",
      status: "active"
    }).count();
    let artistCount = await UserModel.find({
      userType: "artist",
      status: "active"
    }).count();
    let studioCount = await UserModel.find({
      userType: "studio",
      status: "active"
    }).count();
    let obj = {
      clientCount,
      artistCount,
      studioCount
    };
    return SendResponse(res, obj, true, "Data shown successfully.");
  } catch (error) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.bookingCount = async (req, res) => {
  try {
    let { startDate, endDate } = req.query;
    let bookingData = await bookingModel
      .find({ date: { $gte: startDate, $lte: endDate } })
      .populate("bookingOrderId", null, { paymentStatus: "completed" })
      .count();
    return SendResponse(res, bookingData, true, "Data shown successfully.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.studioBookingList = async (req, res) => {
  try {
    {
      aggregate.push({
        $project: {
          date: { $arrayElemAt: ["$bookingData.date", 0] },
          bookingId: { $arrayElemAt: ["$bookingData._id", 0] },
          artistFirstName: { $arrayElemAt: ["$artistData.firstName", 0] },
          artistLastName: { $arrayElemAt: ["$artistData.lastName", 0] },
          clientFirstName: {
            $arrayElemAt: ["$clientData.firstName", 0]
          },
          clientLastName: { $arrayElemAt: ["$clientData.lastName", 0] },
          artistContactNumber: {
            $arrayElemAt: ["$artistProfileData.contactNumber", 0]
          },
          clientContactNumber: {
            $arrayElemAt: ["$clientProfileData.contactNumber", 0]
          },
          artistAddress: {
            $arrayElemAt: ["$artistProfileData.address", 0]
          },
          clientAddress: {
            $arrayElemAt: ["$clientProfileData.address", 0]
          },
          totalPrice: { $arrayElemAt: ["$bookingData.amount", 0] }
        }
      });

      aggregate.push({
        $facet: {
          data: [
            { $sort: { createdAt: -1 } },
            { $skip: parseInt(page) },
            { $limit: parseInt(limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });

      let bookingSlotList = await BookingOrder.aggregate(aggregate);

      let data = bookingSlotList[0].data;
      let finalResult = {};
      if (data.length > 0) {
        finalResult = {
          data: data,
          count: bookingSlotList[0].pageInfo[0].count
        };
      } else {
        finalResult = {
          data: [],
          count: 0
        };
      }

      return SendResponse(res, finalResult, true, "List shown");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.bookingList = async (req, res) => {
  try {
    let { limit, page, searchKey, searchBody, type, date } = req.query;

    let aggregate = [];

    aggregate.push({
      $lookup: {
        from: "bookingdatas",
        localField: "bookingDataId",
        foreignField: "_id",
        as: "bookingData"
      }
    });

    let matchBookingData = {};

    if (type == "upcoming") {
      matchBookingData = {
        $match: { "bookingData.date": { $gt: date } }
      };
    } else if (type == "current") {
      matchBookingData = {
        $match: { "bookingData.date": { $eq: date } }
      };
    } else {
      matchBookingData = {
        $match: { "bookingData.date": { $lt: date } }
      };
    }

    if (searchKey == "date") {
      matchBookingData.$match.$and = [{ "bookingData.date": searchBody }];
    }

    //1
    aggregate.push(matchBookingData);

    //3
    aggregate.push({
      $lookup: {
        from: "users",
        localField: "artistId",
        foreignField: "_id",
        as: "artistData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "users",
        localField: "clientId",
        foreignField: "_id",
        as: "clientData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "users",
        localField: "studioId",
        foreignField: "_id",
        as: "studioData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "profiledetails",
        localField: "artistData.profileId",
        foreignField: "_id",
        as: "artistProfileData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "profiledetails",
        localField: "clientData.profileId",
        foreignField: "_id",
        as: "clientProfileData"
      }
    });

    aggregate.push({
      $lookup: {
        from: "profiledetails",
        localField: "studioData.profileId",
        foreignField: "_id",
        as: "studioProfileData"
      }
    });

    matchArtistData = {};
    if (searchBody) {
      if (searchKey == "name") {
        var nameQuery = searchBody,
          queryArr = nameQuery.split(" "),
          firstName,
          lastName;

        if (queryArr.length == 1) {
          firstName = queryArr[0];
          matchArtistData = {
            $or: [
              {
                "artistData.firstName": { $regex: firstName, $options: "i" }
              },
              {
                "clientData.firstName": { $regex: firstName, $options: "i" }
              },
              {
                "studioData.firstName": { $regex: firstName, $options: "i" }
              }
            ]
          };
        } else if (queryArr.length == 2) {
          firstName = queryArr[0];
          lastName = queryArr[1];
          matchArtistData.$or = [
            {
              $and: [
                {
                  $or: [
                    {
                      "artistData.firstName": {
                        $regex: firstName,
                        $options: "i"
                      },
                      "artistData.lastName": {
                        $regex: lastName,
                        $options: "i"
                      }
                    },
                    {
                      "clientData.firstName": {
                        $regex: firstName,
                        $options: "i"
                      },
                      "clientData.lastName": {
                        $regex: lastName,
                        $options: "i"
                      }
                    },
                    {
                      "studioData.firstName": {
                        $regex: firstName,
                        $options: "i"
                      },
                      "studioData.lastName": { $regex: lastName, $options: "i" }
                    }
                  ]
                }
              ]
            },
            {
              $and: [
                {
                  $or: [
                    {
                      "artistData.firstName": {
                        $regex: lastName,
                        $options: "i"
                      }
                    },
                    {
                      "clientData.firstName": {
                        $regex: lastName,
                        $options: "i"
                      }
                    },
                    {
                      "studioData.firstName": {
                        $regex: lastName,
                        $options: "i"
                      }
                    }
                  ]
                },
                {
                  $or: [
                    {
                      "artistData.lastName": {
                        $regex: firstName,
                        $options: "i"
                      }
                    },
                    {
                      "clientData.lastName": {
                        $regex: firstName,
                        $options: "i"
                      }
                    },
                    {
                      "studioData.lastName": {
                        $regex: firstName,
                        $options: "i"
                      }
                    }
                  ]
                }
              ]
            }
          ];
        }

        aggregate.push({ $match: matchArtistData });
      }
      if (searchKey == "contactNumber") {
        let contactNumber = searchBody;
        matchArtistData = {
          $or: [
            {
              "artistProfileData.contactNumber": {
                $regex: contactNumber,
                $options: "i"
              }
            },
            {
              "clientProfileData.contactNumber": {
                $regex: contactNumber,
                $options: "i"
              }
            },
            {
              "studioProfileData.contactNumber": {
                $regex: contactNumber,
                $options: "i"
              }
            }
          ]
        };
        aggregate.push({ $match: matchArtistData });
      }
    }

    aggregate.push({
      $project: {
        date: { $arrayElemAt: ["$bookingData.date", 0] },
        bookingId: { $arrayElemAt: ["$bookingData._id", 0] },
        artistFirstName: { $arrayElemAt: ["$artistData.firstName", 0] },
        artistLastName: { $arrayElemAt: ["$artistData.lastName", 0] },
        clientFirstName: { $arrayElemAt: ["$clientData.firstName", 0] },
        clientLastName: { $arrayElemAt: ["$clientData.lastName", 0] },
        studioFirstName: { $arrayElemAt: ["$studioData.firstName", 0] },
        studioLastName: { $arrayElemAt: ["$studioData.lastName", 0] },
        artistContactNumber: {
          $arrayElemAt: ["$artistProfileData.contactNumber", 0]
        },
        clientContactNumber: {
          $arrayElemAt: ["$clientProfileData.contactNumber", 0]
        },
        studioContactNumber: {
          $arrayElemAt: ["$studioProfileData.contactNumber", 0]
        },
        artistAddress: {
          $arrayElemAt: ["$artistProfileData.address", 0]
        },
        clientAddress: {
          $arrayElemAt: ["$clientProfileData.address", 0]
        },
        studioAddress: { $arrayElemAt: ["$studioProfileData.address", 0] },
        totalPrice: { $arrayElemAt: ["$bookingData.amount", 0] },
        createdAt: 1
      }
    });

    if (req.query.sorting == "artistName") {
      aggregate.push({
        $facet: {
          data: [
            { $sort: { artistFirstName: 1 } },
            { $skip: parseInt(page) },
            { $limit: parseInt(limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });
    } else if (req.query.sorting == "studioName") {
      aggregate.push({
        $facet: {
          data: [
            { $sort: { studioFirstName: 1 } },
            { $skip: parseInt(page) },
            { $limit: parseInt(limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });
    } else {
      aggregate.push({
        $facet: {
          data: [
            { $sort: { createdAt: -1 } },
            { $skip: parseInt(page) },
            { $limit: parseInt(limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });
    }

    let bookingSlotList = await bookingOrderModel.aggregate(aggregate);
    let data = bookingSlotList[0].data;
    let finalResult = {};
    if (data.length > 0) {
      finalResult = {
        data: data,
        count: bookingSlotList[0].pageInfo[0].count
      };
    } else {
      finalResult = {
        data: [],
        count: 0
      };
    }

    return SendResponse(res, finalResult, true, "List shown");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.bookingDetails = async (req, res) => {
  try {
    let { bookingId } = req.query;
    let bookingData = await bookingModel.findOne({ _id: bookingId }).populate([
      {
        path: "artistId",
        select: "firstName lastName email",
        populate: { path: "profileId", select: { _id: 0, contactNumber: 1 } }
      },
      {
        path: "artistId",
        select: "firstName lastName email",
        populate: { path: "profileId", select: { _id: 0, contactNumber: 1 } }
      },
      {
        path: " bookingOrderId",
        select:
          "paymentStatus bookingStatus bookingId paymentMode totalAmount transactionId slots"
      }
    ]);
    if (bookingData) {
      if (bookingData.artistId.profileId.contactNumber)
        bookingData.set(
          "contactNumber",
          bookingData.artistId.profileId.contactNumber,
          {
            strict: false
          }
        );
      bookingData.artistId.contactNumber =
        bookingData.artistId.profileId.contactNumber;
      return SendResponse(res, bookingData, true, "Data shown");
    } else {
      return SendResponse(res, null, false, "No booking data");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.cancelBooking = async (req, res) => {
  try {
    let { bookingId, status } = req.body;
    let bookingData = await bookingModel.findOneAndUpdate(
      { _id: bookingId, bookingStatus: "pending" },
      { $set: { bookingStatus: status } },
      { new: true }
    );
    let updatedBookingOrderData = await bookingOrderModel.findOneAndUpdate(
      { bookingDataId: bookingId, bookingStatus: "pending" },
      { $set: { bookingStatus: status, paymentStatus: status } },
      { new: true }
    );
    if (!bookingData) {
      return SendResponse(res, null, false, "No booking found");
    }
    return SendResponse(res, null, true, "Booking cancelled successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.subscriptionUserList = async (req, res) => {
  try {
    let { date, page, limit, userType, listType } = req.query;
    let aggregate = userSubscriptionModel.aggregate();

    const fromDate = new Date(date);
    fromDate.setHours(0, 0, 0, 0);
    if (listType == "trialExpiredUser")
      aggregate.match({
        subscriptionType: "trial",
        endDate: { $lte: fromDate }
      });
    else if (listType == "activeTrialUser")
      aggregate.match({
        subscriptionType: "trial",
        endDate: { $gte: fromDate }
      });
    else if (listType == "paidExpiredUser")
      aggregate.match({
        subscriptionType: "paid",
        endDate: { $lte: fromDate }
      });
    else if (listType == "activePaidUser")
      aggregate.match({
        subscriptionType: "paid",
        endDate: { $gte: fromDate }
      });
    aggregate.lookup({
      from: "users",
      localField: "userId",
      foreignField: "_id",
      as: "userData"
    });
    aggregate.lookup({
      from: "subscriptions",
      localField: "subscriptionId",
      foreignField: "_id",
      as: "subscriptionData"
    });
    aggregate.match({ "userData.userType": userType });
    aggregate.project({
      purchaseDate: 1,
      subscriptionType: 1,
      startDate: 1,
      endDate: 1,
      firstName: { $arrayElemAt: ["$userData.firstName", 0] },
      lastName: { $arrayElemAt: ["$userData.lastName", 0] },
      email: { $arrayElemAt: ["$userData.email", 0] },
      subscriptionStatus: { $arrayElemAt: ["$subscriptionData.status", 0] },
      subscriptionType: {
        $arrayElemAt: ["$subscriptionData.subscriptionType", 0]
      },
      trialDays: { $arrayElemAt: ["$subscriptionData.trialDays", 0] },
      subscriptionName: { $arrayElemAt: ["$subscriptionData.name", 0] },
      subscriptionDescription: {
        $arrayElemAt: ["$subscriptionData.description", 0]
      },
      subscriptionDuration: { $arrayElemAt: ["$subscriptionData.duration", 0] },
      subscriptionAmount: { $arrayElemAt: ["$subscriptionData.amount", 0] },
      subscriptionUserType: { $arrayElemAt: ["$subscriptionData.userType", 0] }
    });

    let option = {
      limit: parseInt(limit) || 10,
      page: parseInt(page) || 1,
      sort: { createdAt: -1 }
    };
    let dataList = await userSubscriptionModel.aggregatePaginate(
      aggregate,
      option
    );

    return SendResponse(res, dataList, true, "List shown");
  } catch (err) {
    console.log(err);
    SendResponse(res, Boom.badImplementation());
  }
};

function uploadImageToS3(params) {
  return new Promise((resolve, reject) => {
    s3.upload(params, function(err, data) {
      if (err) {
        console.log("Error", err);
        reject(err);
      }
      if (data) {
        resolve(data.Location);
        console.log("Uploaded in:", data.Location);
      }
    });
  });
}

const Boom = require("boom");
const UserModel = require("../models/user.model");
const AssociationModel = require("../models/associationManagement.model");
const enquiryModel = require("../models/enquiry.model");
const SendResponse = require("../apiHandler");
require("dotenv").config();
process.env.NODE_ENV = "local";
const fs = require("fs");
const path = require("path");
const AWS = require("aws-sdk");
//configuring the AWS environment
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY
});
var s3 = new AWS.S3();

module.exports.saveEnquiry = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    req.body.data = JSON.parse(req.body.data);
    req.body.data.userId = userId;
    let promiseArr = [];

    if (req.files.length >= 1) {
      for (let i = 0; i <= req.files.length - 1; i++) {
        var filePath = req.files[i].path;
        var params = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Body: fs.createReadStream(filePath),
          Key: "referenceImages/" + Date.now() + "_" + path.basename(filePath),
          ACL: "public-read"
        };
        promiseArr.push(uploadImageToS3(params));
      }
    }
    let finalImageArr = await Promise.all(promiseArr);
    req.body.data.referenceImages = finalImageArr;

    let user = await UserModel.findOne({
      _id: userId,
      status: "active",
      userType: "client"
    });
    if (!user) {
      return SendResponse(res, null, false, "Client not found.");
    }
    let enquiryData = new enquiryModel(req.body.data);
    enquiryData = await enquiryData.save();
    return SendResponse(res, enquiryData, true, "Enquiry Data saved.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.sendEnquiryMessage = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { enquiryId, message, by } = req.body;
    let enquiryData = await enquiryModel.findOne({ _id: enquiryId });
    if (!enquiryData) {
      return SendResponse(res, false, null, "Enquiry not found.");
    }
    let reply = {
      by: by,
      message
    };
    enquiryData.conversation.push(reply);
    enquiryData = await enquiryData.save();
    enquiryData = await enquiryModel.findOne({ _id: enquiryId }).populate({
      path: "userId",
      select: "firstName lastName profileId",
      populate: { path: "profileId", select: "profileImage" }
    });
    return SendResponse(
      res,
      enquiryData,
      true,
      "Enquiry updated with new message successfully."
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.enquiryList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let userType = req.decoded.data.userType;
    let enquiryList = [];
    if (userType == "artist") {
      let options = {
        limit: JSON.parse(req.query.limit),
        page: JSON.parse(req.query.page),
        populate: {
          path: "userId",
          select: "profileId firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        },
        sort: { createdAt: -1 }
      };
      enquiryList = await enquiryModel.paginate({ artistId: userId }, options);
    } else if (userType == "studio") {
      let options = {
        limit: JSON.parse(req.query.limit),
        page: JSON.parse(req.query.page),
        populate: {
          path: "userId",
          select: "profileId firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        },
        sort: { createdAt: -1 }
      };
      enquiryList = await enquiryModel.paginate({ studioId: userId }, options);
    } else {
      let options = {
        limit: JSON.parse(req.query.limit),
        page: JSON.parse(req.query.page),
        populate: [
          {
            path: "artistId",
            select: "profileId firstName lastName",
            populate: { path: "profileId", select: "profileImage" }
          },
          {
            path: "studioId",
            select: "profileId firstName lastName",
            populate: { path: "profileId", select: "profileImage" }
          },
          {
            path: "refArtistId",
            select: "profileId firstName lastName",
            populate: { path: "profileId", select: "profileImage" }
          }
        ],
        sort: { createdAt: -1 }
      };
      enquiryList = await enquiryModel.paginate({ userId: userId }, options);
    }
    return SendResponse(res, enquiryList, true, "List Shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getStudioArtistList = async (req, res) => {
  try {
    let userId = req.query.userId;

    let user = await UserModel.findOne({ _id: userId, status: "active" });
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    } else {
      let requestList = await AssociationModel.findOne(
        { userId: userId },
        { joinedRequest: 1, _id: 0 }
      ).populate({
        path: "joinedRequest",
        select: "firstName lastName profileId",
        populate: { path: "profileId", select: "profileImage" }
      });

      if (!requestList || requestList.joinedRequest.length == 0) {
        return SendResponse(res, null, true, "No data found.");
      } else {
        return SendResponse(res, requestList, true, "List Shown.");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateEnquiryStatus = async (req, res) => {
  try {
    let { enquiryId, status } = req.body;
    if (req.decoded.data.userType == "client") {
      return SendResponse(res, null, false, "Client can't change the status");
    }
    let enquiryData = await enquiryModel.findOne({ _id: enquiryId });
    if (!enquiryData) {
      return SendResponse(res, null, false, "Enquiry not found.");
    }
    enquiryData = await enquiryModel.findOneAndUpdate(
      { _id: enquiryId },
      { status },
      { new: true }
    );
    return SendResponse(res, enquiryData, true, "Enquiry updated.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteEnquiry = async (req, res) => {
  try {
    let { enquiryId } = req.query;
    if (req.decoded.data.userType == "client") {
      return SendResponse(res, null, false, "Client can't delete a enquiry.");
    }
    let enquiryData = await enquiryModel.findOneAndDelete({ _id: enquiryId });
    if (enquiryData != null) {
      return SendResponse(res, null, true, "Enquiry deleted successfully.");
    } else {
      return SendResponse(res, null, false, "Enquiry not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.deleteEnquiryMessage = async (req, res) => {
  try {
    let { enquiryId, messageId } = req.query;
    if (req.decoded.data.userType === "client") {
      return SendResponse(res, null, false, "Client can't delete the message");
    }
    let query = {
      _id: enquiryId,
      conversation: {
        $elemMatch: {
          _id: messageId
        }
      }
    };
    let enquiryData = await enquiryModel.findOneAndUpdate(
      query,
      { $pull: { conversation: query.conversation.$elemMatch } },
      { new: true }
    );
    if (enquiryData === null) {
      return SendResponse(res, null, false, "Enquiry or message not found.");
    }
    return SendResponse(res, enquiryData, true, "Message Deleted.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getEnquiryDetails = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { enquiryId } = req.query;
    let enquiryData = await enquiryModel.findOne({ _id: enquiryId }).populate([
      {
        path: "userId",
        select: "firstName lastName profileId",
        populate: { path: "profileId", select: "profileImage" }
      },
      {
        path: "artistId",
        select: "profileId firstName lastName",
        populate: { path: "profileId", select: "profileImage" }
      },
      {
        path: "studioId",
        select: "profileId firstName lastName",
        populate: { path: "profileId", select: "profileImage" }
      },
      {
        path: "refArtistId",
        select: "profileId firstName lastName",
        populate: { path: "profileId", select: "profileImage" }
      }
    ]);
    if (enquiryData)
      return SendResponse(res, enquiryData, true, "Enquiry fetched.");
    else {
      return SendResponse(res, null, false, "Enquiry not found.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

function uploadImageToS3(params) {
  return new Promise((resolve, reject) => {
    s3.upload(params, function(err, data) {
      if (err) {
        console.log("Error", err);
        reject(err);
      }
      if (data) {
        resolve(data.Location);
        console.log("Uploaded in:", data.Location);
      }
    });
  });
}

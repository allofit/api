const afterCareModel = require("../models/afterCareInstruction.model");
const termsAndConditionModel = require("../models//termsAndCondition.model.js");
const consentModel = require("../models/consent.model.js");
const SendResponse = require("../apiHandler");
const profileDetailsModel = require("../models/profileDetails.model");

module.exports.afterCareData = async (req, res) => {
  try {
    let { afterCareId, text } = req.body;
    let afterCareData;
    if (afterCareId) {
      afterCareData = afterCareModel.findOneAndUpdate(
        { _id: afterCareId },
        { $set: { text: text } },
        { new: true }
      );
      return SendResponse(
        res,
        afterCareData,
        true,
        "Data updated successfully"
      );
    } else {
      let obj = { text };
      afterCareData = new afterCareModel(obj);
      afterCareData = await afterCareData.save();
    }
    return SendResponse(res, afterCareData, true, "Data added successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.termsAndConditionData = async (req, res) => {
  try {
    let { termsAndConditionId, text } = req.body;
    let termsAndConditionData;
    if (termsAndConditionId) {
      termsAndConditionData = termsAndConditionModel.findOneAndUpdate(
        { _id: termsAndConditionId },
        { $set: { text: text } },
        { new: true }
      );
      return SendResponse(
        res,
        termsAndConditionData,
        true,
        "Data updated successfully"
      );
    } else {
      let obj = { text };
      termsAndConditionData = new termsAndConditionModel(obj);
      termsAndConditionData = await termsAndConditionData.save();
    }
    return SendResponse(
      res,
      termsAndConditionData,
      true,
      "Data added successfully"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.consentData = async (req, res) => {
  try {
    let { consentId, text } = req.body;
    let consentData;
    if (consentId) {
      consentData = consentModel.findOneAndUpdate(
        { _id: consentId },
        { $set: { text: text } },
        { new: true }
      );
      return SendResponse(res, consentData, true, "Data updated successfully");
    } else {
      let obj = { text };
      consentData = new consentModel(obj);
      consentData = await consentData.save();
    }
    return SendResponse(res, consentData, true, "Data added successfully");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getAfterCareData = async (req, res) => {
  try {
    let { afterCareId } = req.query;
    let userType = req.decoded.data.userType;
    let artistId = req.decoded.data._id;
    let afterCareData = afterCareModel.findOne({ _id: afterCareId });
    if (userType == "artist") {
      let userAfterCareData = profileDetailsModel.findOne(
        { userId: artistId },
        "afterCareData"
      );
      if (userAfterCareData.afterCareData) {
        return SendResponse(res, userAfterCareData, "Data shown successfully");
      } else {
        return SendResponse(res, afterCareData, "Data shown successfully");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getTermsAndConditionData = async (req, res) => {
  try {
    let { termsAndConditionId } = req.query;
    let userType = req.decoded.data.userType;
    let artistId = req.decoded.data._id;
    let termsAndConditionData = termsAndConditionModel.findOne({
      _id: termsAndConditionId
    });
    if (userType == "artist") {
      let userTermsAndConditionData = profileDetailsModel.findOne(
        { userId: artistId },
        "termsAndConditionData"
      );
      if (userTermsAndConditionData.termsAndConditionData) {
        return SendResponse(
          res,
          userTermsAndConditionData,
          "Data shown successfully"
        );
      } else {
        return SendResponse(
          res,
          termsAndConditionData,
          "Data shown successfully"
        );
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getConsentData = async (req, res) => {
  try {
    let { consentId } = req.query;
    let userType = req.decoded.data.userType;
    let artistId = req.decoded.data._id;
    let consentData = consentModel.findOne({
      _id: consentId
    });
    if (userType == "artist") {
      let userConsentData = profileDetailsModel.findOne(
        { userId: artistId },
        "consentData"
      );
      if (userConsentData.ConsentData) {
        return SendResponse(res, userConsentData, "Data shown successfully");
      } else {
        return SendResponse(res, consentData, "Data shown successfully");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

const Boom = require("boom");
const UserModel = require("../models/user.model");
const profileModel = require("../models/profileDetails.model");
const paymentModel = require("../models/payment.model");
const bookingModel = require("../models/bookingData.model");
const bookingOrderModel = require("../models/bookingOrder.model");
const subscriptionPaymentModel = require("../models/subscriptionPayment.model");
const SendResponse = require("../apiHandler");

const keySecret = "sk_test_SsUZ9v9xZg3ZUWeH4qYopX5N00GZTelm0a";

// Note:-
//Please share your stripe published Key with your frond End developer //

const stripe = require("stripe")(keySecret);

module.exports = {
  addCustomerAndAccount: async (req, res) => {
    try {
      let bankAccDetails = {
        type: "custom",
        country: req.body.country,
        email: req.body.email,
        requested_capabilities: ["card_payments", "transfers"],
        business_type: "individual",
        business_profile: {
          mcc: 7333,
          url: "https://app.allofit.co"
        },
        individual: {
          email: req.body.email,
          first_name: req.body.firstName,
          last_name: req.body.lastName,
          phone: req.body.phoneNumber,
          verification: {
            document: {
              back: req.body.backUrl,
              front: req.body.frontUrl
            },
            additional_document: {
              back: req.body.backUrl,
              front: req.body.frontUrl
            }
          },
          dob: {
            day: req.body.birthDay,
            month: req.body.birthMonth,
            year: req.body.birthYear
          },
          address: {
            city: req.body.city,
            country: req.body.country,
            line1: req.body.address,
            postal_code: req.body.zip,
            state: req.body.state
          }
        },
        settings: {
          payments: {
            statement_descriptor: "ALLOFIT INC."
          },
          payouts: {
            debit_negative_balances: true,
            schedule: {
              delay_days: 2,
              interval: "daily"
            },
            statement_descriptor: "ALLOFIT INC."
          }
        },
        external_account: {
          object: "bank_account",
          country: "AU",
          currency: "aud",
          account_holder_name: `${req.body.firstName}  ${req.body.lastName}`,
          account_holder_type: "individual",
          routing_number: req.body.routingNumber,
          account_number: req.body.accountNumber
        }
      };
      let account = await stripe.accounts.create(bankAccDetails);
      let data = await profileModel.findOneAndUpdate(
        { userId: req.decoded.data._id },
        {
          $set: {
            stripeServiceAgreement: req.body.serviceAgreement,
            stripeAccountId: account.id,
            stripeBankId: account.external_accounts.data[0].id
          }
        },
        { new: true }
      );
      let updateAccount = account;
      if (req.body.serviceAgreement) {
        updateAccount = await stripe.accounts.update(account.id, {
          tos_acceptance: {
            date: Math.floor(Date.now() / 1000),
            ip: req.connection.remoteAddress // Assumes you're not using a proxy
          }
        });
      }
      res.send({
        responseCode: 200,
        responseMessage: "Account created successful",
        data: updateAccount
      });
    } catch (err) {
      console.log(err);
      res.send({
        responseCode: 400,
        responseMessage: "Internal server error",
        data: err
      });
    }
  },

  paymentForBooking: async (req, res) => {
    try {
      let stripeToken = req.stripeToken;
      let userData = await UserModel.findOne({
        _id: req.senderId,
        status: "active"
      });

      let stripeCustomerData = await stripe.customers.create({
        email: userData.email,
        source: stripeToken
      });
      let chargesData = await stripe.charges.create({
        amount: req.amount,
        currency: "aud",
        customer: stripeCustomerData.id,
        description: req.description
      });
      let updateBookingData = await bookingModel.findOneAndUpdate(
        { _id: req.bookingId },
        { $set: { bookingStatus: "completed" } },
        { new: true }
      );
      let updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
        { bookingDataId: req.bookingId },
        {
          $set: {
            bookingStatus: "completed",
            paymentStatus: "partialCompleted"
          }
        },
        { new: true }
      );

      var obj = {
        transactionId: chargesData.balance_transaction,
        chargeId: chargesData.id,
        amount: chargesData.amount,
        amount_refunded: chargesData.amount_refunded,
        customerId: chargesData.customer,
        url: chargesData.receipt_url,
        email: stripeCustomerData.email,
        transactionStatus: chargesData.status,
        senderId: req.senderId,
        receiverId: updateBookingOrders.studioId
          ? updateBookingOrders.studioId
          : updateBookingOrders.artistId,
        bookingId: updateBookingOrders._id
      };

      var obj1 = new paymentModel(obj);
      let paymentData = await obj1.save();
      updateBookingData = await bookingModel.findOneAndUpdate(
        { _id: req.bookingId },
        { $set: { bookingStatus: "completed" } },
        { new: true }
      );
      updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
        { bookingDataId: req.bookingId },
        {
          $set: {
            paymentId: paymentData._id,
            transactionId: paymentData.transactionId
          }
        },
        { new: true }
      );
      var data = {
        paymentId: paymentData._id,
        transactionId: paymentData.transactionId,
        transactionStatus: paymentData.transactionStatus,
        amount: paymentData.amount,
        createdAt: paymentData.createdAt,
        url: paymentData.url
      };
      return {
        responseCode: 200,
        responseMessage: "Payment successful",
        data: data
      };
    } catch (error) {
      console.log(error);
      let updateBookingData = await bookingModel.findOneAndUpdate(
        { _id: req.bookingId },
        { $set: { bookingStatus: "cancelled" } }
      );
      let updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
        { bookingDataId: req.bookingId },
        { $set: { paymentStatus: "cancelled", bookingStatus: "cancelled" } }
      );
      return {
        responseCode: 400,
        responseMessage: "Payment unsuccessful",
        data: error
      };
    }
  },

  cancelBookingRefundPayment: async (req, res) => {
    try {
      let payData = await paymentModel.findOne({
        senderId: req.senderId,
        _id: req.paymentId
      });
      let refundData = await stripe.refunds.create({
        charge: payData.chargeId
      });
      let updatePayData = await paymentModel.findByIdAndUpdate(
        {
          senderId: req.senderId,
          _id: req.paymentId
        },
        {
          $set: {
            refundId: refundData.id,
            amount_refunded: refundData.amount
          }
        },
        {
          new: true
        }
      );
      // user
      //   .findOne()
      //   .populate({
      //     path: "_id",
      //     select: "senderEmail senderName"
      //   })
      //   .exec((error5, result5) => {
      // if (error5) {
      //   res.send({
      //     responseCode: 500,
      //     responseMessage: "Internal server error"
      //   });
      // } else {
      //   commonFunction.failedEmail(
      //     result5.senderEmail,
      //     "Balance refund",
      //     result5.senderName,
      //     (error2, result2) => {
      //       if (error2) {
      //         res.send({
      //           responseCode: 500,
      //           responseMessage: "Internal server error"
      //         });
      //       } else {
      return {
        responseCode: 200,
        success: true,
        responseMessage: "Refund successful",
        data: updatePayData
      };
      //       }
      //     }
      //   );
      // }
      // });
    } catch (error) {
      console.log(error);
      return {
        responseCode: 400,
        success: false,
        responseMessage: "Refund unsuccessful",
        data: error
      };
    }
  },

  paymentForSubscription: async (req, res) => {
    try {
      let stripeToken = req.stripeToken;
      let userData = await UserModel.findOne({
        _id: req.senderId,
        status: "active"
      });

      let stripeCustomerData = await stripe.customers.create({
        email: userData.email,
        source: stripeToken
      });
      let chargesData = await stripe.charges.create({
        amount: req.amount,
        currency: "aud",
        customer: stripeCustomerData.id,
        description: req.description
      });
      let updateSubscriptionPaymentData = await subscriptionPaymentModel.findOneAndUpdate(
        { _id: req.subscriptionPaymentDetailsId },
        {
          $set: {
            paymentStatus: "completed"
          }
        }
      );
      var obj = {
        transactionId: chargesData.balance_transaction,
        chargeId: chargesData.id,
        amount: chargesData.amount,
        amount_refunded: chargesData.amount_refunded,
        customerId: chargesData.customer,
        url: chargesData.receipt_url,
        email: stripeCustomerData.email,
        transactionStatus: chargesData.status,
        senderId: req.senderId,
        subscriptionPaymentDetailsId: updateSubscriptionPaymentData._id
      };

      var obj1 = new paymentModel(obj);
      let paymentData = await obj1.save();

      updateSubscriptionPaymentData = await subscriptionPaymentModel.findOneAndUpdate(
        { _id: req.subscriptionId },
        {
          $set: {
            paymentId: paymentData._id,
            transactionId: paymentData.transactionId
          }
        }
      );

      var data = {
        paymentId: paymentData._id,
        transactionId: paymentData.transactionId,
        transactionStatus: paymentData.transactionStatus,
        amount: paymentData.amount,
        createdAt: paymentData.createdAt,
        url: paymentData.url
      };
      return {
        responseCode: 200,
        responseMessage: "Payment successful",
        data: data
      };
    } catch (error) {
      console.log(error);
      return {
        responseCode: 400,
        responseMessage: "Payment unsuccessful",
        data: error
      };
    }
  },

  completeBookingPayment: async (req, res) => {
    try {
      let stripeToken = req.stripeToken;
      let userData = await UserModel.findOne({
        _id: req.senderId,
        status: "active"
      });

      let stripeCustomerData = await stripe.customers.create({
        email: userData.email,
        source: stripeToken
      });

      let chargesData = await stripe.charges.create({
        amount: req.amount,
        currency: "aud",
        customer: stripeCustomerData.id,
        description: req.description
      });

      let updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
        { bookingDataId: req.bookingId },
        {
          $set: {
            paymentStatus: "completed"
          }
        },
        { new: true }
      );

      var obj = {
        transactionId: chargesData.balance_transaction,
        chargeId: chargesData.id,
        amount: chargesData.amount,
        amount_refunded: chargesData.amount_refunded,
        customerId: chargesData.customer,
        url: chargesData.receipt_url,
        email: stripeCustomerData.email,
        transactionStatus: chargesData.status,
        senderId: req.senderId,
        receiverId: updateBookingOrders.studioId
          ? updateBookingOrders.studioId
          : updateBookingOrders.artistId,
        bookingId: updateBookingOrders._id
      };

      var obj1 = new paymentModel(obj);
      let paymentData = await obj1.save();

      updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
        { bookingDataId: req.bookingId },
        {
          $set: {
            paymentId: paymentData._id,
            transactionId: paymentData.transactionId
          }
        },
        { new: true }
      );
      var data = {
        paymentId: paymentData._id,
        transactionId: paymentData.transactionId,
        transactionStatus: paymentData.transactionStatus,
        amount: paymentData.amount,
        createdAt: paymentData.createdAt,
        url: paymentData.url
      };
      return {
        responseCode: 200,
        responseMessage: "Payment successful",
        data: data
      };
    } catch (error) {
      console.log(error);
      return {
        responseCode: 400,
        responseMessage: "Payment unsuccessful",
        data: error
      };
    }
  },

  payment: async (req, res) => {
    try {
      if (
        !req.body.senderId ||
        !req.body.amount ||
        !req.body.stripeToken ||
        !req.body.paymentType
      ) {
        res.send({
          responseCode: 204,
          responseMessage: "Parameter is missing"
        });
      } else {
        let stripeToken = req.body.stripeToken;
        let userData = await UserModel.findOne({
          _id: req.body.senderId,
          status: "active"
        });
        if (!userData) {
          res.send({
            responseCode: 404,
            responseMessage: "Sender not found"
          });
        }

        let stripeCustomerData = await stripe.customers.create({
          email: userData.email,
          source: stripeToken
        });

        // let sourceData = stripe.customers.createSource(stripeCustomerData.id, {
        //   source: stripeToken
        // });

        let bookingData = await bookingModel.findById(req.body.bookingId);

        let profileData = await profileModel.findOne({
          userId: req.body.receiverId
        });

        let chargeObj = {
          amount: req.body.amount,
          currency: "aud",
          // source: stripeToken,
          customer: stripeCustomerData.id,
          description: req.body.description,
          destination: profileData.stripeAccountId,
          application_fee: bookingData.bookingFee
        };

        let chargesData = await stripe.charges.create(chargeObj);
        var obj = {
          currencyCode: chargesData.currency,
          transactionId: chargesData.balance_transaction,
          chargeId: chargesData.id,
          amount: chargesData.amount,
          amount_refunded: chargesData.amount_refunded,
          customerId: chargesData.customer,
          url: chargesData.receipt_url,
          email: stripeCustomerData.email,
          transactionStatus: chargesData.status,
          senderId: req.body.senderId,
          recipientId: req.body.recipientId,
          bookingId: req.body.bookingId,
          subscriptionId: req.body.subscriptionId,
          description: req.body.description
        };

        var obj1 = new paymentModel(obj);
        let paymentData = await obj1.save();

        if (req.body.paymentType == "booking") {
          if (req.body.paidAmountType == "deposit") {
            var updateBookingData = await bookingModel.findOneAndUpdate(
              { _id: req.body.bookingId, clientPaidDepositAmount: false },
              {
                $set: {
                  clientPaidDepositAmount: true,
                  isBookingConfirmByClient: true
                }
              },
              { new: true }
            );
          } else {
            var updateBookingData = await bookingModel.findOneAndUpdate(
              { _id: req.body.bookingId, clientPaidDepositAmount: true },
              {
                $set: { clientPaidFullAmount: true, bookingStatus: "completed" }
              },
              { new: true }
            );
            let updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
              { bookingDataId: req.body.bookingId },
              {
                $set: {
                  bookingStatus: "completed",
                  paymentStatus: "completed",
                  paymentId: paymentData._id,
                  transactionId: paymentData.transactionId
                }
              },
              { new: true }
            );
          }
        } else if (req.body.paymentType == "subscription") {
          let updateSubscriptionPaymentData = await subscriptionPaymentModel.findOneAndUpdate(
            { _id: req.body.subscriptionId },
            {
              $set: {
                paymentStatus: "completed",
                paymentId: paymentData._id,
                transactionId: paymentData.transactionId
              }
            }
          );
        }

        var data = {
          paymentId: paymentData._id,
          transactionId: paymentData.transactionId,
          transactionStatus: paymentData.transactionStatus,
          amount: paymentData.amount,
          createdAt: paymentData.createdAt,
          url: paymentData.url
        };
        res.send({
          responseCode: 200,
          success: true,
          responseMessage: "Charge",
          data
        });
      }
    } catch (error) {
      console.log(error);
      let updateBookingData = await bookingModel.findOneAndUpdate(
        { _id: req.body.bookingId },
        { $set: { bookingStatus: "cancelled" } }
      );
      let updateBookingOrders = await bookingOrderModel.findOneAndUpdate(
        { bookingDataId: req.body.bookingId },
        { $set: { paymentStatus: "cancelled", bookingStatus: "cancelled" } }
      );
      res.send({
        responseCode: 200,
        success: false,
        responseMessage: "Stripe error",
        data: error
      });
    }
  },

  refund: async (req, res) => {
    try {
      if (!req.body.senderId || !req.body.paymentId) {
        res.send({
          responseCode: 204,
          responseMessage: "Parameter is missing"
        });
      } else {
        let payData = await paymentModel.findOne({
          senderId: req.body.senderId,
          _id: req.body.paymentId
        });
        if (!payData) {
          res.send({
            responseCode: 404,
            responseMessage: "Payment id not found"
          });
        } else {
          let refundData = await stripe.refunds.create({
            charge: payData.chargeId
          });
          let updatePayData = await paymentModel.findByIdAndUpdate(
            {
              senderId: req.body.senderId,
              _id: req.body.paymentId
            },
            {
              $set: {
                refundId: refundData.id,
                amount_refunded: refundData.amount
              }
            },
            {
              new: true
            }
          );
          res.send({
            responseCode: 200,
            success: true,
            responseMessage: "Refund successful",
            data: updatePayData
          });
        }
      }
    } catch (error) {
      console.log(error);
      res.send({
        responseCode: 200,
        success: false,
        responseMessage: "Stripe error",
        data: null
      });
    }
  },

  paymentList: async (req, res) => {
    try {
      let { searchKey, searchBody } = req.query;

      let aggregate = [];

      aggregate.push({
        $lookup: {
          from: "users",
          localField: "senderId",
          foreignField: "_id",
          as: "senderData"
        }
      });

      matchSenderData = {};
      if (searchBody) {
        if (searchKey == "name") {
          var nameQuery = searchBody,
            queryArr = nameQuery.split(" "),
            firstName,
            lastName;

          if (queryArr.length == 1) {
            firstName = queryArr[0];
            matchSenderData = {
              "senderData.firstName": { $regex: firstName, $options: "i" }
            };
          } else if (queryArr.length == 2) {
            firstName = queryArr[0];
            lastName = queryArr[1];
            matchSenderData.$or = [
              {
                $and: [
                  {
                    "senderData.firstName": {
                      $regex: firstName,
                      $options: "i"
                    }
                  },
                  {
                    "senderData.lastName": { $regex: lastName, $options: "i" }
                  }
                ]
              },
              {
                $and: [
                  {
                    "senderData.firstName": { $regex: lastName, $options: "i" }
                  },
                  {
                    "senderData.lastName": { $regex: firstName, $options: "i" }
                  }
                ]
              }
            ];
          }

          aggregate.push({ $match: matchSenderData });
        }
      }

      aggregate.push({
        $project: {
          currencyId: 1,
          amount: 1,
          transactionId: 1,
          amount_refunded: 1,
          url: 1,
          email: 1,
          transactionStatus: 1,
          firstName: { $arrayElemAt: ["$senderData.firstName", 0] },
          lastName: { $arrayElemAt: ["$senderData.lastName", 0] }
        }
      });

      aggregate.push({
        $facet: {
          data: [
            { $sort: { createdAt: -1 } },
            { $skip: parseInt(req.query.page) },
            { $limit: parseInt(req.query.limit) }
          ],
          pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
        }
      });

      let paymentList = await paymentModel.aggregate(aggregate);

      let data = paymentList[0].data;
      let finalResult = {};
      if (data.length > 0) {
        finalResult = {
          data: data,
          count: paymentList[0].pageInfo[0].count
        };
      } else {
        finalResult = {
          data: [],
          count: 0
        };
      }

      return SendResponse(
        res,
        finalResult,
        true,
        "Payment list shown successfully"
      );
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  paymentListForUser: async (req, res) => {
    try {
      let { searchKey, searchBody, userType } = req.query;
      let userId = req.decoded.data._id;

      let aggregate = [];
      let query = {};
      if (userType == "client") {
        query.senderId = userId;
      } else {
        query.$or = [{ senderId: userId }, { receiverId: userId }];
      }
      let options = {
        limit: parseInt(req.query.limit),
        page: parseInt(req.query.page),
        populate: {
          path: "bookingId",
          select: "artistId",
          populate: {
            path: "artistId",
            select: "firstName lastName profileId",
            populate: {
              path: "profileId",
              select: "contactNumber"
            }
          }
        },
        sort: {
          createdAt: -1
        }
      };
      let paymentList = await paymentModel.paginate(query, options);
      // aggregate.push({
      //   $lookup: {
      //     from: "users",
      //     localField: "senderId",
      //     foreignField: "_id",
      //     as: "senderData"
      //   }
      // });

      // matchSenderData = {};
      // if (searchBody) {
      //   if (searchKey == "name") {
      //     var nameQuery = searchBody,
      //       queryArr = nameQuery.split(" "),
      //       firstName,
      //       lastName;

      //     if (queryArr.length == 1) {
      //       firstName = queryArr[0];
      //       matchSenderData = {
      //         "senderData.firstName": { $regex: firstName, $options: "i" }
      //       };
      //     } else if (queryArr.length == 2) {
      //       firstName = queryArr[0];
      //       lastName = queryArr[1];
      //       matchSenderData.$or = [
      //         {
      //           $and: [
      //             {
      //               "senderData.firstName": {
      //                 $regex: firstName,
      //                 $options: "i"
      //               }
      //             },
      //             {
      //               "senderData.lastName": { $regex: lastName, $options: "i" }
      //             }
      //           ]
      //         },
      //         {
      //           $and: [
      //             {
      //               "senderData.firstName": { $regex: lastName, $options: "i" }
      //             },
      //             {
      //               "senderData.lastName": { $regex: firstName, $options: "i" }
      //             }
      //           ]
      //         }
      //       ];
      //     }

      //     aggregate.push({ $match: matchSenderData });
      //   }
      // }

      // aggregate.push({
      //   $project: {
      //     currencyId: 1,
      //     amount: 1,
      //     transactionId: 1,
      //     amount_refunded: 1,
      //     url: 1,
      //     email: 1,
      //     transactionStatus: 1,
      //     firstName: { $arrayElemAt: ["$senderData.firstName", 0] },
      //     lastName: { $arrayElemAt: ["$senderData.lastName", 0] }
      //   }
      // });

      // aggregate.push({
      //   $facet: {
      //     data: [
      //       { $sort: { createdAt: -1 } },
      //       { $skip: parseInt(req.query.page) },
      //       { $limit: parseInt(req.query.limit) }
      //     ],
      //     pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
      //   }
      // });

      // let paymentList = await paymentModel.aggregate(aggregate);

      // let data = paymentList[0].data;
      // let finalResult = {};
      // if (data.length > 0) {
      //   finalResult = {
      //     data: data,
      //     count: paymentList[0].pageInfo[0].count
      //   };
      // } else {
      //   finalResult = {
      //     data: [],
      //     count: 0
      //   };
      // }

      return SendResponse(
        res,
        paymentList,
        true,
        "Payment list shown successfully"
      );
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  },

  paymentDetails: async (req, res) => {
    try {
      let paymentDetails = await paymentModel
        .findOne({
          _id: req.query.paymentId
        })
        .populate([
          { path: "senderId", select: "firstName lastName contactNumber" },
          {
            path: "bookingId",
            select: " paymentStatus bookingStatus bookingId artistId",
            populate: { path: "artistId", select: "firstName lastName" }
          },
          { path: "subscriptionId" }
        ]);
      return SendResponse(
        res,
        paymentDetails,
        true,
        "Payment Details shown successfully"
      );
    } catch (err) {
      console.log(err);
      return SendResponse(res, Boom.badImplementation());
    }
  }
};

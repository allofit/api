const Boom = require("boom");
const UserModel = require("../models/user.model");
const userProfileModel = require("../models/profileDetails.model");
const AssociationModel = require("../models/associationManagement.model");
const faqModel = require("../models/faq.model");
const CancellationModel = require("../models/artistCancellationList.model");
const SendResponse = require("../apiHandler");
const util = require("../../util");
const constants = require("../../constants");
const jwt = require("jsonwebtoken");
const mailService = require("../services/mailer");
require("dotenv").config();
process.env.NODE_ENV = "local";
var config = require("../../config");

const SubscriptionModel = require("../models/subscription.model");
const userSubscriptionModel = require("../models/userSubscription.model");
const specializationModel = require("../models/specialization.model");

module.exports.register = async (req, res) => {
  try {
    let { email } = req.body;
    email = email.toLowerCase();
    req.body.firstName = util.initCap(req.body.firstName);
    req.body.lastName = util.initCap(req.body.lastName);
    let user = new UserModel(req.body);
    let emailToken = util.generateOTP();
    let exists = await UserModel.findOne({
      email: email
    });
    if (exists) {
      return SendResponse(res, null, false, "Email is already registered.");
    }
    user.password = user.hash(user.password);
    user = await user.save();
    let token = jwt.sign(
      {
        data: {
          _id: user._id,
          createdAt: new Date(),
          userType: user.userType
        }
      },
      constants.jwtSecret,
      {
        expiresIn: constants.tokenExpiresIn
      }
    );
    user.token = token;
    var link1 = `Hi ${user.firstName},<br> Please click the link to verify your email- http://api.allofit.co:${config.port}/api/user/emailVerify?emailToken=${emailToken}&userId=${user._id}`;
    //api.allofit.co
    let sendMail = await mailService.send(
      req.body.email,
      "Email verification link",
      link1
    );
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    for (var i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    UserModel.findByIdAndUpdate(
      {
        _id: user._id
      },
      {
        token: token,
        emailToken: emailToken,
        referralCode: text
      },
      {
        new: true
      },
      (err, data) => {}
    );
    user.password = "";
    if (user.userType == "artist" || user.userType == "studio")
      firstSubscriptionBuy(user._id, user.userType);

    return SendResponse(res, user, true, "User created.");
  } catch (err) {
    console.log(err);
    if (err.code == 11000) {
      return SendResponse(res, null, false, "User name already exist");
    } else {
      return SendResponse(res, Boom.badImplementation());
    }
  }
};

module.exports.emailVerify = async (req, res) => {
  try {
    let userData = await UserModel.findOne({
      _id: req.query.userId,
      status: "active"
    });
    if (userData) {
      if (req.query.emailToken == userData.emailToken) {
        let updateData = await UserModel.findOneAndUpdate(
          {
            _id: req.query.userId,
            status: "active"
          },
          {
            $set: {
              emailToken: "NULL",
              emailVerified: true
            }
          },
          {
            new: true
          }
        );
        return SendResponse(res, null, true, "Email Verified successfully");
      } else {
        return SendResponse(res, null, false, "Token Mismatch.");
      }
    } else return SendResponse(res, null, false, "User not found.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.update = async (req, res) => {
  try {
    let id = req.decoded.data._id;
    let { emailVerified, UserType, email } = req.body;
    let nonUpdatable = {
      emailVerified,
      email
    };
    let user = await UserModel.findOne({
      _id: id
    });
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    for (let key in nonUpdatable) {
      delete req.body[key];
    }
    if (req.body.firstName) {
      req.body.firstName = util.initCap(req.body.firstName);
    }
    if (req.body.lastName) {
      req.body.lastName = util.initCap(req.body.lastName);
    }
    if (req.body.email) {
      let exists = await UserModel.findOne({
        email: req.body.email,
        _id: {
          $ne: user._id
        }
      });
      if (exists) {
        return SendResponse(
          res,
          null,
          false,
          "Email is linked with another account."
        );
      }
      req.body.emailVerified = false;
    }
    user = await UserModel.findByIdAndUpdate(id, req.body, {
      new: true
    });
    return SendResponse(res, user, true, "User updated.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.get = async (req, res) => {
  try {
    let id = req.decoded.data._id;
    let user = await UserModel.findOne(
      {
        _id: id,
        status: "active"
      },
      "firstName lastName email userType"
    );
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    return SendResponse(res, user, true, "User shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getProfileData = async (req, res) => {
  try {
    let id = req.decoded.data._id;
    let user = await UserModel.findOne(
      {
        _id: id,
        status: "active"
      },
      { _id: 0, firstName: 1, lastName: 1, email: 1, userType: 1, userName: 1 }
    );
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    } else {
      let userProfile = await userProfileModel.findOne(
        { userId: id },
        {
          _id: 0
        }
      );
      if (user.userType == "studio") {
        let userAssociationData = await AssociationModel.findOne(
          { userId: id },
          { _id: 0, joinedRequest: 1 }
        ).populate({
          path: "joinedRequest",
          select: "firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        });
        user.set("associatedArtist", userAssociationData, { strict: false });
      }
      user.set("profileDetails", userProfile, { strict: false });
      return SendResponse(res, user, true, "User shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getProfileDataPublic = async (req, res) => {
  try {
    let userName = req.query.userName;
    let user = await UserModel.findOne(
      {
        userName: userName,
        status: "active"
      },
      { firstName: 1, lastName: 1, email: 1, userType: 1, userName: 1 }
    );
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    } else {
      console.log(user);
      let userProfile = await userProfileModel
        .findOne(
          { userId: user._id },
          {
            isBookingOpen: 1,
            isEnquiryOpen: 1,
            userName: 1,
            profileImage: 1,
            gallery: 1,
            specialization: 1,
            coverImage: 1,
            about: 1,
            studioName: 1,
            socialLink: 1,
            contactNumber: 1,
            videoUrl: 1,
            _id: 0
          }
        )
        .populate([
          {
            path: "specialization",
            select: "specializationName imageUrl"
          },
          { path: "themeId" }
        ]);
      let faqList = await faqModel
        .find({ status: "active" })
        .sort({ priorityOrder: 1 });
      let userFaqList = await userProfileModel.findOne(
        { userId: user._id },
        "faqAnswers"
      );
      let userNewFaq = await userProfileModel.findOne(
        { userId: user._id },
        "newFaqs"
      );
      if (!userFaqList) {
        user.set("Faq", faqList, { strict: false });
      } else {
        for (let i of faqList) {
          let userFaqData = await userProfileModel.findOne(
            { userId: user._id, "faqAnswers.faqId": i._id },
            { _id: 0, faqAnswers: { $elemMatch: { faqId: i._id } } }
          );
          if (userFaqData)
            i.description = userFaqData.faqAnswers[0].description;
        }
        if (userNewFaq.newFaqs) {
          for (let i of userNewFaq.newFaqs) {
            faqList.push(i);
          }
        }
        user.set("Faq", faqList, { strict: false });
      }

      if (user.userType == "studio") {
        let userAssociationData = await AssociationModel.findOne(
          { userId: user._id },
          { _id: 0, joinedRequest: 1 }
        ).populate({
          path: "joinedRequest",
          select: "firstName lastName",
          populate: { path: "profileId", select: "profileImage" }
        });
        user.set("associatedArtist", userAssociationData, { strict: false });
      }
      user.set("profileDetails", userProfile, { strict: false });
      return SendResponse(res, user, true, "User shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.delete = async (req, res) => {
  try {
    let { userId } = req.query;

    let user = await UserModel.findOne({ _id: userId, status: "active" });
    if (!user) {
      return SendResponse(res, null, false, "User not found.");
    }
    user = await UserModel.findByIdAndDelete(userId);
    return SendResponse(res, user, true, "User deleted.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.shareProfileLink = async (req, res) => {
  try {
    let link1 = req.body.shareableProfileLink;
    let sendMail = await mailService.send(
      req.body.email,
      "Profile link",
      "Profile link : " + link1
    );
    return SendResponse(res, null, true, "Mail sent successfully.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.joinCancellationList = async (req, res) => {
  try {
    let { artistId, startDate, endDate, remark } = req.body;
    let userId = req.decoded.data._id;
    let artistData = UserModel.findById({
      _id: artistId,
      status: "active",
      userType: "artist"
    });
    let clientData = UserModel.findById({
      _id: userId,
      status: "active",
      userType: "client"
    });
    if (!clientData) {
      return SendResponse(
        res,
        null,
        false,
        "Client not found or blocked by admin"
      );
    }
    let userObj = {
      userId,
      startDate,
      endDate,
      remark
    };
    if (!artistData)
      return SendResponse(
        res,
        null,
        false,
        "Artist not found or blocked by the admin."
      );
    let artistCancellationListData = await CancellationModel.findOne({
      artistId: artistId
    });
    if (!artistCancellationListData) {
      let obj = {
        artistId: artistId,
        artistProfileId: artistData.profileId,
        userList: [
          {
            userId,
            startDate,
            endDate,
            remark
          }
        ]
      };
      let artistCancellationData = new CancellationModel(obj);
      artistCancellationListData = await artistCancellationData.save();
    } else {
      artistCancellationListData = await CancellationModel.findOneAndUpdate(
        { artistId: artistId, "userList.userId": { $ne: userId } },
        { $push: { userList: userObj } },
        { new: true }
      );
    }

    return SendResponse(
      res,
      artistCancellationListData,
      true,
      "List Join Successfully."
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getCancellationList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let userData = await UserModel.findById({
      _id: userId,
      status: "active"
    });
    if (userData.userType == "artist") {
      let artistCancellationList = await CancellationModel.findOne(
        {
          artistId: userId
        },
        { _id: 0, userList: 1 }
      ).populate({
        path: "userList.userId",
        select: "firstName lastName",
        populate: { path: "profileId", select: "contactNumber" }
      });
      return SendResponse(
        res,
        artistCancellationList,
        true,
        "List Shown Successfully."
      );
    } else if (userData.userType == "client") {
      let userCancellationList = await CancellationModel.find(
        {
          "userList.userId": { $in: userId }
        },
        { _id: 0, "userList.$": 1, artistId: 1 }
      );
      return SendResponse(
        res,
        userCancellationList,
        true,
        "List Shown Successfully."
      );
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getStudioList = async (req, res) => {
  try {
    let id = req.decoded.data._id;
    let user = await UserModel.find(
      {
        status: "active",
        userType: "studio"
      },
      { firstName: 1, lastName: 1, email: 1, userType: 1 }
    );
    return SendResponse(res, user, true, "List shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getAllProfileLinks = async (req, res) => {
  try {
    let user = await UserModel.find(
      {
        status: "active",
        userType: { $in: ["studio", "artist"] }
      },
      { _id: 0, userName: 1, userType: 1 }
    );
    return SendResponse(res, user, true, "List shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.specializationListForUser = async (req, res) => {
  try {
    let specializationList = await specializationModel.find(
      { status: "active" },
      "specializationName imageUrl"
    );
    return SendResponse(
      res,
      specializationList,
      true,
      "Specialization List shown."
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

firstSubscriptionBuy = async (userId, userType) => {
  try {
    let subscriptionType = "trial";
    let subscriptionData = await SubscriptionModel.findOne({
      subscriptionType: subscriptionType,
      userType: userType
    });
    let startDate = new Date();
    let endDate = new Date();
    endDate.setDate(endDate.getDate() + subscriptionData.duration);
    let subscriptionId = subscriptionData._id;

    let obj = {
      userId: userId,
      subscriptionId: subscriptionId,
      startDate: startDate,
      endDate: endDate,
      subscriptionType: subscriptionType
    };
    let userSubscriptionData = new userSubscriptionModel(obj);
    userSubscriptionData = userSubscriptionData.save();
  } catch (err) {
    console.log(err);
  }
};

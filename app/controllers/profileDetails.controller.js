const Boom = require("boom");
const userProfileDetailModel = require("../models/profileDetails.model");
const themeModel = require("../models/themeManagement.model");
const userModel = require("../models/user.model");
const faqModel = require("../models/faq.model");
const SendResponse = require("../apiHandler");
const util = require("../../util");
const mailService = require("../services/mailer");
const fs = require("fs");
const path = require("path");
require("dotenv").config();
process.env.NODE_ENV = "local";
const AWS = require("aws-sdk");
//configuring the AWS environment
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY
});
var s3 = new AWS.S3();
module.exports.update = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let userProfileData = await userProfileDetailModel.findOne({
      userId: userId
    });
    let finalObj = {};
    finalObj = JSON.parse(req.body.data);
    const themeData = await themeModel.findOne({ userId: userId });
    if (themeData) finalObj.themeId = themeData._id;
    let newPromiseArr = [];
    //profileImage   .....Checking profile image exist or not
    if (req.files && req.files.profileImage) {
      var filePath = req.files.profileImage[0].path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key: "profileImage/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      newPromiseArr.push(uploadImageToS3(params));
    } else {
      newPromiseArr.push(Promise.resolve(""));
    }
    //coverImage     .....Checking cover image exist or not
    if (req.files && req.files.coverImage) {
      var filePath = req.files.coverImage[0].path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key: "coverImage/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      newPromiseArr.push(uploadImageToS3(params));
    } else {
      newPromiseArr.push(Promise.resolve(""));
    }
    // //drivingLicenceDoc    .....Checking driving licence doc exist or not
    // if (req.files && req.files.drivingLicenceDoc) {
    //   var filePath = req.files.drivingLicenceDoc[0].path;
    //   var params = {
    //     Bucket: process.env.AWS_BUCKET_NAME,
    //     Body: fs.createReadStream(filePath),
    //     Key: "drivingLicenceDoc/" + Date.now() + "_" + path.basename(filePath),
    //     ACL: "public-read"
    //   };
    //   newPromiseArr.push(uploadImageToS3(params));
    // } else {
    //   newPromiseArr.push(Promise.resolve(""));
    // }
    //tattooLicenceDoc    .....Checking tattoo licence doc exist or not
    if (req.files && req.files.tattooLicenceDoc) {
      var filePath = req.files.tattooLicenceDoc[0].path;
      var params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Body: fs.createReadStream(filePath),
        Key: "tattooLicenceDoc/" + Date.now() + "_" + path.basename(filePath),
        ACL: "public-read"
      };
      newPromiseArr.push(uploadImageToS3(params));
    } else {
      newPromiseArr.push(Promise.resolve(""));
    }

    //gallery     .....Checking gallery image exist or not
    if (req.files && req.files.gallery) {
      if (req.files.gallery.length >= 1) {
        let promiseArr = [];
        for (let i = 0; i <= req.files.gallery.length - 1; i++) {
          var filePath = req.files.gallery[i].path;
          var params = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Body: fs.createReadStream(filePath),
            Key: "gallery/" + Date.now() + "_" + path.basename(filePath),
            ACL: "public-read"
          };
          promiseArr.push(uploadImageToS3(params));
        }
        newPromiseArr.push(Promise.all(promiseArr));
      }
    } else {
      newPromiseArr.push(Promise.resolve([]));
    }
    // .....Resolving all the promises
    let finalImageArr = await Promise.all(newPromiseArr);
    if (finalImageArr[0]) {
      //    .....checking profileImage is in request or not
      finalObj.profileImage = finalImageArr[0];
    }
    if (finalImageArr[1]) {
      //       .....checking coverImage is in request or not
      finalObj.coverImage = finalImageArr[1];
    }
    // if (finalImageArr[2]) {
    //   //       ......checking drivingLicenceDoc is in request or not
    //   finalObj.drivingLicenceDoc = finalImageArr[2];
    // }
    if (finalImageArr[2]) {
      //         ......checking tattooLicenceDoc is in request or not
      finalObj.tattooLicenceDoc = finalImageArr[2];
    }
    if (finalImageArr[3].length > 0) {
      //       ....checking gallery is in request or not
      for (let i of finalImageArr[3]) {
        finalObj.gallery.push(i);
      }
    }
    let { firstName, lastName, userName } = finalObj;
    var userData = await userModel.findOne({
      _id: userId,
      status: "active"
    });
    if (!userData) return SendResponse(res, null, false, "User not found.");
    else {
      let checkUserName = await userModel.findOne({
        userName: userName,
        _id: { $ne: userData._id }
      });
      if (checkUserName) {
        return SendResponse(res, null, false, "User name already exist");
      }
      userData = await userModel.findOneAndUpdate(
        {
          _id: userId
        },
        {
          $set: {
            firstName: firstName,
            lastName: lastName,
            userName: userName
          }
        },
        {
          new: true
        }
      );
      if (userProfileData) {
        console.log("object", finalObj);
        let user = await userProfileDetailModel.findOneAndUpdate(
          {
            userId: userId
          },
          finalObj,
          {
            new: true
          }
        );
        user.set("firstName", userData.firstName, {
          strict: false
        });
        user.set("lastName", userData.lastName, {
          strict: false
        });
        user.set("userName", userData.userName, {
          strict: false
        });
        return SendResponse(res, user, true, "User Updated.");
      } else {
        finalObj.userId = userId;
        let user = new userProfileDetailModel(finalObj);
        user = await user.save();
        let updateProfileId = await userModel.findOneAndUpdate(
          {
            _id: userId
          },
          {
            $set: {
              profileId: user._id
            }
          },
          {
            new: true
          }
        );
        user.set("firstName", userData.firstName, {
          strict: false
        });
        user.set("lastName", userData.lastName, {
          strict: false
        });
        return SendResponse(res, user, true, "User Updated.");
      }
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.getFaqList = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let faqList = await faqModel
      .find({ status: "active" })
      .sort({ priorityOrder: 1 });
    let userFaqList = await userProfileDetailModel.findOne(
      { userId: userId },
      "faqAnswers"
    );
    if (!userFaqList) {
      let faqObj = {
        adminFaq: faqList,
        userFaq: []
      };
      console.log("===>>>>i m here 223", faqObj);
      return SendResponse(res, faqObj, true, "User Faq List shown.");
    } else {
      for (let i of faqList) {
        let userFaqData = await userProfileDetailModel.findOne(
          { userId: userId, "faqAnswers.faqId": i._id },
          { _id: 0, faqAnswers: { $elemMatch: { faqId: i._id } } }
        );
        if (userFaqData) i.description = userFaqData.faqAnswers[0].description;
      }
      let newFaq = await userProfileDetailModel.findOne(
        {
          userId: userId
        },
        { _id: 0, newFaqs: 1 }
      );
      let faqObj = {
        adminFaq: faqList,
        userFaq: newFaq
      };
      console.log("====>>>>>", faqObj);
      return SendResponse(res, faqObj, true, "List shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateUserFaq = async (req, res) => {
  try {
    let { faqId, title, description } = req.body;
    let userId = req.decoded.data._id;
    let userFaqList = await userProfileDetailModel.findOne(
      { userId: userId, "faqAnswers.faqId": req.body.faqId },
      { _id: 0, faqAnswers: { $elemMatch: { faqId: req.body.faqId } } }
    );
    if (!userFaqList) {
      let obj = {
        faqId: faqId,
        title: title,
        description: description
      };
      let updatedFaq = await userProfileDetailModel.findOneAndUpdate(
        { userId: userId },
        { $push: { faqAnswers: obj } },
        { new: true }
      );
      let object = updatedFaq.faqAnswers.find(
        item => item.faqId == req.body.faqId
      );
      return SendResponse(res, updatedFaq, true, "Updated.");
    } else {
      let updatedFaq = await userProfileDetailModel.findOneAndUpdate(
        { userId: userId, "faqAnswers.faqId": req.body.faqId },
        { "faqAnswers.$.description": description },
        { new: true }
      );
      let object = updatedFaq.faqAnswers.find(
        item => item.faqId == req.body.faqId
      );
      return SendResponse(res, object, true, "Updated.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.addFaq = async (req, res) => {
  try {
    let { title, description } = req.body;
    let userId = req.decoded.data._id;
    let obj = {
      title,
      description
    };
    let userFaq = await userProfileDetailModel.findOneAndUpdate(
      { userId: userId },
      { $push: { newFaqs: obj } },
      { new: true }
    );
    if (userFaq) {
      return SendResponse(res, userFaq, true, "Faq added successfully");
    } else {
      return SendResponse(res, null, false, "Please setup your profile first.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.removeFaq = async (req, res) => {
  try {
    let { faqId } = req.params;
    let removeFaq = await userProfileDetailModel.findOneAndUpdate(
      { "newFaqs._id": faqId },
      { $pull: { newFaqs: { _id: faqId } } },
      { new: true }
    );
    if (removeFaq) {
      return SendResponse(res, removeFaq, true, "Faq removed successfully");
    } else {
      return SendResponse(res, null, false, "No user faq found");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateFaq = async (req, res) => {
  try {
    let { description } = req.body;
    let userId = req.decoded.data._id;
    let updatedFaq = await userProfileDetailModel.findOneAndUpdate(
      { userId: userId, "newFaqs._id": req.body.faqId },
      { "newFaqs.$.description": description },
      { new: true }
    );
    let object = await updatedFaq.newFaqs.find(
      item => item._id == req.body.faqId
    );
    return SendResponse(res, object, true, "Update faq.");
  } catch (err) {
    console.log(err);
    return SendResponse();
  }
};

module.exports.getFaq = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let userFaqData = await userProfileDetailModel.findOne(
      { userId: userId, "faqAnswers.faqId": req.params.faqId },
      { _id: 0, faqAnswers: { $elemMatch: { faqId: req.params.faqId } } }
    );
    if (!userFaqData) {
      userFaqData = await faqModel.findOne(
        { _id: req.params.faqId },
        "title description"
      );
      return SendResponse(res, userFaqData, true, "List shown.");
    } else {
      return SendResponse(res, userFaqData.faqAnswers[0], true, "List shown.");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.updateNotificationAlert = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let notificationAlert = await userProfileDetailModel.findOneAndUpdate(
      { userId: userId },
      { $set: { notificationAlert: req.body.notificationAlert } },
      { new: true }
    );
    if (!notificationAlert) {
      return SendResponse(res, null, false, "User not found");
    }
    return SendResponse(
      res,
      notificationAlert,
      true,
      "Notification alert setting updated"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

function uploadImageToS3(params) {
  return new Promise((resolve, reject) => {
    s3.upload(params, function(err, data) {
      if (err) {
        console.log("Error", err);
        reject(err);
      }
      if (data) {
        resolve(data.Location);
        console.log("Uploaded in:", data.Location);
      }
    });
  });
}

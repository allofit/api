const Boom = require("boom");
const SubscriptionModel = require("../models/subscription.model");
const userSubscriptionModel = require("../models/userSubscription.model");
const subscriptionPaymentModel = require("../models/subscriptionPayment.model");
const paymentController = require("./payment.controller");
const SendResponse = require("../apiHandler");

module.exports.subscriptionList = async (req, res) => {
  try {
    let { limit, page, userType } = req.query;
    let options = {
      limit: limit,
      page: page,
      sort: {
        createdAt: -1
      }
    };
    let subscriptionList = await SubscriptionModel.find({
      status: "active",
      userType: userType
    });
    return SendResponse(
      res,
      subscriptionList,
      true,
      "Subscription list shown successfully."
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.firstSubscriptionBuy = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { userType } = req.body;
    let subscriptionType = "trial";
    let subscriptionData = await SubscriptionModel.findOne({
      subscriptionType: subscriptionType,
      userType: userType
    });
    let startDate = new Date();
    let endDate = new Date();
    endDate.setDate(endDate.getDate() + subscriptionData.duration);
    let subscriptionId = subscriptionData._id;

    let obj = {
      userId: userId,
      subscriptionId: subscriptionId,
      startDate: startDate,
      endDate: endDate,
      subscriptionType: subscriptionType
    };
    let userSubscriptionData = new userSubscriptionModel(obj);
    userSubscriptionData = userSubscriptionData.save();
    // return SendResponse(
    //   res,
    //   userSubscriptionData,
    //   true,
    //   "User subscribed successfully"
    // );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.buySubscription = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let { subscriptionId, paymentType } = req.body;
    let subscriptionData = await SubscriptionModel.findOne({
      _id: subscriptionId
    });
    let obj = {
      subscriptionId: subscriptionId,
      userId: userId,
      amount: subscriptionData.amount,
      days: subscriptionData.duration,
      paymentType: paymentType
    };
    let subscriptionPaymentData = new subscriptionPaymentModel(obj);
    subscriptionPaymentData = await subscriptionPaymentData.save();
    let userSubscriptionData = await userSubscriptionModel.findOne({
      userId: userId
    });
    req.body.paymentData.subscriptionPaymentDetailsId =
      subscriptionPaymentData._id;
    let paymentDone = await paymentController.paymentForSubscription(
      req.body.paymentData
    );
    if (paymentDone.responseCode == "400") {
      return SendResponse(res, paymentDone.data, false, "Payment unsuccessful");
    }
    let userSubscriptionObj = {
      subscriptionId: subscriptionData._id,
      purchaseDate: new Date(),
      startDate: new Date(userSubscriptionData.endDate),
      endDate: new Date(userSubscriptionData.endDate),
      subscriptionType: "paid"
    };
    userSubscriptionObj.endDate.setDate(
      userSubscriptionObj.endDate.getDate() + subscriptionData.duration
    );

    userSubscriptionData = await userSubscriptionModel.findOneAndUpdate(
      { userId: userId },
      userSubscriptionObj,
      { new: true }
    );

    return SendResponse(
      res,
      userSubscriptionData,
      true,
      "Subscription purchased successfully"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.userSubscriptionDetails = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let userSubscriptionData = await userSubscriptionModel.findOne({
      userId: userId
    });
    if (userSubscriptionData) {
      return SendResponse(
        res,
        userSubscriptionData,
        true,
        "User subscription shown successfully"
      );
    } else {
      return SendResponse(res, null, false, "No subscribed subscription");
    }
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

module.exports.subscriptionHistory = async (req, res) => {
  try {
    let userId = req.decoded.data._id;
    let options = {
      limit: 100,
      page: 1,
      populate: { path: "subscriptionId", select: "name description" },
      sort: {
        createdAt: -1
      }
    };
    let paymentHistoryData = await subscriptionPaymentModel.paginate(
      { userId: userId },
      options
    );
    return SendResponse(
      res,
      paymentHistoryData,
      true,
      "Payment history shown successfully"
    );
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

const Boom = require("boom");
const notificationModel = require("../models/notification.model");
const SendResponse = require("../apiHandler");
const userModel = require("../models/user.model");

//firebase-admin for push notification
var admin = require("firebase-admin");

var serviceAccount = require("../../allOfIt");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://all-of-it-4ffb8.firebaseio.com"
});

module.exports.sendNotificationForMessage = async (
  req,
  userId,
  notificationTitle
) => {
  let notificationType = "message";
  let receiverId = req.conversation[req.conversation.length - 1].receiverId._id;
  let obj = {
    userId: userId,
    notificationTitle: notificationTitle,
    senderId: userId,
    receiverId: receiverId,
    notificationType: notificationType
  };

  let notificationData = new notificationModel(obj);
  notificationData = await notificationData.save();
  let notificationId = notificationData._id;

  let message =
    "New message from " +
    req.conversation[req.conversation.length - 1].senderId.firstName +
    " " +
    req.conversation[req.conversation.length - 1].senderId.lastName;

  console.log("Notification Api", notificationId);
  var payload = {
    notification: {
      title: "AllOfIt",
      body: message

      // notificationId: notificationId
    },
    data: {
      notificationId: notificationId + "",
      notificationType: notificationType
    }
  };
  var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  };
  let device_token =
    req.conversation[req.conversation.length - 1].receiverId.webToken;
  admin
    .messaging()
    .sendToDevice(device_token, payload, options)
    .then(function(response) {})
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
};

module.exports.sendNotificationForBooking = async (
  req,
  userId,
  notificationTitle
) => {
  let notificationType = "booking";
  let receiverId = "";
  if (req.studioId) {
    receiverId = req.studioId;
  } else {
    receiverId = req.artistId;
  }
  let receiverData = await userModel.findById({ _id: receiverId });
  let senderData = await userModel.findById({ _id: userId });
  let obj = {
    userId: userId,
    notificationTitle: notificationTitle,
    senderId: userId,
    receiverId: receiverId,
    notificationType: notificationType
  };

  let notificationData = new notificationModel(obj);
  notificationData = await notificationData.save();
  let notificationId = notificationData._id;

  let message =
    "New booking from " + senderData.firstName + " " + senderData.lastName;

  console.log("Notification Api", notificationId);
  var payload = {
    notification: {
      title: "AllOfIt",
      body: message

      // notificationId: notificationId
    },
    data: {
      notificationId: notificationId + "",
      notificationType: notificationType
    }
  };
  var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  };
  let device_token = receiverData.webToken;
  admin
    .messaging()
    .sendToDevice(device_token, payload, options)
    .then(function(response) {})
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
};

module.exports.sendNotificationForJoinRequest = async (
  senderData,
  receiverData,
  notificationTitle
) => {
  let notificationType = "joining";
  let obj = {
    userId: senderData._id,
    notificationTitle: notificationTitle,
    senderId: senderData._id,
    receiverId: receiverData._id,
    notificationType: notificationType
  };

  let notificationData = new notificationModel(obj);
  notificationData = await notificationData.save();
  let notificationId = notificationData._id;

  let message =
    "Joining request from " + senderData.firstName + " " + senderData.lastName;

  console.log("Notification Api", notificationId);
  var payload = {
    notification: {
      title: "AllOfIt",
      body: message

      // notificationId: notificationId
    },
    data: {
      notificationId: notificationId + "",
      notificationType: notificationType
    }
  };
  var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  };
  let device_token = receiverData.webToken;
  admin
    .messaging()
    .sendToDevice(device_token, payload, options)
    .then(function(response) {})
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
};

module.exports.sendNotificationForCancelBooking = async (
  req,
  userId,
  notificationTitle
) => {
  let notificationType = "cancelBooking";
  let receiverId = "";
  if (req.studioId) {
    receiverId = req.studioId;
  } else {
    receiverId = req.artistId;
  }
  let receiverData = await userModel.findById({ _id: receiverId });
  let senderData = await userModel.findById({ _id: userId });
  let obj = {
    userId: userId,
    notificationTitle: notificationTitle,
    senderId: userId,
    receiverId: receiverId,
    notificationType: notificationType
  };

  let notificationData = new notificationModel(obj);
  notificationData = await notificationData.save();
  let notificationId = notificationData._id;

  let message =
    "Booking is cancelled by " +
    senderData.firstName +
    " " +
    senderData.lastName;

  console.log("Notification Api", notificationId);
  var payload = {
    notification: {
      title: "AllOfIt",
      body: message

      // notificationId: notificationId
    },
    data: {
      notificationId: notificationId + "",
      notificationType: notificationType
    }
  };
  var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  };
  let device_token = receiverData.webToken;
  admin
    .messaging()
    .sendToDevice(device_token, payload, options)
    .then(function(response) {})
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
};

module.exports.notificationList = async (req, res) => {
  try {
    console.log("req.decoded", req.decoded.data);
    let userId = req.decoded.data._id,
      notificationType = req.query;

    let notificationList = await notificationModel
      .find({ receiverId: userId, notificationType: notificationType })
      .sort({ createdAt: -1 })
      .populate({
        path: "senderId",
        select: "firstName lastName profileId",
        populate: { path: "profileId", select: "profileImage" }
      });
    return SendResponse(res, notificationList, true, "List Shown.");
  } catch (err) {
    console.log(err);
    return SendResponse(res, Boom.badImplementation());
  }
};

const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;
const cancellationListSchema = new Schema(
  {
    artistId: {
      type: String,
      ref: "user"
    },
    artistProfileId: {
      type: String,
      ref: "profileDetails"
    },
    userList: [
      {
        userId: { type: String, ref: "user" },
        startDate: { type: String },
        endDate: { type: String },
        remark: { type: String }
      }
    ]
  },
  {
    timestamps: true
  }
);
cancellationListSchema.plugin(mongoosePaginate);
const cancellationList = mongoose.model(
  "cancellationList",
  cancellationListSchema
);
module.exports = cancellationList;

/**
 * @swagger
 * definitions:
 *   joinCancellationList:
 *     properties:
 *       artistId:
 *         type: string
 *       startDate:
 *         type: string
 *       endDate:
 *         type: string
 *       remark:
 *         type: string
 */

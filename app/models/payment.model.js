const mongoose = require("mongoose");
const schema = mongoose.Schema;
const aggregatePaginate = require("mongoose-aggregate-paginate");

var mongoosePaginate = require("mongoose-paginate");
let paymentDetails = new schema(
  {
    currencyCode: {
      type: String
    },
    description: {
      type: String
    },
    transactionId: {
      type: String
    },
    chargeId: {
      type: String
    },
    customerId: {
      type: String
    },
    url: {
      type: String
    },
    refundId: {
      type: String
    },
    amount_refunded: {
      type: String
    },

    paymentType: {
      type: String,
      enum: ["debitCard"]
    },
    senderId: {
      type: schema.Types.ObjectId,
      ref: "user"
    },
    receiverId: {
      type: schema.Types.ObjectId,
      ref: "user"
    },
    bookingId: {
      type: schema.Types.ObjectId,
      ref: "bookingData"
    },
    subscriptionPaymentDetailsId: {
      type: String,
      ref: "subscriptionPayment"
    },
    currency: {
      type: String,
      default: "aud"
    },
    amount: {
      type: String,
      default: "0"
    },
    email: {
      type: String
    },
    transactionStatus: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

paymentDetails.plugin(mongoosePaginate);
paymentDetails.plugin(aggregatePaginate);

module.exports = mongoose.model("paymentDetails", paymentDetails);

/**
 * @swagger
 * definitions:
 *   paymentData:
 *     properties:
 *       senderId:
 *         type: string
 *       amount:
 *         type: string
 *       bookingId:
 *         type: string
 *       stripeToken:
 *         type: string
 *       paymentType:
 *         type: string
 *       subscriptionOrderId:
 *         type: string
 */

/**
 *@swagger
 * definitions:
 *  refundData:
 *    properties:
 *      senderId:
 *        type: string
 *      paymentId:
 *        type: string
 */

const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const { Schema } = mongoose;
const profileDetailsSchema = new Schema(
  {
    stripeServiceAgreement: {
      type: Boolean,
      default: false
    },
    stripeAccountId: {
      type: String
    },
    stripeBankId: {
      type: String
    },
    dateOfBirth: {
      type: String
    },
    referralCode: {
      type: String
    },
    isBookingOpen: {
      type: Boolean
    },
    isEnquiryOpen: {
      type: Boolean
    },
    videoUrl: {
      type: String
    },
    notificationAlert: {
      type: Boolean,
      default: true
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    contactNumber: {
      type: String,
      default: ""
    },
    gender: {
      type: String
    },
    tattooLicence: {
      type: String
    },
    tattooLicenceDoc: {
      type: String
    },
    // drivingLicence: {
    //   type: String
    // },
    // drivingLicenceDoc: {
    //   type: String
    // },
    stateLicence: {
      type: String
    },
    // artistABN: {
    //   type: String
    // },
    specialization: [
      {
        type: Schema.Types.ObjectId,
        ref: "specialization"
      }
    ],
    about: {
      type: String
    },
    socialLink: {
      facebook: {
        type: String,
        default: ""
      },
      instagram: {
        type: String,
        default: ""
      },
      twitter: {
        type: String,
        default: ""
      }
    },
    coverImage: {
      type: String
    },
    profileImage: {
      type: String,
      default: ""
    },
    gallery: {
      type: Array
    },
    studioName: {
      type: String
    },
    themeId: {
      type: String,
      ref: "themeSchema"
    },
    newFaqs: [
      {
        title: {
          type: String
        },
        description: {
          type: String
        }
      }
    ],
    faqAnswers: [
      {
        faqId: {
          type: Schema.Types.ObjectId,
          ref: "faq"
        },
        title: {
          type: String
        },
        description: {
          type: String
        }
      }
    ],
    afterCareData: String,
    termsAndConditionData: String,
    consentData: String,
    address: {
      fullAddress: {
        type: String
      },
      city: {
        type: String
      },
      state: {
        type: String
      },
      country: {
        type: String
      }
    },
    location: {
      type: {
        type: String,
        default: "Point"
      },
      coordinates: [
        {
          type: Number
        }
      ],
      name: {
        type: String,
        default: null
      }
    }
  },
  {
    timestamps: true
  }
);
profileDetailsSchema.index({
  "location.coordinates": "2d"
});
profileDetailsSchema.index({
  "location.coordinates": "2dsphere"
});
const profileDetails = mongoose.model("profileDetails", profileDetailsSchema);
module.exports = profileDetails;
/**
 * @swagger
 * definitions:
 *   location:
 *     type: object
 *     properties:
 *       coordinates:
 *         type: array
 *       name:
 *         type: string
 *   address_details:
 *     type: object
 *     properties:
 *       city:
 *         type: string
 *       state:
 *         type: string
 *       country:
 *         type: string
 *       fullAddress:
 *         type: string
 *   socialLink:
 *     type: object
 *     properties:
 *       facebook:
 *         type: string
 *       instagram:
 *         type: string
 *       twitter:
 *         type: string
 *   profileUpdate:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       contactNumber:
 *         type: string
 *       tattooLicence:
 *         type: string
 *       tattooLicenceDoc:
 *         type: string
 *       stateLicence:
 *         type: string
 *       specialization:
 *         type: array
 *       gender:
 *         type: string
 *       about:
 *         type: string
 *       socialLink:
 *         $ref : "#/definitions/socialLink"
 *       coverImage:
 *         type: string
 *       profileImage:
 *         type: string
 *       gallery:
 *         type: array
 *       studioName:
 *         type: string
 *       address:
 *         $ref : "#/definitions/address_details"
 *       location:
 *         $ref : "#/definitions/location"
 */
/**
 * @swagger
 * definitions:
 *   location:
 *     type: object
 *     properties:
 *       coordinates:
 *         type: array
 *       name:
 *         type: string
 *   address_details:
 *     type: object
 *     properties:
 *       city:
 *         type: string
 *       state:
 *         type: string
 *       country:
 *         type: string
 *       full_address:
 *         type: string
 *       latitude:
 *         type: number
 *       longitude:
 *         type: number
 *   socialLink:
 *     type: object
 *     properties:
 *       facebook:
 *         type: string
 *       instagram:
 *         type: string
 *       twitter:
 *         type: string
 *   profileUpdateRequest:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       contactNumber:
 *         type: string
 *       tattooLicence:
 *         type: string
 *       drivingLicence:
 *         type: string
 *       stateLicence:
 *         type: string
 *       specialization:
 *         type: array
 *       about:
 *         type: string
 *       socialLink:
 *         $ref : "#/definitions/socialLink"
 *       studioName:
 *         type: string
 *       address:
 *         $ref : "#/definitions/address_details"
 *       location:
 *         $ref : "#/definitions/location"
 */

/**
 * @swagger
 * definitions:
 *   themeUpdate:
 *     properties:
 *       themeId:
 *         type: string
 *       themeName:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   addFaq:
 *     properties:
 *       title:
 *         type: string
 *       description:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   updateFaq:
 *     properties:
 *       faqId:
 *         type: string
 *       description:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   updateNotificationAlert:
 *     properties:
 *       notificationAlert:
 *         type: boolean
 */

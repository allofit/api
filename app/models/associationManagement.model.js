const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const constants = require("../..//constants");

const { Schema } = mongoose;

const associationManagementSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    userProfileId: {
      type: Schema.Types.ObjectId,
      ref: "profileDetails"
    },
    joinedRequest: [
      {
        type: Schema.Types.ObjectId,
        ref: "user"
      }
    ],
    sentRequest: [
      {
        type: Schema.Types.ObjectId,
        ref: "user"
      }
    ],
    receivedRequest: [
      {
        type: Schema.Types.ObjectId,
        ref: "user"
      }
    ]
  },
  {
    timestamps: true
  }
);

const associationManagement = mongoose.model(
  "associationManagement",
  associationManagementSchema
);

module.exports = associationManagement;

/**
 * @swagger
 * definitions:
 *   associationManagement:
 *     properties:
 *       userId:
 *         type: string
 *       joined:
 *         type: array
 *       requested:
 *         type: array
 *       receivedRequest:
 *         type: array
 */

/**
 *@swagger
 * definitions:
 *   joinUsers:
 *     properties:
 *       joinId:
 *         type: string
 */

/**
 *@swagger
 * definitions:
 *   joinRequestAction:
 *     properties:
 *       joinId:
 *         type: string
 *       action:
 *         type: string
 */

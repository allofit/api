const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;

let supportConversationSchema = new Schema(
  {
    message: { type: String },
    by: { type: String, enum: ["user", "support"] }
  },
  { timestamps: true }
);

let helpAndSupportSchema = new Schema(
  {
    userId: { type: Schema.Types.ObjectId, ref: "user" },
    title: { type: String },
    query: { type: String },
    conversation: [supportConversationSchema],
    ticketNo: {
      type: String,
      default:
        "T-" +
        Math.random()
          .toString(36)
          .substr(2, 5)
    },
    status: {
      type: String,
      enum: ["open", "processing", "closed"],
      default: "open"
    }
  },
  { timestamps: true }
);
helpAndSupportSchema.plugin(mongoosePaginate);
let helpAndSupport = mongoose.model("helpAndSupport", helpAndSupportSchema);
let supportConversation = mongoose.model(
  "supportConversation",
  supportConversationSchema
);

module.exports = { helpAndSupport, supportConversation };

/**
 * @swagger
 * definitions:
 *   generateTicket:
 *     properties:
 *       title:
 *         type: string
 *       message:
 *         type: string
 *       by:
 *         type: string
 *         enum:
 *           - support
 *           - user
 */

/**
 * @swagger
 * definitions:
 *   sendTicketMessage:
 *     properties:
 *       ticketId:
 *         type: string
 *       message:
 *         type: string
 *       by:
 *         type: string
 *         enum:
 *           - support
 *           - user
 */

/**
 * @swagger
 * definitions:
 *   ticketStatus:
 *     properties:
 *       ticketId:
 *         type: string
 *       status:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   messageUpdate:
 *     properties:
 *       ticketId:
 *         type: string
 *       message:
 *         type: string
 *       messageId:
 *         type: string
 */

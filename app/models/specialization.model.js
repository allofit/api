const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const specializationSchema = new Schema(
  {
    specializationName: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String
    },
    status: {
      type: String,
      enum: ["active", "inactive"],
      default: "active"
    }
  },
  {
    timestamps: true
  }
);
specializationSchema.plugin(mongoosePaginate);
const specialization = mongoose.model("specialization", specializationSchema);

module.exports = specialization;

(function init() {
  let obj = {
    imageUrl:
      "https://iqpad.s3.amazonaws.com/guideImage/1563190074263_guideImage-1563190072410.jpeg",
    specializationName: "Dot Tattoo"
  };
  let obj1 = {
    imageUrl:
      "https://iqpad.s3.amazonaws.com/guideImage/1563190072457_guideImage-1563190072408.jpeg",
    specializationName: "Line Tattoo"
  };
  let obj2 = {
    imageUrl:
      "https://iqpad.s3.amazonaws.com/guideImage/1563190075736_guideImage-1563190072411.jpeg",
    specializationName: "Traditional Tattoo"
  };
  let obj3 = {
    imageUrl:
      "https://iqpad.s3.amazonaws.com/eventImage/1563190085857_guideImage-1563190072411.jpeg",
    specializationName: "Black Tattoo"
  };
  let obj4 = {
    imageUrl:
      "https://iqpad.s3.amazonaws.com/eventImage/1563190082602_guideImage-1563190072408.jpeg",
    specializationName: "3D Tattoo"
  };

  mongoose
    .model("specialization", specializationSchema)
    .findOne({}, (err, result) => {
      if (err) console.log("Welcome Page creation at findOne error--> ", err);
      else if (!result) {
        mongoose
          .model("specialization", specializationSchema)
          .create(obj, (err, success) => {
            mongoose
              .model("specialization", specializationSchema)
              .create(obj1, (err, success) => {
                mongoose
                  .model("specialization", specializationSchema)
                  .create(obj2, (err, success) => {
                    mongoose
                      .model("specialization", specializationSchema)
                      .create(obj3, (err, success) => {
                        mongoose
                          .model("specialization", specializationSchema)
                          .create(obj4, (err, success) => {});
                      });
                  });
              });
          });
      } else {
        console.log("Specialization.");
      }
    });
})();

/**
 * @swagger
 * definitions:
 *   specialization:
 *     properties:
 *       specializationName:
 *         type: string
 *       imageUrl:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   updateSpecialization:
 *     properties:
 *       specializationId:
 *         type: string
 *       specializationName:
 *         type: string
 *       imageUrl:
 *         type: string
 */

// /**
//  * @swagger
//  * definitions:
//  *   deleteSpecialization:
//  *     properties:
//  *       specializationId:
//  *         type: string
//  */

const mongoose = require("mongoose");
const { Schema } = mongoose;
const consentFormSchema = new Schema(
  {
    text: { type: String }
  },
  {
    timestamps: true
  }
);

const consentForm = mongoose.model("consentForm", consentFormSchema);
module.exports = consentForm;

/**
 * @swagger
 * definitions:
 *   consentData:
 *     properties:
 *       text:
 *         type: string
 */

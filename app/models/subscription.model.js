const mongoose = require("mongoose");
var mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const subscriptionSchema = new Schema(
  {
    name: {
      type: String
    },
    duration: {
      type: Number
    },
    amount: {
      type: String
    },
    userType: {
      type: String,
      enum: ["artist", "studio"]
    },
    description: {
      type: String
    },
    status: {
      type: String,
      default: "active",
      enum: ["active", "inactive"]
    },
    subscriptionType: {
      type: String,
      default: "trial"
    },
    trialDays: {
      type: Number,
      default: 30
    }
  },
  {
    timestamps: true
  }
);

subscriptionSchema.plugin(mongoosePaginate);
const subscription = mongoose.model("subscription", subscriptionSchema);
module.exports = subscription;

(function init() {
  let obj = {
    name: "Studio trial subscription",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    duration: 30,
    amount: "0",
    userType: "studio"
  };
  let obj1 = {
    name: "Artist trial subscription",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    duration: 30,
    amount: "0",
    userType: "artist"
  };

  mongoose
    .model("subscription", subscriptionSchema)
    .findOne({}, (err, result) => {
      if (err) console.log("FAQ created at findOne error--> ", err);
      else if (!result) {
        mongoose
          .model("subscription", subscriptionSchema)
          .create(obj, (err, success) => {
            mongoose
              .model("subscription", subscriptionSchema)
              .create(obj1, (err, success) => {});
          });
      } else {
        console.log("Subscription.");
      }
    });
})();

/**
 * @swagger
 * definitions:
 *   addSubscription:
 *     properties:
 *       name:
 *         type: string
 *       duration:
 *         type: number
 *       amount:
 *         type: string
 *       userType:
 *         type: string
 *       description:
 *         type: string
 *       status:
 *         type: string
 *       subscriptionType:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   updateSubscription:
 *     properties:
 *       subscriptionId:
 *         type: string
 *       name:
 *         type: string
 *       duration:
 *         type: string
 *       amount:
 *         type: string
 *       description:
 *         type: string
 *       status:
 *         type: string
 *       userType:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   searchData:
 *     properties:
 *       searchKey:
 *         type: string
 *       searchBody:
 *         type: string
 */

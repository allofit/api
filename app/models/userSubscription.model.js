const mongoose = require("mongoose");
var mongoosePaginate = require("mongoose-paginate");
var mongooseAggregatePaginate = require("mongoose-aggregate-paginate");

const { Schema } = mongoose;

const userSubscriptionSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    subscriptionId: {
      type: Schema.Types.ObjectId,
      ref: "subscription"
    },
    purchaseDate: {
      type: Date,
      default: Date.now()
    },
    startDate: {
      type: Date
    },
    endDate: {
      type: Date
    },
    subscriptionType: {
      type: String,
      default: "trial"
    }
  },
  {
    timestamps: true
  }
);

userSubscriptionSchema.plugin(mongoosePaginate);
userSubscriptionSchema.plugin(mongooseAggregatePaginate);

const userSubscription = mongoose.model(
  "userSubscription",
  userSubscriptionSchema
);
module.exports = userSubscription;

/**
 * @swagger
 * definitions:
 *   buySubscription:
 *     properties:
 *       subscriptionId:
 *         type: string
 *       startDate:
 *         type: string
 *       endDate:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   userFirstSubscription:
 *     properties:
 *       userType:
 *         type: string
 */

const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
var mongooseAggregatePaginate = require("mongoose-aggregate-paginate");
const { Schema } = mongoose;
const bookingModelSchema = new Schema(
  {
    paymentCollectedByArtist: {
      type: Boolean,
      default: false
    },
    bookingFee: {
      type: Number,
      default: 0
    },
    stripeFee: {
      type: Number,
      default: 0
    },
    isBookingConfirmByArtist: {
      type: Boolean,
      default: false
    },
    isBookingConfirmByClient: {
      type: Boolean,
      default: false
    },
    cancellationReason: {
      type: String
    },
    rescheduleBookingId: {
      type: Schema.Types.ObjectId,
      ref: "rescheduleBooking"
    },
    isBookingReschedule: {
      type: Boolean,
      default: false
    },
    isRescheduleConfirmByArtist: {
      type: Boolean,
      default: false
    },
    isRescheduleConfirmByClient: {
      type: Boolean,
      default: false
    },
    isBookingCompleted: {
      type: Boolean,
      default: false
    },
    additionalNotes: {
      type: String
    },
    bookingDuration: {
      type: String
    },
    bookingTitle: {
      type: String
    },
    tattooDescription: {
      type: String
    },
    tattooColor: {
      type: String
    },
    tattooSizing: {
      width: String,
      height: String
    },
    tattooPlacement: {
      type: String
    },
    referenceImages: [
      {
        type: String
      }
    ],
    notes: [
      {
        type: String
      }
    ],
    // subject: {
    //   type: String
    // },
    fullAmountByArtist: {
      type: String
    },
    depositAmountByArtist: {
      type: String
    },
    clientPaidDepositAmount: {
      type: Boolean,
      default: false
    },
    clientPaidFullAmount: {
      type: Boolean,
      default: false
    },
    bookingOrderId: {
      type: Schema.Types.ObjectId,
      ref: "bookingOrder"
    },
    bookingStatus: {
      type: String,
      default: "pending",
      enum: ["pending", "completed", "cancelled"]
    },
    artistId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    clientId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    date: {
      type: String
    },
    dateArray: {
      type: Array
    },
    selectedDateByArtist: {
      type: String
    },
    day: {
      type: String
    },
    totalHour: Number,
    noOfDays: Number,
    location: {
      type: {
        type: String,
        default: "Point"
      },
      coordinates: [
        {
          type: Number
        }
      ],
      name: {
        type: String,
        default: null
      }
    },
    studioId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    }
  },
  {
    timestamps: true
  }
);
bookingModelSchema.plugin(mongoosePaginate);
bookingModelSchema.plugin(mongooseAggregatePaginate);
const bookingData = mongoose.model("bookingData", bookingModelSchema);
module.exports = bookingData;

/**
 * @swagger
 * definitions:
 *   tattooSizing:
 *     properties:
 *       width:
 *         type: string
 *       height:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   bookingData:
 *     properties:
 *       additionalNotes:
 *         type: string
 *       bookingDuration:
 *         type: string
 *       bookingTitle:
 *         type: string
 *       tattooColor:
 *         type: string
 *       tattooSizing:
 *         $ref: '#/definitions/tattooSizing'
 *       tattooPlacement:
 *         type: string
 *       address:
 *         type: array
 *       notes:
 *         type: array
 *       subject:
 *         type: string
 *       day:
 *         type: string
 *       date:
 *         type: string
 *       startTime:
 *         type: string
 *       endTime:
 *         type: string
 *       clientId:
 *         type: string
 *       location:
 *         type: object
 *         properties:
 *           coordinates:
 *             type: array
 *             items:
 *               type: number
 *           name:
 *             type: string
 *       studioId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   bookingSlotList:
 *     properties:
 *       date:
 *         type: string
 *       artistId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   studioFilterSlots:
 *     properties:
 *       date:
 *         type: string
 *       artistId:
 *         type: string
 *       studioId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   changeBookingStatus:
 *     properties:
 *       bookingId:
 *         type: string
 *       status:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   completeBooking:
 *     properties:
 *       bookingId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   addBookingAmount:
 *     properties:
 *       depositAmountByArtist:
 *         type: string
 *       fullAmountByArtist:
 *         type: string
 *       bookingId:
 *         type: string
 *       paidAmountType:
 *         type: string
 */
//addBookingAmount fullAmountByArtist, bookingId, depositAmountByArtist

/**
 * @swagger
 * definitions:
 *   cancelBookingByClient:
 *     properties:
 *       bookingId:
 *         type: string
 *       paymentId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   rescheduleBooking:
 *     properties:
 *       bookingId:
 *         type: string
 *       date:
 *         type: string
 *       slots:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             startTime:
 *               type: string
 *             endTime:
 *               type: string
 */

const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;
const calendarModelSchema = new Schema(
  {
    artistId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    day: {
      type: String
    },
    date: {
      type: String
    },
    startTime: Number,
    endTime: Number,
    totalHour: Number,
    location: {
      type: {
        type: String,
        default: "Point"
      },
      coordinates: [
        {
          type: Number
        }
      ],
      name: {
        type: String,
        default: null
      }
    },
    studioId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    }
  },
  {
    timestamps: true
  }
);
const calendar = mongoose.model("calendar", calendarModelSchema);
module.exports = calendar;

/**
 * @swagger
 * definitions:
 *   addSlotsToCalendar:
 *     properties:
 *       day:
 *         type: string
 *       date:
 *         type : string
 *       startTime:
 *         type: string
 *       endTime:
 *         type: string
 *       location:
 *         type: object
 *         properties:
 *           coordinates:
 *             type: array
 *             items:
 *               type: number
 *           name:
 *             type: string
 *       studioId:
 *         type: string
 */

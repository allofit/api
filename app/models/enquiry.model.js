const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;

let conversationSchema = new Schema(
  {
    message: { type: String },
    by: { type: String, enum: ["user", "support"] }
  },
  { timestamps: true }
);

const enquirySchema = new Schema(
  {
    conversation: [conversationSchema],
    status: {
      type: String,
      enum: ["open", "closed"],
      default: "open"
    },
    bookingTitle: {
      type: String
    },
    enquiryType: {
      type: String,
      enum: ["Price", "Time"]
    },
    tattooColor: {
      type: String
    },
    tattooSizing: {
      width: String,
      height: String
    },
    tattooPlacement: {
      type: String
    },
    address: Array,

    artistId: {
      type: String,
      ref: "user"
    },
    refArtistId: {
      type: String,
      ref: "user"
    },
    studioId: {
      type: String,
      ref: "user"
    },
    userId: {
      type: String,
      ref: "user"
    },
    referenceImages: [
      {
        type: String
      }
    ],
    notes: [
      {
        type: String
      }
    ],
    subject: {
      type: String
    }
  },
  {
    timestamps: true
  }
);
enquirySchema.plugin(mongoosePaginate);
const enquiry = mongoose.model("enquiry", enquirySchema);

module.exports = enquiry;

/**
 * @swagger
 * definitions:
 *   enquiry:
 *     properties:
 *       artistId:
 *         type: string
 *       refArtistId:
 *         type: string
 *       studioId:
 *         type: string
 *       bookingTitle:
 *         type: string
 *       tattooColor:
 *         type: string
 *       tattooPlacement:
 *         type: string
 *       address:
 *         type: array
 *       referenceImages:
 *         type: array
 *       notes:
 *         type: array
 *       subject:
 *         type: string
 *       tattooSizing:
 *         type: object
 *         properties:
 *           width:
 *             type: string
 *           height:
 *             type: string
 */

/**
 * @swagger
 * definitions:
 *   sendEnquiryMessage:
 *     properties:
 *       enquiryId:
 *         type: string
 *       message:
 *         type: string
 *       by:
 *         type: string
 *         enum:
 *           - support
 *           - user
 */

/**
 * @swagger
 * definitions:
 *   enquiryStatus:
 *     properties:
 *       enquiryId:
 *         type: string
 *       status:
 *         type: string
 */

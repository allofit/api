const mongoose = require("mongoose");
const schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

let rescheduleBookingSchema = new schema({
  date: {
    type: String
  },
  startTime: {
    type: Number
  },
  endTime: {
    type: Number
  },
  totalHour: Number,
  bookingId: {
    type: schema.Types.ObjectId,
    ref: "bookingData"
  },
  isRescheduleConfirm: {
    type: Boolean,
    default: false
  },
  isArtistRespond: {
    type: Boolean,
    default: false
  },
  paymentId: {
    type: schema.Types.ObjectId,
    ref: "paymentDetails"
  }
});

rescheduleBookingSchema.plugin(mongoosePaginate);

const rescheduleBooking = mongoose.model(
  "rescheduleBooking",
  rescheduleBookingSchema
);

module.exports = rescheduleBooking;

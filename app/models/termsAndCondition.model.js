const mongoose = require("mongoose");
const { Schema } = mongoose;
const termsAndConditionSchema = new Schema(
  {
    text: { type: String }
  },
  {
    timestamps: true
  }
);

const termsAndCondition = mongoose.model(
  "termsAndCondition",
  termsAndConditionSchema
);
module.exports = termsAndCondition;

/**
 * @swagger
 * definitions:
 *   t&cData:
 *     properties:
 *       text:
 *         type: string
 */

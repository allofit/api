const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const constants = require("../..//constants");
var mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const userSchema = new Schema(
  {
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    email: {
      type: String,
      unique: true
    },
    password: {
      type: String,
      select: false
    },
    rememberMe: {
      type: Boolean,
      default: false
    },
    associationId: {
      type: Schema.Types.ObjectId,
      ref: "associationManagement"
    },
    profileId: {
      type: Schema.Types.ObjectId,
      ref: "profileDetails"
    },
    userType: {
      type: String,
      enum: ["artist", "studio", "client", "admin"]
    },
    facebookId: {
      type: String
    },
    twitterId: {
      type: String
    },
    instagramId: {
      type: String
    },
    emailVerified: {
      type: Boolean,
      default: false
    },
    status: {
      type: String,
      enum: ["active", "inactive"],
      default: "active"
    },
    token: {
      type: String
    },
    webToken: {
      type: String
    },
    emailToken: {
      type: String
    },
    emailLinkTime: {
      type: String
    },
    userName: {
      type: String,
      unique: true
    },
    referredBy: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    appliedCoupon: {
      type: String,
      unique: true
    }
  },
  {
    timestamps: true
  }
);

userSchema.methods.hash = password =>
  bcrypt.hashSync(password, constants.saltRounds);

userSchema.methods.compare = (password, hash) =>
  bcrypt.compareSync(password, hash);
userSchema.plugin(mongoosePaginate);

const user = mongoose.model("user", userSchema);
module.exports = user;

(function init() {
  let obj = {
    firstName: "All Of It",
    lastName: "Admin",
    email: "allofit_admin@gmail.com",
    password: "12345678",
    userType: "admin"
  };
  let salt = bcrypt.genSaltSync(10);
  obj.password = bcrypt.hashSync(obj.password, salt);
  mongoose
    .model("user", userSchema)
    .findOne({ userType: "admin" }, (err, result) => {
      if (err) console.log("Super Admin creation at findOne error--> ", err);
      else if (!result) {
        mongoose.model("user", userSchema).create(obj, (err, success) => {
          if (err)
            console.log("Super Admin creation at create method error--> ", err);
          else
            console.log(
              "Super Admin creation at create method success--> ",
              success
            );
        });
      } else {
        console.log("Admin.");
      }
    });
})();
/**
 * @swagger
 * definitions:
 *   user:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       email:
 *         type: string
 *         format : email
 *       password:
 *         type: string
 *         format : password
 *       userType:
 *         type: string
 *         enum:
 *           - artist
 *           - studio
 *           - client
 *       emailVerified:
 *         type: boolean
 *         default: false
 *       status:
 *         type: string
 *         enum:
 *           - active
 *           - inactive
 *       token:
 *         type: string
 *       emailToken:
 *         type: string
 *       rememberMe:
 *         type: boolean
 */

/**
 * @swagger
 * definitions:
 *   userSignup:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       email:
 *         type: string
 *         format : email
 *       password:
 *         type: string
 *         format : password
 *       userName:
 *         type: string
 *       webToken:
 *         type: string
 *       userType:
 *         type: string
 *         enum:
 *           - artist
 *           - studio
 *           - client
 */

/**
 * @swagger
 * definitions:
 *   userUpdate:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       userType:
 *         type: string
 *         enum:
 *           - artist
 *           - studio
 *           - client
 */

/**
 * @swagger
 * definitions:
 *   login:
 *     properties:
 *       user:
 *         type: string
 *         format : email
 *       password:
 *         type: string
 *         format : password
 *       webToken:
 *         type: string
 *       rememberMe:
 *         type: boolean
 */

/**
 * @swagger
 * definitions:
 *   socialLogin:
 *     properties:
 *       facebookId:
 *         type: string
 *       twitterId:
 *         type: string
 *       instagramId:
 *         type: string
 *       email:
 *         type: string
 *         format : email
 *       firstName:
 *         type: string
 *       emailVerified:
 *         type: boolean
 *         default: true
 */

/**
 * @swagger
 * definitions:
 *   changePassword:
 *     properties:
 *       oldPassword:
 *         type: string
 *       newPassword:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   forgotPassword:
 *     properties:
 *       email:
 *         type: string
 *         format : email
 */

/**
 * @swagger
 * definitions:
 *   resetPassword:
 *     properties:
 *       emailToken:
 *         type: string
 *       password:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   shareProfileLink:
 *     properties:
 *       email:
 *         type: array
 *       shareableProfileLink:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   userListAdmin:
 *     properties:
 *       userType:
 *         type: string
 *       searchKey:
 *         type: string
 *       searchBody:
 *         type: string
 *       page:
 *         type: number
 *       limit:
 *         type: number
 */

/**
 * @swagger
 * definitions:
 *   blockUserAdmin:
 *     properties:
 *       userId:
 *         type: string
 *       action:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   userUpdateAdmin:
 *     properties:
 *       userId:
 *         type: string
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       contactNumber:
 *         type: string
 *       gender:
 *         type: string
 *       tattooLicence:
 *         type: string
 *       tattooLicenceDoc:
 *         type: string
 *       stateLicence:
 *         type: string
 *       specialization:
 *         type: array
 *       about:
 *         type: string
 *       socialLink:
 *         $ref : "#/definitions/socialLink"
 *       coverImage:
 *         type: string
 *       profileImage:
 *         type: string
 *       gallery:
 *         type: array
 *       studioName:
 *         type: string
 *       address:
 *         $ref : "#/definitions/address_details"
 *       location:
 *         $ref : "#/definitions/location"
 *       userName:
 *         type: string
 *       status:
 *         type: string
 *       password:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   userDetailsAdmin:
 *     properties:
 *       userId:
 *         type: string
 */

const mongoose = require("mongoose");
var mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const subscriptionPaymentSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    subscriptionId: {
      type: Schema.Types.ObjectId,
      ref: "subscription"
    },
    amount: {
      type: String
    },
    paymentType: {
      type: String
    },
    paymentStatus: {
      type: String,
      default: "pending"
    },
    paymentId: {
      type: String,
      ref: "paymentDetails"
    },
    transactionId: {
      type: String,
      ref: "paymentDetails"
    },
    days: {
      type: Number
    }
  },
  {
    timestamps: true
  }
);

subscriptionPaymentSchema.plugin(mongoosePaginate);
const subscriptionPayment = mongoose.model(
  "subscriptionPayment",
  subscriptionPaymentSchema
);
module.exports = subscriptionPayment;

/**
 * @swagger
 * definitions:
 *   addSubscriptionPayment:
 *     properties:
 *       subscriptionId:
 *         type: string
 *       paymentType:
 *         type: string
 */

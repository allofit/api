const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;
const messageSchema = new Schema(
  {
    participants: [
      {
        type: Schema.Types.ObjectId,
        ref: "user"
      }
    ],
    conversation: [
      new Schema(
        {
          senderId: {
            type: Schema.Types.ObjectId,
            ref: "user"
          },
          receiverId: {
            type: Schema.Types.ObjectId,
            ref: "user"
          },
          deletedUserId: [
            {
              type: Schema.Types.ObjectId,
              ref: "user"
            }
          ],
          isViewed: {
            type: Boolean,
            default: false
          },
          message: {
            type: String
          }
        },
        {
          timestamps: true
        }
      )
    ]
  },
  {
    timestamps: true
  }
);
const message = mongoose.model("message", messageSchema);
module.exports = message;

/**
 * @swagger
 * definitions:
 *   sendMessage:
 *     properties:
 *       senderId:
 *         type: string
 *       receiverId:
 *         type: string
 *       message:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   deleteMessage:
 *     properties:
 *       documentId:
 *         type: string
 *       messageId:
 *         type: string
 */

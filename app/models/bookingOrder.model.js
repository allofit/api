const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;
const bookingOrderSchema = new Schema(
  {
    paymentCollectedByArtist: {
      type: Boolean,
      default: false
    },
    isBookingCompleted: {
      type: Boolean,
      default: false
    },
    studioId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    artistId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    clientId: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    date: String,
    startTime: String,
    endTime: String,
    paymentMode: {
      type: String
    },
    paymentStatus: {
      type: String,
      default: "pending",
      enum: ["pending", "partialCompleted", "completed", "cancelled, refunded"]
    },
    bookingStatus: {
      type: String,
      default: "pending",
      enum: ["pending", "completed", "cancelled"]
    },
    amountPaid: {
      type: String
    },
    paymentId: {
      type: Schema.Types.ObjectId,
      ref: "paymentDetails"
    },
    transactionId: {
      type: String,
      ref: "paymentDetails"
    },
    bookingId: {
      type: String,
      default:
        "TATTOO-" +
        Math.random()
          .toString(36)
          .substring(7)
          .toUpperCase()
    },
    bookingDataId: {
      type: Schema.Types.ObjectId,
      ref: "bookingData"
    }
  },
  {
    timestamps: true
  }
);
const bookingOrder = mongoose.model("bookingOrder", bookingOrderSchema);
module.exports = bookingOrder;

/**
 * @swagger
 * definitions:
 *   bookingOrder:
 *     properties:
 *       slots:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             date:
 *               type: string
 *             startTime:
 *               type: string
 *             endTime:
 *               type: string
 *       artistId:
 *         type: string
 *       studioId:
 *         type: string
 *       paymentMode:
 *         type: string
 *       totalAmount:
 *         type: string
 *       transactionId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   checkAvailableSlot:
 *     properties:
 *       artistId:
 *         type: string
 *       date:
 *         type: string
 */

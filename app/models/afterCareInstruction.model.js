const mongoose = require("mongoose");
const { Schema } = mongoose;
const afterCareInstructionSchema = new Schema(
  {
    text: { type: String }
  },
  {
    timestamps: true
  }
);

const afterCareInstruction = mongoose.model(
  "afterCareInstruction",
  afterCareInstructionSchema
);
module.exports = afterCareInstruction;

/**
 * @swagger
 * definitions:
 *   afterCareData:
 *     properties:
 *       text:
 *         type: string
 */

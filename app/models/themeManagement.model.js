const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;
const themeManagementSchema = new Schema(
  {
    userId: {
      type: String,
      ref: "user"
    },
    accent: {
      type: String
    },
    accentSecondary: {
      type: String
    },

    background: {
      type: String
    },
    font: {
      type: String
    },
    fontSecondary: {
      type: String
    }
  },
  {
    timestamps: true
  }
);
const themeSchema = mongoose.model("themeSchema", themeManagementSchema);
module.exports = themeSchema;

/**
 * @swagger
 * definitions:
 *   themeSchema:
 *     properties:
 *       studioId:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   updateUserThemeData:
 *     properties:
 *       accent:
 *         type: string
 *       accentSecondary:
 *         type: string
 *       background:
 *         type: string
 *       font:
 *         type: string
 *       fontSecondary:
 *         type: string
 */

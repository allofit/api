const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const notificationSchema = new Schema(
  {
    notificationTitle: {
      type: String
    },
    notificationBody: {
      type: String
    },
    notificationType: {
      type: String
    },
    userId: {
      type: mongoose.Types.ObjectId,
      ref: "user"
    },
    senderId: {
      type: mongoose.Types.ObjectId,
      ref: "user"
    },
    receiverId: {
      type: mongoose.Types.ObjectId,
      ref: "user"
    }
  },
  {
    timestamps: true
  }
);

const notification = mongoose.model("notification", notificationSchema);
module.exports = notification;

const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const enquirySchema = {
  userEnquiryList: Joi.object().keys({
    page: Joi.number().required(),
    limit: Joi.number().required()
  }),
  artistEnquiryList: Joi.object().keys({
    page: Joi.number().required(),
    limit: Joi.number().required()
  })
};

module.exports = {
  "/userEnquiryList": enquirySchema.userEnquiryList,
  "/artistEnquiryList": enquirySchema.artistEnquiryList,
  "/saveEnquiry": enquirySchema.saveEnquiry
};

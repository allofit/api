const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const schema = {
  generateTicket: Joi.object({
    title: Joi.string().required(),
    message: Joi.string().required(),
    by: Joi.string()
      .valid("user", "support")
      .required()
  }),
  sendMessage: Joi.object({
    ticketId: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    message: Joi.string().required(),
    by: Joi.string()
      .valid("user", "admin")
      .required()
  }),
  updateTicketStatus: Joi.object({
    ticketId: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    status: Joi.string()
      .valid("open", "closed", "processing")
      .required()
  }),
  delete: Joi.object({
    ticket: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required()
  }),
  updateMessage: Joi.object({
    ticketId: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    messageId: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    message: Joi.string().required()
  }),
  deleteMessage: Joi.object({
    ticket: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    user: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    messageId: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required()
  })
};

module.exports = {
  "/generateTicket": schema.generateTicket,
  "/sendMessage": schema.sendMessage,
  "/update": schema.update,
  "/delete": schema.delete,
  "/update/message": schema.updateMessage,
  "/delete/message": schema.deleteMessage
};

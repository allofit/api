const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const calendarSchema = {
  addSlotsInCalendar: Joi.object({
    day: Joi.string().required(),
    date: Joi.string().required(),
    startTime: Joi.string().required(),
    endTime: Joi.string().required(),
    location: Joi.object().optional(),
    studioId: Joi.string().optional()
  }),
  updateSlot: Joi.object().keys({
    slotId: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    location: Joi.object().optional(),
    studioId: Joi.string().optional()
  }),
  getDefaultMonthSlotList: Joi.object().keys({
    // id: Joi.string().regex(objectIdRegex, objectIdRegMsg).required(),
    date: Joi.string().required(),
    totalDays: Joi.number().required()
  })
};

module.exports = {
  "/saveSlotsInCalendar": calendarSchema.saveSlotsInCalendar,
  "/updateSlot": calendarSchema.updateSlot,
  "/getDefaultMonthSlotList": calendarSchema.getDefaultMonthSlotList
};

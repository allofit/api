const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const schema = {
  passwordReset: Joi.object().keys({
    emailToken: Joi.string().required(),
    password: Joi.string().required()
  }),
  forgetPassword: Joi.object().keys({
    email: Joi.string().required()
  }),
  socialLogin: Joi.object().keys({
    facebookId: Joi.string().optional(),
    twitterId: Joi.string().optional(),
    instagramId: Joi.string().optional(),
    email: Joi.string().required(),
    firstName: Joi.string().required()
  }),
  login: Joi.object().keys({
    user: Joi.string().required(),
    password: Joi.string().required(),
    rememberMe: Joi.boolean().optional()
  }),
  changePassword: Joi.object().keys({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string()
      .min(8)
      .max(15)
      .required()
  })
};

module.exports = {
  "/login": schema.login,
  "/login/admin": schema.login,
  "/password/change": schema.changePassword,
  "/socialLogin": schema.socialLogin,
  "/forgetPassword": schema.forgetPassword,
  "/passwordReset": schema.passwordReset
};

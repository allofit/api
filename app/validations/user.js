const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const userSchema = {
  register: Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string()
      .email({ minDomainAtoms: 2 })
      .required(),
    password: Joi.string()
      .min(8)
      .max(15)
      .required(),
    userType: Joi.string()
      .valid("artist", "studio", "client")
      .required()
  }),
  verify: Joi.object().keys({
    email: Joi.string()
      .email({ minDomainAtoms: 2 })
      .optional(),
    password: Joi.string()
      .min(8)
      .max(15)
      .required()
  }),
  shareProfileLink: Joi.object().keys({
    email: Joi.array().required(),
    shareableProfileLink: Joi.string().required()
  }),
  update: Joi.object().keys({
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional(),
    password: Joi.string()
      .min(8)
      .max(15)
      .optional(),
    userType: Joi.string()
      .valid("artist", "studio", "client")
      .optional(),
    emailVerified: Joi.boolean().optional(),
    status: Joi.string()
      .valid("active", "inactive")
      .optional()
  }),
  joinCancellationList: Joi.object().keys({
    artistId: Joi.string().required()
  })
};

module.exports = {
  "/register": userSchema.register,
  "/verify": userSchema.verify,
  "/update": userSchema.update,
  "/shareProfileLink": userSchema.shareProfileLink,
  "/joinCancellationList": userSchema.joinCancellationList
};

const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const profileDetailsSchema = {
  updateUserFaq: Joi.object({
    faqId: Joi.string().required(),
    title: Joi.string().required(),
    description: Joi.string().required()
  })
};

module.exports = {
  "/updateUserFaq": profileDetailsSchema.updateUserFaq
};

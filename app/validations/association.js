const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const userSchema = {
  register: Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string()
      .email({ minDomainAtoms: 2 })
      .required(),
    password: Joi.string()
      .min(8)
      .max(15)
      .required(),
    userType: Joi.string()
      .valid("artist", "studio", "client")
      .required()
  }),
  verify: Joi.object().keys({
    email: Joi.string()
      .email({ minDomainAtoms: 2 })
      .optional(),
    password: Joi.string()
      .min(8)
      .max(15)
      .required()
  }),
  update: Joi.object().keys({
    // id: Joi.string().regex(objectIdRegex, objectIdRegMsg).required(),
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional(),
    password: Joi.string()
      .min(8)
      .max(15)
      .optional(),
    userType: Joi.string()
      .valid("artist", "studio", "client")
      .optional(),
    emailVerified: Joi.boolean().optional(),
    status: Joi.string()
      .valid("active", "inactive")
      .optional()
  })
};

module.exports = {
  "/register": userSchema.register,
  "/verify": userSchema.verify,
  "/update": userSchema.update
};

const Joi = require("joi");

const objectIdRegex = /^[0-9a-fA-F]{24}$/;
const objectIdRegMsg = "Object ID";

const schema = {
  addSpecialization: Joi.object().keys({
    specializationName: Joi.string().required(),
    imageUrl: Joi.string().required()
  }),
  delete: Joi.object().keys({
    specializationId: Joi.string().required()
  }),
  editSpecialization: Joi.object().keys({
    specializationName: Joi.string().required(),
    imageUrl: Joi.string().required(),
    specializationId: Joi.string().required()
  }),
  changePassword: Joi.object().keys({
    id: Joi.string()
      .regex(objectIdRegex, objectIdRegMsg)
      .required(),
    oldPassword: Joi.string().required(),
    newPassword: Joi.string()
      .min(8)
      .max(15)
      .required()
  }),
  resend: Joi.object().keys({
    user: Joi.string().required()
  })
};

module.exports = {
  "/addSpecialization": schema.addSpecialization,
  "/login": schema.login,
  "/password/reset": schema.resetPassword,
  "/login/admin": schema.login,
  "/password/change": schema.changePassword,
  "/resend": schema.resend
};

const express = require("express");
const swaggerJSDoc = require("swagger-jsdoc");

const userRoutes = require("./api/user.route");
const authRoutes = require("./api/auth.route");
const adminRoutes = require("./api/admin.route");
const associationRoutes = require("./api/association.route");
const calendarRoutes = require("./api/calendar.route");
const profileRoutes = require("./api/profileDetails.route");
const enquiryRoutes = require("./api/enquiry.route");
const bookingRoutes = require("./api/bookingData.route");
const themeRoutes = require("./api/theme.routes");
const MessageRoute = require("./api/message.route");
const SubscriptionRoute = require("./api/userSubscription.route");
const SupportRoute = require("./api/support.route");
const paymentRoute = require("./api/payment.route");
const notificationRoute = require("./api/notification.route");
const staticPageRoute = require("./api/staticPages.route");

var config = require("../../config");
const router = express.Router();

router.use("/user", userRoutes);
router.use("/auth", authRoutes);
router.use("/admin", adminRoutes);
router.use("/profile", profileRoutes);
router.use("/association", associationRoutes);
router.use("/calendar", calendarRoutes);
router.use("/enquiry", enquiryRoutes);
router.use("/booking", bookingRoutes);
router.use("/theme", themeRoutes);
router.use("/message", MessageRoute);
router.use("/subscription", SubscriptionRoute);
router.use("/support", SupportRoute);
router.use("/payment", paymentRoute);
router.use("/notification", notificationRoute);
router.use("/staticPage", staticPageRoute);

//swagger code
// swagger definition
const swaggerDefinition = {
  info: {
    title: "All-Of-It API Explorer",
    version: "1.0.0",
    description: "Available REST Endpoints of All-Of-It RESTful API"
  },
  host: `${config.host}:${process.env.PORT}/api`,
  basePath: "/",
  securityDefinitions: {
    JWT: {
      description: "",
      type: "apiKey",
      name: "authenticate",
      in: "header"
    }
  }
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ["app/routes/**/*.js", "app/models/*.js"]
};

// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);
/**
 * @swagger
 * responses:
 *   Unauthorized:
 *     description: Unauthorized
 *   BadRequest:
 *     description: BadRequest / Invalid Input
 */

/**
 * @swagger
 * /:
 *   get:
 *     tags:
 *       - Status
 *     description: Returns API status
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: API Status
 */
router.get("/swagger.json", (req, res) => {
  res.status(200).json(swaggerSpec);
});

router.get("/", (req, res) => {
  res.json("Welcome to All-Of-It APIs.");
});

module.exports = router;

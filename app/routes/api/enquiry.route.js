const express = require("express");
const validationSchema = require("../../validations/enquiry");
const validator = require("../../validations/validator");
const enquiryController = require("../../controllers/enquiry.controller");
const AuthService = require("../../services/auth");
const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const uploadImage = require("../../services/upload");
const router = express.Router();

/**
 * @swagger
 * /enquiry/saveEnquiry:
 *   post:
 *     tags:
 *       - Artist Enquiry
 *     description: Save Enquiry Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Enquiry Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/enquiry'
 *     responses:
 *       200:
 *         description: Data Saved Successfully
 *         schema:
 *           $ref: '#/definitions/enquiry'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/saveEnquiry",
  authenticate,
  uploadImage.array("referenceImages", 10),
  enquiryController.saveEnquiry
);

/**
 * @swagger
 * /enquiry/sendEnquiryMessage:
 *   post:
 *     tags:
 *       - Artist Enquiry
 *     description: Send new message
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Message
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/sendEnquiryMessage'
 *     responses:
 *       200:
 *         description: Message send successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post(
  "/sendEnquiryMessage",
  authenticate,
  validateRequest,
  enquiryController.sendEnquiryMessage
);

/**
 * @swagger
 * /enquiry/enquiryList:
 *   get:
 *     tags:
 *       - Artist Enquiry
 *     description: List of Enquiry
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Pagination
 *         in: query
 *         required: true
 *         type: string
 *       - name: page
 *         description: Pagination
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown Successfully
 *         schema:
 *           $ref: '#/definitions/enquiry'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/enquiryList", authenticate, enquiryController.enquiryList);

/**
 * @swagger
 * /enquiry/getStudioArtistList:
 *   get:
 *     tags:
 *       - Artist Enquiry
 *     description: List of Studios
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: User Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown Successfully
 *         schema:
 *           $ref: '#/definitions/enquiry'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/getStudioArtistList",
  authenticate,
  enquiryController.getStudioArtistList
);

/**
 * @swagger
 * /enquiry/updateEnquiryStatus:
 *   put:
 *     tags:
 *       - Artist Enquiry
 *     description: Update Enquiry status
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Enquiry status
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/enquiryStatus'
 *     responses:
 *       200:
 *         description: Enquiry updated successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.put(
  "/updateEnquiryStatus",
  authenticate,
  enquiryController.updateEnquiryStatus
);

/**
 * @swagger
 * /enquiry/deleteEnquiryMessage:
 *   delete:
 *     tags:
 *       - Artist Enquiry
 *     description: Delete Message
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: messageId
 *         description: Message Id
 *         in: query
 *         required: true
 *         type: string
 *       - name: enquiryId
 *         description: Enquiry Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Message deleted successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.delete(
  "/deleteEnquiryMessage",
  authenticate,
  validateRequest,
  enquiryController.deleteEnquiryMessage
);

/**
 * @swagger
 * /enquiry/deleteEnquiry:
 *   delete:
 *     tags:
 *       - Artist Enquiry
 *     description: Delete Ticket
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enquiryId
 *         description: Enquiry Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Enquiry deleted successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete(
  "/deleteEnquiry",
  authenticate,
  validateRequest,
  enquiryController.deleteEnquiry
);

/**
 * @swagger
 * /enquiry/getEnquiryDetails:
 *   get:
 *     tags:
 *       - Artist Enquiry
 *     description: Get ticket details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enquiryId
 *         description: Enquiry Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Enquiry details shown successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getEnquiryDetails",
  authenticate,
  enquiryController.getEnquiryDetails
);

module.exports = router;

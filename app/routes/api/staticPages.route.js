const express = require("express");
const validationSchema = require("../../validations/profileDetails");
const validator = require("../../validations/validator");
const staticPageController = require("../../controllers/staticPage.controller");
const AuthService = require("../../services/auth");
//const authenticate = AuthService.auth;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /staticPage/afterCareData:
 *   post:
 *     tags:
 *       - Static Page
 *     description: Add AfterCare Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: After Case Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/afterCareData'
 *     responses:
 *       200:
 *         description: Data added successfully.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/afterCareData", staticPageController.afterCareData);

/**
 * @swagger
 * /staticPage/termsAndConditionData:
 *   post:
 *     tags:
 *       - Static Page
 *     description: Add T&C Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Terms and Condition Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/t&cData'
 *     responses:
 *       200:
 *         description: Data added successfully.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post(
  "/termsAndConditionData",
  staticPageController.termsAndConditionData
);

/**
 * @swagger
 * /staticPage/consentData:
 *   post:
 *     tags:
 *       - Static Page
 *     description: Add Consent Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Consent  Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/consentData'
 *     responses:
 *       200:
 *         description: Data added successfully.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("consentData", staticPageController.consentData);

/**
 * @swagger
 * /staticPage/getAfterCareData:
 *   get:
 *     tags:
 *       - Static Page
 *     description: Get AfterCare Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: afterCareId
 *         description: After Care Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: After Care details.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/getAfterCareData", staticPageController.getAfterCareData);

/**
 * @swagger
 * /staticPage/getTermsAndConditionData:
 *   get:
 *     tags:
 *       - Static Page
 *     description: Get TermsAndCondition Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: termsAndConditionId
 *         description: Terms and Condition Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Terms and condition details.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getTermsAndConditionData",
  staticPageController.getTermsAndConditionData
);

/**
 * @swagger
 * /staticPage/getConsentData:
 *   get:
 *     tags:
 *       - Static Page
 *     description: Get Consent Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: consentId
 *         description: Terms and Condition Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Terms and condition details.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/getConsentData", staticPageController.getConsentData);
module.exports = router;

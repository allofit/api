const express = require("express");
const validationSchema = require("../../validations/user");
const validator = require("../../validations/validator");
const NotificationController = require("../../controllers/notification.controller");
const AuthService = require("../../services/auth");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /notification/notificationList:
 *   post:
 *     tags:
 *       - Notification
 *     description: Notification List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: notificationType
 *         description: Notification Type
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Notification List.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/notificationList",
  authenticate,
  NotificationController.notificationList
);

// /**
//  * @swagger
//  * /message/deleteMessage:
//  *   delete:
//  *     tags:
//  *       - Message
//  *     description: Delete Message
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: messageId
//  *         description: Message Id
//  *         in: query
//  *         required: true
//  *         type: string
//  *     responses:
//  *       200:
//  *         description: Message Deleted.
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

//  router.delete('/deleteMessage', authenticate, MessageController.deleteMessage);

// /**
//  * @swagger
//  * /message/viewMessage:
//  *   get:
//  *     tags:
//  *       - Message
//  *     description: View Message
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: messageId
//  *         description: Message Id
//  *         in: query
//  *         required: true
//  *         type: string
//  *     responses:
//  *       200:
//  *         description: Message Shown.
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

//  router.get('/viewMessage', authenticate, MessageController.viewMessage);

//  /**
//  * @swagger
//  * /message/messageList:
//  *   get:
//  *     tags:
//  *       - Message
//  *     description: Message list
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     responses:
//  *       200:
//  *         description: Message List Shown.
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

//  router.get('/messageList', authenticate, MessageController.messageList);

// /**
//  * @swagger
//  * /message/deleteChat:
//  *   delete:
//  *     tags:
//  *       - Message
//  *     description: Delete Chat
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: chatId
//  *         description: Chat Id
//  *         in: query
//  *         required: true
//  *         type: string
//  *     responses:
//  *       200:
//  *         description: Chat Deleted.
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

//  router.delete('/deleteChat', authenticate, MessageController.deleteChat);

module.exports = router;

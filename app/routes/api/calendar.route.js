const express = require("express");
const validationSchema = require("../../validations/calendar");
const validator = require("../../validations/validator");
const CalendarController = require("../../controllers/calendar.controller");
const AuthService = require("../../services/auth");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /calendar/addSlotsInCalendar:
 *   post:
 *     tags:
 *       - Artist Calendar
 *     description: Add new slots in calendar
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Slots Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/addSlotsToCalendar'
 *     responses:
 *       200:
 *         description: Slots Successfully Added
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/addSlotsInCalendar",
  authenticate,
  validateRequest,
  CalendarController.addSlotsInCalendar
);

/**
 * @swagger
 * /calendar/getCalendarSlots:
 *   get:
 *     tags:
 *       - Artist Calendar
 *     description: List of slot
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: startDate
 *         description: Slots start date
 *         in: query
 *         required: true
 *         type: string
 *       - name: endDate
 *         description: Slots end date
 *         in: query
 *         required: true
 *         type: string
 *       - name: artistId
 *         description: Artist Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Slot Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/getCalendarSlots",
  authenticate,
  CalendarController.getCalendarSlots
);

/**
 * @swagger
 * /calendar/getRemainingHour:
 *   get:
 *     tags:
 *       - Artist Calendar
 *     description: List of remaining hour slot
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: startDate
 *         description: Slots start date
 *         in: query
 *         required: true
 *         type: string
 *       - name: endDate
 *         description: Slots end date
 *         in: query
 *         required: true
 *         type: string
 *       - name: artistId
 *         description: Artist Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Slot Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/getRemainingHour",
  authenticate,
  CalendarController.getRemainingHour
);

/**
 * @swagger
 * /calendar/getArtistAvailableDateList:
 *   get:
 *     tags:
 *       - Artist Calendar
 *     description: List of slot
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: date
 *         description: Today date
 *         in: query
 *         required: true
 *         type: string
 *       - name: artistId
 *         description: Artist Id
 *         in: query
 *         required: true
 *         type: string
 *       - name: reqHour
 *         description: Required Hour
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Slot Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getArtistAvailableDateList",
  authenticate,
  CalendarController.getArtistAvailableDateList
);
//getArtistAvailableDateList
/**
 * @swagger
 * /calendar/deleteSlotFromCalendar:
 *   delete:
 *     tags:
 *       - Artist Calendar
 *     description: Delete Slot
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: slotId
 *         description: Slots Data
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Deleted Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.delete(
  "/deleteSlotFromCalendar",
  authenticate,
  CalendarController.deleteSlotFromCalendar
);

module.exports = router;

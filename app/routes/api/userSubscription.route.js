const express = require("express");
const validationSchema = require("../../validations/enquiry");
const validator = require("../../validations/validator");
const subscriptionController = require("../../controllers/subscription.controller");
const AuthService = require("../../services/auth");
const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /subscription/subscriptionList:
 *   get:
 *     tags:
 *       - User Subscription
 *     description: Subscription List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userType
 *         description: User type
 *         in: query
 *         required: true
 *         type: string
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: false
 *         type: string
 *       - name: page
 *         description: Page
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Subscription list shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/subscriptionList", subscriptionController.subscriptionList);

/**
 * @swagger
 * /subscription/buySubscription:
 *   post:
 *     tags:
 *       - User Subscription
 *     description: Buy new subscription
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Buy subscription
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/addSubscriptionPayment'
 *     responses:
 *       200:
 *         description: Subscription purchased Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/buySubscription",
  authenticate,
  subscriptionController.buySubscription
);

/**
 * @swagger
 * /subscription/userSubscriptionDetails:
 *   get:
 *     tags:
 *       - User Subscription
 *     description: User Subscription Details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: User subscription shown successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/userSubscriptionDetails",
  authenticate,
  subscriptionController.userSubscriptionDetails
);

/**
 * @swagger
 * /subscription/firstSubscriptionBuy:
 *   post:
 *     tags:
 *       - User Subscription
 *     description: First subscription
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: User Type
 *         description: User Type
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/userFirstSubscription'
 *     responses:
 *       200:
 *         description: Subscription subscribed Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/firstSubscriptionBuy",
  authenticate,
  subscriptionController.firstSubscriptionBuy
);

/**
 * @swagger
 * /subscription/subscriptionHistory:
 *   get:
 *     tags:
 *       - User Subscription
 *     description: User subscription history
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: false
 *         type: string
 *       - name: page
 *         description: Page
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Subscription history shown successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/subscriptionHistory",
  authenticate,
  subscriptionController.subscriptionHistory
);

module.exports = router;

const express = require("express");
const validationSchema = require("../../validations/profileDetails");
const validator = require("../../validations/validator");
const profileDetailsController = require("../../controllers/profileDetails.controller");
const AuthService = require("../../services/auth");
const uploadImage = require("../../services/upload");
//const authenticate = AuthService.auth;
const validateRequest = validator(false, validationSchema);
const router = express.Router();
/**
 * @swagger
 * /profile/update:
 *   put:
 *     tags:
 *       - Customer Profile
 *     description: Update customer
 *     security:
 *       - JWT: []
 *     consumes:
 *       - multipart/form-data
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: formData
 *         name: profileImage
 *         type: file
 *         description: Upload profileImage.
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/profileUpdateRequest'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *         schema:
 *           $ref: '#/definitions/profileUpdate'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put(
  "/update",
  AuthService.jwtAuthenticate,
  uploadImage.fields([
    {
      name: "profileImage"
    },
    {
      name: "coverImage"
    },
    // {
    //     name: 'drivingLicenceDoc'
    // },
    {
      name: "tattooLicenceDoc"
    },
    {
      name: "gallery",
      maxCount: 10
    }
  ]),
  profileDetailsController.update
);
/**
 * @swagger
 * /profile/getFaqList:
 *   get:
 *     tags:
 *       - Customer Profile
 *     description: Get customer details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getFaqList",
  AuthService.jwtAuthenticate,
  profileDetailsController.getFaqList
);
/**
 * @swagger
 * /profile/getFaq/{faqId}:
 *   get:
 *     tags:
 *       - Customer Profile
 *     description: Get customer details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: faqId
 *         description: FaqId you want to get data.
 *         in: path
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getFaq/:faqId",
  AuthService.jwtAuthenticate,
  profileDetailsController.getFaq
);
/**
 * @swagger
 * /profile/updateUserFaq:
 *   put:
 *     tags:
 *       - Customer Profile
 *     description: Update FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateUserFaq'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put(
  "/updateUserFaq",
  AuthService.jwtAuthenticate,
  validateRequest,
  profileDetailsController.updateUserFaq
);

/**
 * @swagger
 * /profile/addFaq:
 *   post:
 *     tags:
 *       - Customer Profile
 *     description: Add FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/addFaq'
 *     responses:
 *       200:
 *         description: Successfully aDDED
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post(
  "/addFaq",
  AuthService.jwtAuthenticate,
  profileDetailsController.addFaq
);

/**
 * @swagger
 * /profile/removeFaq/{faqId}:
 *   delete:
 *     tags:
 *       - Customer Profile
 *     description: Delete Faq
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: faqId
 *         description: FaqId you want to delete.
 *         in: path
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully Deleted
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete(
  "/removeFaq/:faqId",
  AuthService.jwtAuthenticate,
  profileDetailsController.removeFaq
);

/**
 * @swagger
 * /profile/updateFaq:
 *   put:
 *     tags:
 *       - Customer Profile
 *     description: Update FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateUserFaq'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put(
  "/updateFaq",
  AuthService.jwtAuthenticate,
  profileDetailsController.updateFaq
);

/**
 * @swagger
 * /profile/updateNotificationAlert:
 *   put:
 *     tags:
 *       - Customer Profile
 *     description: Update notification alert
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Notification alert
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateNotificationAlert'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put(
  "/updateNotificationAlert",
  AuthService.jwtAuthenticate,
  profileDetailsController.updateNotificationAlert
);

module.exports = router;

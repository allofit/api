const express = require("express");
const validationSchema = require("../../validations/user");
const validator = require("../../validations/validator");
const UserController = require("../../controllers/user.controller");
const AuthService = require("../../services/auth");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /user/register:
 *   post:
 *     tags:
 *       - Customer
 *     description: Create new customer
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/userSignup'
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post("/register", validateRequest, UserController.register);
router.get("/emailVerify", UserController.emailVerify);

/**
 * @swagger
 * /user/update:
 *   put:
 *     tags:
 *       - Customer
 *     description: Update customer
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/userUpdate'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.put("/update", authenticate, validateRequest, UserController.update);

/**
 * @swagger
 * /user/get:
 *   get:
 *     tags:
 *       - Customer
 *     description: Get customer details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/get", authenticate, UserController.get);

/**
 * @swagger
 * /user/getProfileData:
 *   get:
 *     tags:
 *       - Customer
 *     description: Get customer profile details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/getProfileData", authenticate, UserController.getProfileData);

/**
 * @swagger
 * /user/delete:
 *   delete:
 *     tags:
 *       - Customer
 *     description: Delete customer
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: UserId you want to delete.
 *         in: query
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully Deleted
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.delete("/delete", authenticate, UserController.delete);

/**
 * @swagger
 * /user/getProfileDataPublic:
 *   get:
 *     tags:
 *       - Customer
 *     description: Get customer profile details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: userName
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/getProfileDataPublic", UserController.getProfileDataPublic);

/**
 * @swagger
 * /user/shareProfileLink:
 *   post:
 *     tags:
 *       - Customer
 *     description: Share profile link
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/shareProfileLink'
 *     responses:
 *       200:
 *         description: Sent Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/shareProfileLink",
  validateRequest,
  UserController.shareProfileLink
);

/**
 * @swagger
 * /user/joinCancellationList:
 *   post:
 *     tags:
 *       - Customer
 *     description: Join Cancellation List
 *     produces:
 *       - application/json
 *     security:
 *       - JWT: []
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/joinCancellationList'
 *     responses:
 *       200:
 *         description: List Join Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/joinCancellationList",
  validateRequest,
  authenticate,
  UserController.joinCancellationList
);

/**
 * @swagger
 * /user/getCancellationList:
 *   get:
 *     tags:
 *       - Customer
 *     description: Get Cancellation List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/getCancellationList",
  authenticate,
  UserController.getCancellationList
);

/**
 * @swagger
 * /user/getStudioList:
 *   get:
 *     tags:
 *       - Customer
 *     description: Get Studio List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/getStudioList", authenticate, UserController.getStudioList);

/**
 * @swagger
 * /user/specializationListForUser:
 *   get:
 *     tags:
 *       - Customer
 *     description: Specialization List
 *     security:
 *       - JWT: []
 *     responses:
 *       200:
 *         description: Specialization List Shown Successfully
 *         schema:
 *           $ref: '#/definitions/specialization'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/specializationListForUser",
  authenticate,
  UserController.specializationListForUser
);

/**
 * @swagger
 * /user/getAllProfileLinks:
 *   get:
 *     tags:
 *       - Customer
 *     description: Get public profile details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/getAllProfileLinks", UserController.getAllProfileLinks);
//  router.get('/stripePayment', UserController.stripePayment)

module.exports = router;

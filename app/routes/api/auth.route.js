const express = require("express");
const validationSchema = require("../../validations/auth");
const validator = require("../../validations/validator");
const AuthService = require("../../services/auth");

const validateRequest = validator(false, validationSchema);
const router = express.Router();
const authenticate = AuthService.jwtAuthenticate;

// router.post('/verify-otp', validateRequest, AuthService.verifyOTP);
/**
 * @swagger
 * /auth/login:
 *   post:
 *     tags:
 *       - Customer Auth
 *     description: Login customer
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/login'
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/login", validateRequest, AuthService.login);
/**
 * @swagger
 * /auth/socialLogin:
 *   post:
 *     tags:
 *       - Customer Auth
 *     description: Social Login customer
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/socialLogin'
 *     responses:
 *       200:
 *         description: Successfully Logged in
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/socialLogin", AuthService.socialLogin);
/**
 * @swagger
 * /auth/password/forget:
 *   post:
 *     tags:
 *       - Customer Auth
 *     description: Forgot Password customer
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/forgotPassword'
 *     responses:
 *       200:
 *         description: Link send successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/password/forget", validateRequest, AuthService.forgetPassword);

/**
 * @swagger
 * /auth/tokenVerify/{token}:
 *   get:
 *     tags:
 *       - Customer Auth
 *     description: Token verification
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token verification.
 *         in: path
 *         type: string
 *     responses:
 *       200:
 *         description: Token Matched
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/tokenVerify/:token", AuthService.tokenVerify);

/**
 * @swagger
 * /auth/passwordReset:
 *   post:
 *     tags:
 *       - Customer Auth
 *     description: Reset password customer
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: New password
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/resetPassword'
 *     responses:
 *       200:
 *         description: Successfully updated
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/passwordReset", validateRequest, AuthService.passwordReset);

/**
 * @swagger
 * /auth/password/change:
 *   post:
 *     tags:
 *       - Customer Auth
 *     description: Change password customer
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/changePassword'
 *     responses:
 *       200:
 *         description: Successfully Changed
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post(
  "/password/change",
  authenticate,
  validateRequest,
  AuthService.changePassword
);

// router.post('/password/reset', validateRequest, AuthService.resetPassword);
// router.post('/login/admin', validateRequest, AuthService.adminLogin);
// router.post('/resend', validateRequest, AuthService.resendOtp);
// router.get('/logout', AuthService.logout);
// router.put('/password/forgot/admin', AuthService.adminForgotPassword);

module.exports = router;

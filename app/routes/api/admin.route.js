const express = require("express");
const adminController = require("../../controllers/admin.controller");
const AuthService = require("../../services/auth");
const uploadImage = require("../../services/upload");

const authenticate = AuthService.jwtAuthenticate;
const router = express.Router();

/**
 * @swagger
 * /admin/login:
 *   post:
 *     tags:
 *       - Admin
 *     description: Login Admin
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Admin Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/login'
 *     responses:
 *       200:
 *         description: Successfully Login
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/login", adminController.login);

/**
 * @swagger
 * /admin/password/forget:
 *   post:
 *     tags:
 *       - Admin
 *     description: Admin forgot password
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Admin data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/forgotPassword'
 *     responses:
 *       200:
 *         description: Link send successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/password/forget", AuthService.forgetPassword);

/**
 * @swagger
 * /admin/addSpecialization:
 *   post:
 *     tags:
 *       - Admin
 *     description: Create new Specification
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Specification's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/specialization'
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/specialization'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post(
  "/addSpecialization",
  uploadImage.single("imageUrl"),
  adminController.addSpecialization
);

/**
 * @swagger
 * /admin/specializationList:
 *   post:
 *     tags:
 *       - Admin
 *     description: Specialization List
 *     security:
 *       - JWT: []
 *     parameters:
 *       - name: limit
 *         description: limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: page
 *         in: query
 *         required: true
 *         type: number
 *       - name: body
 *         description: Searching Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/searchData'
 *     responses:
 *       200:
 *         description: Specialization List Shown Successfully
 *         schema:
 *           $ref: '#/definitions/specialization'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/specializationList", adminController.specializationList);

// *       - name: searchKey
// *         description: search key
// *         in: query
// *         required: false
// *         type: string
// *       - name: searchBody
// *         description: search body
// *         in: query
// *         required: false
// *         type: string
/**
 * @swagger
 * /admin/updateSpecialization:
 *   put:
 *     tags:
 *       - Admin
 *     description: Update Specification
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Specification's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateSpecialization'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *         schema:
 *           $ref: '#/definitions/specialization'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put(
  "/updateSpecialization",
  uploadImage.single("imageUrl"),
  adminController.updateSpecialization
);

/**
 * @swagger
 * /admin/deleteSpecialization:
 *   delete:
 *     tags:
 *       - Admin
 *     description: delete Specialization
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: specializationId
 *         description: Specialization Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Specialization Deleted Successfully
 *         schema:
 *           $ref: '#/definitions/specialization'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete("/deleteSpecialization", adminController.deleteSpecialization);

/**
 * @swagger
 * /admin/addFaq:
 *   post:
 *     tags:
 *       - Admin
 *     description: Create new FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/faq'
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/addFaq", adminController.addFaq);

/**
 * @swagger
 * /admin/faqList:
 *   get:
 *     tags:
 *       - Admin
 *     description: FAQ List
 *     security:
 *       - JWT: []
 *     responses:
 *       200:
 *         description: FAQ List Shown Successfully
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/faqList", adminController.faqList);

/**
 * @swagger
 * /admin/updateFaq:
 *   put:
 *     tags:
 *       - Admin
 *     description: Update FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Specification's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateFaq'
 *     responses:
 *       200:
 *         description: Successfully Updated
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put("/updateFaq", adminController.updateFaq);

/**
 * @swagger
 * /admin/deleteFaq:
 *   delete:
 *     tags:
 *       - Admin
 *     description: delete FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: faqId
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: FAQ Deleted Successfully
 *         schema:
 *           $ref: '#/definitions/faq'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete("/deleteFaq", adminController.deleteFaq);

/**
 * @swagger
 * /admin/userList:
 *   post:
 *     tags:
 *       - Admin
 *     description: Create new FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/userListAdmin'
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/userList", authenticate, adminController.userList);

/**
 * @swagger
 * /admin/blockUser:
 *   post:
 *     tags:
 *       - Admin
 *     description: Create new FAQ
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/blockUserAdmin'
 *       - name: auth
 *         in: header
 *         description: an authorization header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/blockUser", adminController.blockUser);

/**
 * @swagger
 * /admin/updateUser:
 *   put:
 *     tags:
 *       - Admin
 *     description: User details update
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: FAQ's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/userUpdateAdmin'
 *     responses:
 *       200:
 *         description: Successfully Created
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put(
  "/updateUser",
  uploadImage.fields([
    {
      name: "profileImage"
    },
    {
      name: "coverImage"
    },
    // {
    //   name: "drivingLicenceDoc"
    // },
    {
      name: "tattooLicenceDoc"
    },
    {
      name: "gallery",
      maxCount: 10
    }
  ]),
  adminController.updateUser
);

//updateUser

/**
 * @swagger
 * /admin/userDetails:
 *   post:
 *     tags:
 *       - Admin
 *     description: User Details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: User Id
 *         in: query
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/userDetailsAdmin'
 *     responses:
 *       200:
 *         description: Details shown successfully
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/userDetails", adminController.userDetails);

/**
 * @swagger
 * /admin/addSubscription:
 *   post:
 *     tags:
 *       - Admin
 *     description: Add Subscription
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Subscription details
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/addSubscription'
 *     responses:
 *       200:
 *         description: Subscription added successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/addSubscription", adminController.addSubscription);

/**
 * @swagger
 * /admin/subscriptionList:
 *   post:
 *     tags:
 *       - Admin
 *     description: Subscription List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: body
 *         description: Searching Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/searchData'
 *     responses:
 *       200:
 *         description: Subscription list shown successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/subscriptionList", adminController.subscriptionList);

/**
 * @swagger
 * /admin/deleteSubscription:
 *   delete:
 *     tags:
 *       - Admin
 *     description: Delete Subscription
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: subscriptionId
 *         description: subscriptionId
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Subscription delete successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete("/deleteSubscription", adminController.deleteSubscription);

/**
 * @swagger
 * /admin/updateSubscription:
 *   put:
 *     tags:
 *       - Admin
 *     description: Update Subscription Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Subscription details
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateSubscription'
 *     responses:
 *       200:
 *         description: Subscription updated successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put("/updateSubscription", adminController.updateSubscription);

/**
 * @swagger
 * /admin/ticketList:
 *   get:
 *     tags:
 *       - Admin
 *     description: Ticket List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: status
 *         description: Ticket status
 *         in: query
 *         required: false
 *         type: string
 *       - name: limit
 *         description: limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: page
 *         in: query
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Ticket List Shown Successfully
 *         schema:
 *           $ref: '#/definitions/specialization'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/ticketList", authenticate, adminController.ticketList);

/**
 * @swagger
 * /admin/deleteTicket:
 *   delete:
 *     tags:
 *       - Admin
 *     description: Delete Ticket
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: ticketId
 *         description: Ticket Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Ticket deleted successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete("/deleteTicket", authenticate, adminController.deleteTicket);

/**
 * @swagger
 * /admin/userCount:
 *   get:
 *     tags:
 *       - Admin
 *     description: User count
 *     security:
 *       - JWT: []
 *     responses:
 *       200:
 *         description: data Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/userCount", adminController.userCount);

/**
 * @swagger
 * /admin/bookingCount:
 *   get:
 *     tags:
 *       - Admin
 *     description: Booking count
 *     security:
 *       - JWT: []
 *     parameters:
 *       - name: startDate
 *         description: Start date
 *         in: query
 *         required: true
 *         type: string
 *       - name: endDate
 *         description: End date
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Data Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/bookingCount", adminController.bookingCount);

/**
 * @swagger
 * /admin/bookingList:
 *   get:
 *     tags:
 *       - Admin
 *     description: Booking List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: searchKey
 *         description: Search key
 *         in: query
 *         required: false
 *         type: string
 *       - name: type
 *         description: Calendar type
 *         in: query
 *         required: true
 *         type: string
 *       - name: date
 *         description: Today's Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         required: false
 *         type: string
 *       - name: sorting
 *         description: Sorting
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/bookingList", adminController.bookingList);

/**
 * @swagger
 * /admin/bookingDetails :
 *   get:
 *     tags:
 *       - Admin
 *     description: Get booking details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: bookingId
 *         description: Booking Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Data Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/bookingDetails", adminController.bookingDetails);

/**
 * @swagger
 * /admin/cancelBooking:
 *   put:
 *     tags:
 *       - Admin
 *     description: Update booking status
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Booking status
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/changeBookingStatus'
 *     responses:
 *       200:
 *         description: Booking updated successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.put("/cancelBooking", adminController.cancelBooking);

/**
 * @swagger
 * /admin/subscriptionUserList:
 *   get:
 *     tags:
 *       - Admin
 *     description: List of expired trial
 *     security:
 *       - JWT: []
 *     parameters:
 *       - name: date
 *         description: Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: userType
 *         description: User type
 *         in: query
 *         required: true
 *         type: string
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: string
 *       - name: page
 *         description: page
 *         in: query
 *         required: true
 *         type: string
 *       - name: listType
 *         description: List type
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Data Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/subscriptionUserList", adminController.subscriptionUserList);

module.exports = router;

const express = require("express");
const validationSchema = require("../../validations/association");
const validator = require("../../validations/validator");
const BookingController = require("../../controllers/bookingData.controller");
const AuthService = require("../../services/auth");
const uploadImage = require("../../services/upload");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /booking/artistStudioList:
 *   get:
 *     tags:
 *       - Booking
 *     description: List of studio List
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: List of studio and artist
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/artistStudioList", BookingController.artistStudioList);

//bookingSlotList

// /**
//  * @swagger
//  * /booking/bookingSlotList:
//  *   post:
//  *     tags:
//  *       - Booking
//  *     description: Booking slot list
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: body
//  *         description: Booking Slot list
//  *         in: body
//  *         required: true
//  *         type: string
//  *         schema:
//  *           $ref: '#/definitions/bookingSlotList'
//  *     responses:
//  *       200:
//  *         description: Booking successfully
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

// router.post("/bookingSlotList", BookingController.bookingSlotList);

/**
 * @swagger
 * /booking/addBooking:
 *   post:
 *     tags:
 *       - Booking
 *     description: Add Booking
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Booking Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/bookingOrder'
 *     responses:
 *       200:
 *         description: Booking successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/addBooking",
  authenticate,
  uploadImage.array("referenceImages", 10),
  BookingController.addBooking
);

//limit, page, searchKey, searchBody
/**
 * @swagger
 * /booking/clientBookingList:
 *   get:
 *     tags:
 *       - Booking
 *     description: Booking List for Client
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: searchKey
 *         description: Search key
 *         in: query
 *         required: false
 *         type: string
 *       - name: type
 *         description: Calendar type
 *         in: query
 *         required: true
 *         type: string
 *       - name: date
 *         description: Today's Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/clientBookingList",
  authenticate,
  BookingController.clientBookingList
);

/**
 * @swagger
 * /booking/artistBookingList:
 *   get:
 *     tags:
 *       - Booking
 *     description: Booking List for Client
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: searchKey
 *         description: Search key
 *         in: query
 *         required: false
 *         type: string
 *       - name: type
 *         description: Calendar type
 *         in: query
 *         required: true
 *         type: string
 *       - name: date
 *         description: Today's Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/artistBookingList",
  authenticate,
  BookingController.artistBookingList
);

/**
 * @swagger
 * /booking/studioBookingList:
 *   get:
 *     tags:
 *       - Booking
 *     description: Booking List for Client
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: searchKey
 *         description: Search key
 *         in: query
 *         required: false
 *         type: string
 *       - name: type
 *         description: Calendar type
 *         in: query
 *         required: true
 *         type: string
 *       - name: date
 *         description: Today's Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/studioBookingList",
  authenticate,
  BookingController.studioBookingList
);
//studioBookingList

/**
 * @swagger
 * /booking/artistBookedSlot:
 *   get:
 *     tags:
 *       - Booking
 *     description: Booking List for Artist
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: searchKey
 *         description: Search key
 *         in: query
 *         required: false
 *         type: string
 *       - name: type
 *         description: Calendar type
 *         in: query
 *         required: true
 *         type: string
 *       - name: date
 *         description: Today's Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/artistBookedSlot",
  authenticate,
  BookingController.artistBookedSlot
);

/**
 * @swagger
 * /booking/studioBookedSlot:
 *   get:
 *     tags:
 *       - Booking
 *     description: Booking List for Studio
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: true
 *         type: number
 *       - name: page
 *         description: Page
 *         in: query
 *         required: true
 *         type: number
 *       - name: searchKey
 *         description: Search key
 *         in: query
 *         required: false
 *         type: string
 *       - name: type
 *         description: Calendar type
 *         in: query
 *         required: true
 *         type: string
 *       - name: date
 *         description: Today's Date
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/studioBookedSlot",
  authenticate,
  BookingController.studioBookedSlot
);

/**
 * @swagger
 * /booking/getBookingDetails :
 *   get:
 *     tags:
 *       - Booking
 *     description: Get booking details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: bookingId
 *         description: Booking Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Data Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/getBookingDetails", BookingController.getBookingDetails);

// /**
//  * @swagger
//  * /booking/studioFilterSlots:
//  *   post:
//  *     tags:
//  *       - Booking
//  *     description: Booking slot list
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: body
//  *         description: Booking Slot list
//  *         in: body
//  *         required: true
//  *         type: string
//  *         schema:
//  *           $ref: '#/definitions/studioFilterSlots'
//  *     responses:
//  *       200:
//  *         description: Booking successfully
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

// router.post("/studioFilterSlots", BookingController.studioFilterSlots);

/**
 * @swagger
 * /booking/clientListOfArtist:
 *   get:
 *     tags:
 *       - Booking
 *     description: Client list of artist
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Booking successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/clientListOfArtist",
  authenticate,
  BookingController.clientListOfArtist
);

/**
 * @swagger
 * /booking/clientBookingListForArtist:
 *   get:
 *     tags:
 *       - Booking
 *     description: Client list of artist
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: clientId
 *         description: Client Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Booking successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/clientBookingListForArtist",
  authenticate,
  BookingController.clientBookingListForArtist
);
//clientBookingListForArtist

/**
 * @swagger
 * /booking/changeBookingStatus:
 *   post:
 *     tags:
 *       - Booking
 *     description: Booking slot list
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Change booking status
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/changeBookingStatus'
 *     responses:
 *       200:
 *         description: Booking successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post("/changeBookingStatus", BookingController.changeBookingStatus);

/**
 * @swagger
 * /booking/completeBooking:
 *   post:
 *     tags:
 *       - Booking
 *     description: Complete booking
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Slot Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/completeBooking'
 *     responses:
 *       200:
 *         description: Booking update successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post("/completeBooking", BookingController.completeBooking);

/**
 * @swagger
 * /booking/paymentCollectedByArtist:
 *   post:
 *     tags:
 *       - Booking
 *     description: Payment Collected
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Slot Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/completeBooking'
 *     responses:
 *       200:
 *         description: Booking update successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/paymentCollectedByArtist",
  BookingController.paymentCollectedByArtist
);

/**
 * @swagger
 * /booking/cancelBookingByClient:
 *   post:
 *     tags:
 *       - Booking
 *     description: Cancel booking by client
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Cancel booking data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/cancelBookingByClient'
 *     responses:
 *       200:
 *         description: Booking cancelled successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/cancelBookingByClient",
  authenticate,
  BookingController.cancelBookingByClient
);

/**
 * @swagger
 * /booking/rescheduleBooking:
 *   post:
 *     tags:
 *       - Booking
 *     description: Reschedule booking
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Reschedule Booking data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/rescheduleBooking'
 *     responses:
 *       200:
 *         description: Booking reschedule request sent successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/rescheduleBooking",
  authenticate,
  BookingController.rescheduleBooking
);

/**
 * @swagger
 * /booking/rescheduleBookingList :
 *   get:
 *     tags:
 *       - Booking
 *     description: Get reschedule booking list
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/rescheduleBookingList",
  authenticate,
  BookingController.rescheduleBookingList
);

/**
 * @swagger
 * /booking/rescheduleBookingDetails :
 *   get:
 *     tags:
 *       - Booking
 *     description: Get reschedule booking list
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: bookingId
 *         description: Booking Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List Shown
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/rescheduleBookingDetails",
  authenticate,
  BookingController.rescheduleBookingDetails
);

/**
 * @swagger
 * /booking/bookingData:
 *   get:
 *     tags:
 *       - Booking
 *     description: Booking Data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: bookingId
 *         description: Booking Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Data Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/bookingData", BookingController.bookingData);

/**
 * @swagger
 * /booking/addBookingAmountByArtist:
 *   put:
 *     tags:
 *       - Booking
 *     description: Reschedule booking
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: bookingAmount
 *         description: Reschedule Booking data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/addBookingAmount'
 *     responses:
 *       200:
 *         description: Booking amount save successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.put(
  "/addBookingAmountByArtist",
  authenticate,
  BookingController.addBookingAmountByArtist
);
//addBookingAmountByArtist
module.exports = router;

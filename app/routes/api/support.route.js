const express = require("express");
const router = express.Router();
const validationSchema = require("../../validations/support");
const validator = require("../../validations/validator");
const supportController = require("../../controllers/support.controller");
const AuthService = require("../../services/auth");
const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);

/**
 * @swagger
 * /support/generateTicket:
 *   post:
 *     tags:
 *       - User Support
 *     description: Generate new ticket
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Ticket Data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/generateTicket'
 *     responses:
 *       200:
 *         description: Ticket generated successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/generateTicket",
  authenticate,
  validateRequest,
  supportController.generateTicket
);

/**
 * @swagger
 * /support/sendTicketMessage:
 *   post:
 *     tags:
 *       - User Support
 *     description: Send new message
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Message
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/sendTicketMessage'
 *     responses:
 *       200:
 *         description: Message send successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post(
  "/sendTicketMessage",
  authenticate,
  validateRequest,
  supportController.sendTicketMessage
);

/**
 * @swagger
 * /support/updateTicketStatus:
 *   put:
 *     tags:
 *       - User Support
 *     description: Update ticket status
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Ticket status
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/ticketStatus'
 *     responses:
 *       200:
 *         description: Ticket updated successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.put(
  "/updateTicketStatus",
  authenticate,
  validateRequest,
  supportController.updateTicketStatus
);

/**
 * @swagger
 * /support/updateMessage:
 *   put:
 *     tags:
 *       - User Support
 *     description: Update Message
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Message update
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/messageUpdate'
 *     responses:
 *       200:
 *         description: Message updated successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.put(
  "/updateMessage",
  authenticate,
  validateRequest,
  supportController.updateMessage
);

/**
 * @swagger
 * /support/deleteMessage:
 *   delete:
 *     tags:
 *       - User Support
 *     description: Delete Message
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: messageId
 *         description: Message Id
 *         in: query
 *         required: true
 *         type: string
 *       - name: ticketId
 *         description: Ticket Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Message deleted successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.delete(
  "/deleteMessage",
  authenticate,
  validateRequest,
  supportController.deleteMessage
);

/**
 * @swagger
 * /support/deleteTicket:
 *   delete:
 *     tags:
 *       - User Support
 *     description: Delete Ticket
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: ticketId
 *         description: Ticket Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Ticket deleted successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.delete(
  "/deleteTicket",
  authenticate,
  validateRequest,
  supportController.deleteTicket
);

/**
 * @swagger
 * /support/getTicketList:
 *   get:
 *     tags:
 *       - User Support
 *     description: Get ticket list
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Limit
 *         in: query
 *         required: false
 *         type: number
 *       - name: page
 *         description: page
 *         in: query
 *         required: false
 *         type: number
 *     responses:
 *       200:
 *         description: Ticket list shown successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/getTicketList", authenticate, supportController.getTicketList);

/**
 * @swagger
 * /support/getTicketDetails:
 *   get:
 *     tags:
 *       - User Support
 *     description: Get ticket details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: ticketId
 *         description: Ticket Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Ticket details shown successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getTicketDetails",
  authenticate,
  supportController.getTicketDetails
);

module.exports = router;

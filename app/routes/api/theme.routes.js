const express = require("express");
const validationSchema = require("../../validations/association");
const validator = require("../../validations/validator");
const themeController = require("../../controllers/themeManagement.controller");
const AuthService = require("../../services/auth");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /theme/themeDetails:
 *   get:
 *     tags:
 *       - Themes
 *     description: Theme details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Theme Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/themeDetails", authenticate, themeController.themeDetails);

/**
 * @swagger
 * /theme/userThemeDetails:
 *   get:
 *     tags:
 *       - Themes
 *     description: Theme details
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/userThemeDetails", authenticate, themeController.userThemeDetails);

//    /**
//  * @swagger
//  * /theme/themeList:
//  *   get:
//  *     tags:
//  *       - Themes
//  *     description: Theme details
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     responses:
//  *       200:
//  *         description: List Shown Successfully
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */

//  router.get('/themeList', authenticate, themeController.themeList)

/**
 * @swagger
 * /theme/updateUserThemeData:
 *   put:
 *     tags:
 *       - Themes
 *     description: Update theme data
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Theme data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updateUserThemeData'
 *     responses:
 *       200:
 *         description: Shown Successfully
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.put(
  "/updateUserThemeData",
  authenticate,
  themeController.updateUserThemeData
);
//  updateUserThemeData
module.exports = router;

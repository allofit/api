const express = require("express");
const validationSchema = require("../../validations/association");
const validator = require("../../validations/validator");
const AssociationController = require("../../controllers/association.controller");
const AuthService = require("../../services/auth");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /association/getUnjoinedList:
 *   get:
 *     tags:
 *       - Customer Association
 *     description: Get list of artist or studio
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/user'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/getUnjoinedList",
  authenticate,
  AssociationController.getUnjoinedList
);

/**
 * @swagger
 * /association/sendJoinRequest:
 *   post:
 *     tags:
 *       - Customer Association
 *     description: Join new artist or studio
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/joinUsers'
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/associationManagement'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/sendJoinRequest",
  authenticate,
  AssociationController.sendJoinRequest
);

/**
 * @swagger
 * /association/getPendingRequestList:
 *   get:
 *     tags:
 *       - Customer Association
 *     description: Get Received request list
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/associationManagement'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/getPendingRequestList",
  authenticate,
  AssociationController.getPendingRequestList
);

/**
 * @swagger
 * /association/joinRequestAction:
 *   post:
 *     tags:
 *       - Customer Association
 *     description: Accept or Reject join request
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: customer's Entity
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/joinRequestAction'
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/associationManagement'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.post(
  "/joinRequestAction",
  authenticate,
  AssociationController.joinRequestAction
);

/**
 * @swagger
 * /association/getJoinedList:
 *   get:
 *     tags:
 *       - Customer Association
 *     description: Get Joined list
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successfully Shown
 *         schema:
 *           $ref: '#/definitions/associationManagement'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get("/getJoinedList", authenticate, AssociationController.getJoinedList);

/**
 * @swagger
 * /association/removeJoinedUser/{removerId}:
 *   get:
 *     tags:
 *       - Customer Association
 *     description: Remove joined user
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: removerId
 *         description: Remover Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully removed
 *         schema:
 *           $ref: '#/definitions/associationManagement'
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */

router.get(
  "/removeJoinedUser/:removerId",
  authenticate,
  AssociationController.removeJoinedUser
);

module.exports = router;

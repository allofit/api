const express = require("express");
const validationSchema = require("../../validations/user");
const validator = require("../../validations/validator");
const paymentController = require("../../controllers/payment.controller");
const AuthService = require("../../services/auth");
const uploadImage = require("../../services/upload");

const authenticate = AuthService.jwtAuthenticate;
const validateRequest = validator(false, validationSchema);
const router = express.Router();

/**
 * @swagger
 * /payment/payment:
 *   post:
 *     tags:
 *       - Payment
 *     description: Payment
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Payment data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/paymentData'
 *     responses:
 *       200:
 *         description: Payment Done.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/payment", paymentController.payment);

/**
 * @swagger
 * /payment/refund:
 *   post:
 *     tags:
 *       - Payment
 *     description: refund
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: refund data
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/refundData'
 *     responses:
 *       200:
 *         description: Refund successfully.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.post("/refund", paymentController.refund);

// /**
//  * @swagger
//  * /payment/paymentList:
//  *   get:
//  *     tags:
//  *       - Payment
//  *     description: PaymentList
//  *     security:
//  *       - JWT: []
//  *     produces:
//  *       - application/json
//  *     responses:
//  *       200:
//  *         description: Payment list Shown.
//  *       401:
//  *         $ref: '#/responses/Unauthorized'
//  *       400:
//  *         $ref: '#/responses/BadRequest'
//  */
// router.get('/paymentList', paymentController.paymentList)

/**
 * @swagger
 * /payment/paymentListForUser:
 *   get:
 *     tags:
 *       - Payment
 *     description: PaymentList
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Pagination
 *         in: query
 *         required: true
 *         type: string
 *       - name: page
 *         description: Pagination
 *         in: query
 *         required: true
 *         type: string
 *       - name: userType
 *         description: User type
 *         in: query
 *         type: string
 *     responses:
 *       200:
 *         description: Payment list Shown.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get(
  "/paymentListForUser",
  authenticate,
  paymentController.paymentListForUser
);
//paymentListForUser

/**
 * @swagger
 * /payment/paymentList:
 *   get:
 *     tags:
 *       - Payment
 *     description: PaymentList
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: limit
 *         description: Pagination
 *         in: query
 *         required: true
 *         type: string
 *       - name: page
 *         description: Pagination
 *         in: query
 *         required: true
 *         type: string
 *       - name: searchKey
 *         description: SearchKey
 *         in: query
 *         type: string
 *       - name: searchBody
 *         description: Search Body
 *         in: query
 *         type: string
 *     responses:
 *       200:
 *         description: Payment list Shown.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/paymentList", paymentController.paymentList);

/**
 * @swagger
 * /payment/paymentDetails:
 *   get:
 *     tags:
 *       - Payment
 *     description: PaymentList
 *     security:
 *       - JWT: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: paymentId
 *         description: Payment Id
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Payment details.
 *       401:
 *         $ref: '#/responses/Unauthorized'
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
router.get("/paymentDetails", paymentController.paymentDetails);

router.post(
  "/addCustomerAndAccount",
  authenticate,
  paymentController.addCustomerAndAccount
);
module.exports = router;

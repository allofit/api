// server.js
require("dotenv").config();

// process.env.NODE_ENV = 'local';

var config = require("./config");

var app = require("./app");

var fs = require("fs");
var https = require("https");
var http = require("http");

let credentials = {};
if(config.env === 'development'){
  const privateKey = fs.readFileSync("/etc/apache2/ssl/allofit.key", "utf8");
  const certificate = fs.readFileSync("/etc/apache2/ssl/allofit.crt", "utf8");
  credentials = { key: privateKey, cert: certificate };
}else {
  const privateKey = fs.readFileSync("./config/server.key", "utf8");
  const certificate = fs.readFileSync("./config/server.cert", "utf8");
  credentials = { key: privateKey, cert: certificate };
}



var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

const HTTP_PORT = process.env.HTTP_PORT || 3000;
const HTTPS_PORT = process.env.HTTPS_PORT || 8443;

httpServer.listen(HTTP_PORT);
httpsServer.listen(HTTPS_PORT);

// var server = httpserver.listen(config.port, () => {
//   console.log(`Server started on Port @ ${server.address().port}`); //listening port

//   process.on("uncaughtException", function(err) {
//     console.log("Caught exception: " + err);
//     throw err;
//   });
// });

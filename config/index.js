// config.js

var env = process.env.NODE_ENV || "production";

var config = {
  local: {
    env: process.env.NODE_ENV,
    host: "192.168.0.147",
    ssl: false,
    port: 3002
  },
  development: {
    env: process.env.NODE_ENV,
    ssl: false,
    port: 3002,
    host: "13.210.26.150"
  },
  production: {
    env: process.env.NODE_ENV,
    ssl: false,
    port: 3002,
    host: "13.210.26.150"
  }
};

module.exports = config[env];
